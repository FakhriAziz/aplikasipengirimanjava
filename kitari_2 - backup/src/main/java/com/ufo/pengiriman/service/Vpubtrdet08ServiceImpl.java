package com.ufo.pengiriman.service;

import com.ufo.pengiriman.dao.Vpubtrdet08DAO;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author PROGRAMER
 */
@Service
@Transactional
public class Vpubtrdet08ServiceImpl implements Vpubtrdet08Service {

    @Autowired
    Vpubtrdet08DAO vpubtrdet08DAO;
    
    @Override
    @Transactional
    //FAF, tambah parameter
    public int addSOkirim(int jmlDataTambahBarangSO, String[] dataQtyKirimSO, String[] dataTglKirimSO, String[] dataNomorBuktiSO,
            String[] dataNomorTRMSTSO, String[] dataNomorTRDETSO, String dataNamaCustomerSO, String dataAlamatCustomerSO, String dataTelpCustomerSO, String dataNAMA_DRIVER, String dataNO_PLAT)
    {
        return vpubtrdet08DAO.addSOkirim(jmlDataTambahBarangSO, dataQtyKirimSO, dataTglKirimSO, dataNomorBuktiSO,
                dataNomorTRMSTSO, dataNomorTRDETSO, dataNamaCustomerSO, dataAlamatCustomerSO, dataTelpCustomerSO, dataNAMA_DRIVER, dataNO_PLAT);
    }
    
    @Override
    @Transactional
    public int addKirimMutasi(int dataProsesMutasi, String[] dataNomorBuktiOM, String[] dataNomorTRMSTOM, String[] dataNomorTRDETOM,
            String[] dataQtyKirimOM, String[] dataTglOM, String[] dataMemoMutasi)
    {
        return vpubtrdet08DAO.addKirimMutasi(dataProsesMutasi, dataNomorBuktiOM, dataNomorTRMSTOM, dataNomorTRDETOM,
            dataQtyKirimOM, dataTglOM, dataMemoMutasi);
    }
    
    @Override
    @Transactional
    public int addTerimaRJ(int dataProsesTRJ, String[] dataNomorBuktiTRJ, String[] dataNomorTRMSTTRJ, String[] dataNomorTRDETTRJ,
            String[] dataQtyTRJ, String[] dataTglTRJ, String[] dataMemoTRJ, String[] dataNamaTRMSTPHORJ, String[] dataAlamatTRMSTPHORJ,
            String[] dataTelpTRMSTPHORJ)
    {
        return vpubtrdet08DAO.addTerimaRJ(dataProsesTRJ, dataNomorBuktiTRJ, dataNomorTRMSTTRJ, dataNomorTRDETTRJ,
            dataQtyTRJ, dataTglTRJ, dataMemoTRJ, dataNamaTRMSTPHORJ, dataAlamatTRMSTPHORJ, dataTelpTRMSTPHORJ);
    }
    
    @Override
    @Transactional
    public int addTerimaRB(int dataProsesTRB, String[] dataNomorBuktiTRB, String[] dataNomorTRMSTTRB, String[] dataNomorTRDETTRB,
        String[] dataQtyTRB, String[] dataTglTRB, String[] dataMemoTRB, String[] dataSupplierTRB, String[] dataAlamatTRMSTPHORB,
        String[] dataTelpTRMSTPHORB)
    {
        return vpubtrdet08DAO.addTerimaRB(dataProsesTRB, dataNomorBuktiTRB, dataNomorTRMSTTRB, dataNomorTRDETTRB,
            dataQtyTRB, dataTglTRB, dataMemoTRB, dataSupplierTRB, dataAlamatTRMSTPHORB, dataTelpTRMSTPHORB);
    }
    
    @Override
    @Transactional
    public int addTerimaLPB(int dataProsesLPB, String[] dataNomorBuktiLPB, String dataNamaSupplierLPB, String dataAlamatSupplierLPB, 
            String dataTelpSupplierLPB, String[] dataNomorTRMSTLPB, String[] dataNomorTRDETLPB, String[] dataQtyLPB, String[] dataTglLPB,
            String[] dataMemoLPB)
    {
        return vpubtrdet08DAO.addTerimaLPB(dataProsesLPB, dataNomorBuktiLPB, dataNamaSupplierLPB, dataAlamatSupplierLPB,
                dataTelpSupplierLPB, dataNomorTRMSTLPB, dataNomorTRDETLPB, dataQtyLPB, dataTglLPB, dataMemoLPB);
    }
    
    @Override
    @Transactional
    public List<String> getSuratJalan() {
        return vpubtrdet08DAO.getSuratJalan();
    }

    @Override
    @Transactional
    public List<String> getTglSuratJalan(String tgl1, String tgl2) {
        return vpubtrdet08DAO.getTglSuratJalan(tgl1, tgl2);
    }

    @Override
    @Transactional
    public List<String> getReport() {
        return vpubtrdet08DAO.getReport();
    }

    @Override
    @Transactional
    public List<String> getTglReport(String tglawal, String tglakhir) {
        return vpubtrdet08DAO.getTglReport(tglawal, tglakhir);
    }
    
    @Override
    @Transactional
    public List<String> getLapMutasi() {
        return vpubtrdet08DAO.getLapMutasi();
    }

    @Override
    @Transactional
    public List<String> getTglLapMutasi(String tglawal, String tglakhir) {
        return vpubtrdet08DAO.getTglLapMutasi(tglawal, tglakhir);
    }
    
    //untuk jasper report SJ penjualan (main report)
    @Override
    @Transactional
    public JRDataSource getDataSJpenjualan(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.getDataSJpenjualan(noTRMSTSJ);
    }
    
    //untuk jasper report SJ penjualan (sub report)
    @Override
    @Transactional
    public JRDataSource getDataSJpenjualan_sub(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.getDataSJpenjualan_sub(noTRMSTSJ);
    }
    
    //untuk jasper report SJ penjualan (main report)
    @Override
    @Transactional
    public JRDataSource regetDataSJpenjualan(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.regetDataSJpenjualan(noTRMSTSJ);
    }
    
    //untuk jasper report SJ penjualan (sub report)
    @Override
    @Transactional
    public JRDataSource regetDataSJpenjualan_sub(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.regetDataSJpenjualan_sub(noTRMSTSJ);
    }
    
    //untuk jasper report invoice SJ (main report)
    @Override
    @Transactional
    public JRDataSource getDataInvoiceSJ(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.getDataInvoiceSJ(noTRMSTSJ);
    }
    
    //untuk jasper report invoice SJ (sub report)
    @Override
    @Transactional
    public JRDataSource getDataInvoiceSJ_sub(Integer noTRMSTSJ) {
        return vpubtrdet08DAO.getDataInvoiceSJ_sub(noTRMSTSJ);
    }
    
    //untuk jasper report mutasi (main report)
    @Override
    @Transactional
    public JRDataSource getDataMutasi(Integer kodeMutasi) {
        return vpubtrdet08DAO.getDataMutasi(kodeMutasi);
    }
    
    //untuk jasper report mutasi (sub report)
    @Override
    @Transactional
    public JRDataSource getDataMutasi_sub(Integer kodeMutasi) {
        return vpubtrdet08DAO.getDataMutasi_sub(kodeMutasi);
    }
    
    //untuk jasper report terima retur jual (main report)
    @Override
    @Transactional
    public JRDataSource getDataTRJ(Integer kodeTRJ) {
        return vpubtrdet08DAO.getDataTRJ(kodeTRJ);
    }
    
    //untuk jasper report terima retur jual (sub report)
    @Override
    @Transactional
    public JRDataSource getDataTRJ_sub(Integer kodeTRJ) {
        return vpubtrdet08DAO.getDataTRJ_sub(kodeTRJ);
    }
    
    //untuk jasper report invoice TRJ (main report)
    @Override
    @Transactional
    public JRDataSource getDataInvoiceTRJ(Integer noTRMST_TRJ) {
        return vpubtrdet08DAO.getDataInvoiceTRJ(noTRMST_TRJ);
    }
    
    //untuk jasper report invoice TRJ (sub report)
    @Override
    @Transactional
    public JRDataSource getDataInvoiceTRJ_sub(Integer noTRMST_TRJ) {
        return vpubtrdet08DAO.getDataInvoiceTRJ_sub(noTRMST_TRJ);
    }
    
    //untuk jasper report terima retur beli (main report)
    @Override
    @Transactional
    public JRDataSource getDataTRB(Integer kodeTRB) {
        return vpubtrdet08DAO.getDataTRB(kodeTRB);
    }
    
    //untuk jasper report terima retur beli (sub report)
    @Override
    @Transactional
    public JRDataSource getDataTRB_sub(Integer kodeTRB) {
        return vpubtrdet08DAO.getDataTRB_sub(kodeTRB);
    }
    
    //untuk jasper report form LPB (main report)
    @Override
    @Transactional
    public JRDataSource getDataLPB(Integer kodeLPB) {
        return vpubtrdet08DAO.getDataLPB(kodeLPB);
    }
    
    //untuk jasper report form LPB (sub report)
    @Override
    @Transactional
    public JRDataSource getDataLPB_sub(Integer kodeLPB) {
        return vpubtrdet08DAO.getDataLPB_sub(kodeLPB);
    }
    
}
