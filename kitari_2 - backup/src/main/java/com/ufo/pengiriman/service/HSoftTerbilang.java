/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.service;

/**
 *
 * @author heince
 */
public final class HSoftTerbilang {

    private static final String[] cBilangan = {"", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh",
        "Delapan", "Sembilan", "Sepuluh", "Sebelas"
    };
    private String hasil;
    private long nilai;

    public HSoftTerbilang() {
        hasil = "";
    }

    public HSoftTerbilang(int nilai) {
        setNilai(nilai);
    }

    public HSoftTerbilang(long nilai) {
        setNilai(nilai);
    }

    public String getHasil() {
        return hasil;
    }

    public long getNilai() {        //getNilai yg int tidak ada karena bentrok dg yang long ini
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
        setNilai(this.nilai);   //dirubah ke long semua
    }

    public void setNilai(long nilai) {
        this.nilai = nilai;
        hasil = konversiBesar(this.nilai).trim();   //buang space
    }

    @Override
    public String toString() {
        return getHasil();
    }

    private String konversiKecil(int i) {
        //The largest integer value is 2147483648
        //                             1000000000

        if (i < 12) {
            return " "+cBilangan[i];
        } else if (i < 20) {
            return konversiKecil(i - 10) + " Belas";
        } else if (i < 100) {
            return konversiKecil(i / 10) + " Puluh" + konversiKecil(i % 10);
        } else if (i < 200) {
            return " Seratus" + konversiKecil(i - 100);
        } else if (i < 1000) {
            return konversiKecil(i / 100) + " Ratus" + konversiKecil(i % 100);
        } else if (i < 2000) {
            return " Seribu" + konversiKecil(i - 1000);
        } else if (i < 1000000) {   //1.000.000

            return konversiKecil(i / 1000) + " Ribu" + konversiKecil(i % 1000);
        } else {
            return "kecil overflow";
        }
    }

    private String konversiBesar(long i) {
        //The largest long value is 9223372036854775808
        //                          1000000000000
        int x;
        int y;
        long z;
        if (i < 1000000l) {
            x = (int) i;
            return konversiKecil(x);
        } else if ((i >= 1000000l) && (i < 1000000000l)) {
            x = (int) (i / 1000000l);
            y = (int) (i % 1000000l);
            return konversiKecil(x) + " Juta" + konversiKecil(y);
        } else if ((i >= 1000000000l) && (i < 1000000000000l)) {
            x = (int) (i / 1000000000l);
            y = (int) (i % 1000000000l);
            return konversiKecil(x) + " Miliar" + konversiBesar(y);
        } else if ((i >= 1000000000000l) && (i < 1000000000000000l)) {
            x = (int) (i / 1000000000000l);
            z = (i % 1000000000000l);
            return konversiKecil(x) + " Triliun" + konversiBesar(z);
        } else {
            return "besar overflow";
        }
    }
}

