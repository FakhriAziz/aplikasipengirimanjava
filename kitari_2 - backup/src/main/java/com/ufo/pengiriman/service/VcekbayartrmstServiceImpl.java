package com.ufo.pengiriman.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ufo.pengiriman.dao.VcekbayartrmstDAO;
import java.util.List;

/**
 *
 * @author PROGRAMER
 */
@Service
@Transactional
public class VcekbayartrmstServiceImpl implements VcekbayartrmstService {

    @Autowired
    VcekbayartrmstDAO vcekbayartrmstDAO;
    
    @Override
    @Transactional
    public List<Object> getCountDashboard() {
        return vcekbayartrmstDAO.getCountDashboard();   //logic dashboard mutasi
    }
    
    @Override
    @Transactional
    public Double getTotalSJPerHari() {
        return vcekbayartrmstDAO.getTotalSJPerHari();   //logic total kirim order penjualan untuk batasan qty kirim gudang per harinya
    }
    
    @Override
    @Transactional
    public Double getTotalMTPerHari() {
        return vcekbayartrmstDAO.getTotalMTPerHari();   //logic total kirim order mutasi untuk batasan qty kirim gudang per harinya
    }
    
    @Override
    @Transactional
    public Double getTotalRJPerHari() {
        return vcekbayartrmstDAO.getTotalRJPerHari();   //logic total kirim order retur jual
    }
    
    @Override
    @Transactional
    public Double getTotalRBPerHari() {
        return vcekbayartrmstDAO.getTotalRBPerHari();   //logic total kirim order retur beli
    }
    
    @Override
    @Transactional
    public List<String> getCekBayarTrmst() {
        return vcekbayartrmstDAO.getCekBayarTrmst();
    }
    
    @Override
    @Transactional
    public List<String> getTglSO(String tgl1, String tgl2, short selisihTgl) {
        return vcekbayartrmstDAO.getTglSO(tgl1, tgl2, selisihTgl);
    }
    
    /**
     * logic default tarik list OM utk pengiriman mutasi dari 1 hari terakhir
     * @return 
     */
    @Override
    @Transactional
    public List<String> getCekMutasi() {
        return vcekbayartrmstDAO.getCekMutasi();
    }
    
    /**
     * logic tarik list OM utk pengiriman mutasi dengan pilih tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @param selisihTgl
     * @return 
     */
    @Override
    @Transactional
    public List<String> getTglMutasi(String tgl1, String tgl2, short selisihTgl) {
        return vcekbayartrmstDAO.getTglMutasi(tgl1, tgl2, selisihTgl);
    }
    
    @Override
    @Transactional
    public List<String> getCekORJ() {
        return vcekbayartrmstDAO.getCekORJ();    //logic default daftar order retur jual
    }
    
    @Override
    @Transactional
    public List<String> getTglORJ(String tgl1, String tgl2) {
        return vcekbayartrmstDAO.getTglORJ(tgl1, tgl2);      //logic pilih tanggal daftar order retur jual
    }
    
    @Override
    @Transactional
    public List<String> getCekORB() {
        return vcekbayartrmstDAO.getCekORB();    //logic default daftar order retur beli
    }
    
    @Override
    @Transactional
    public List<String> getTglORB(String tgl1, String tgl2) {
        return vcekbayartrmstDAO.getTglORB(tgl1, tgl2);      //logic pilih tanggal daftar order retur beli
    }
    
    @Override
    @Transactional
    public List<String> getCekPO() {
        return vcekbayartrmstDAO.getCekPO();    //logic default daftar PO
    }
    
    @Override
    @Transactional
    public List<String> getTglPO(String tgl1, String tgl2, short selisihTgl) {
        return vcekbayartrmstDAO.getTglPO(tgl1, tgl2, selisihTgl);      //logic pilih tanggal daftar PO
    }
    
    @Override
    @Transactional
    public List<String> getBarang(String nomor) {
        return vcekbayartrmstDAO.getBarang(nomor);
    }
    
    @Override
    @Transactional
    public List<String> getCekUser() {
        return vcekbayartrmstDAO.getCekUser();
    }
    
    @Override
    @Transactional
    public String getLogin(String noBukti) {
        return vcekbayartrmstDAO.getLogin(noBukti);
    }
    
    @Override
    @Transactional
    public String getNoBukti(Integer noBukti) {
        return vcekbayartrmstDAO.getNoBukti(noBukti);
    }

    @Override
    @Transactional
    public void deleteData(Integer noBukti) {
        vcekbayartrmstDAO.deleteData(noBukti);
    }
    
//  method getSO di-off karena tidak dipanggil/tidak digunakan
//    @Override
//    @Transactional
//    public List<String> getSO(List<String> NoBukti) {
//        return vcekbayartrmstDAO.getSO(NoBukti);
//    }

}
