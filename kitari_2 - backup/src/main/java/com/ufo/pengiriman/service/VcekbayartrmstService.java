package com.ufo.pengiriman.service;

import java.util.List;

/**
 *
 * @author PROGRAMER
 */
public interface VcekbayartrmstService {
    
    public List<Object> getCountDashboard();
    
    public Double getTotalSJPerHari();
    public Double getTotalMTPerHari();
    public Double getTotalRJPerHari();
    public Double getTotalRBPerHari();

    public List<String> getCekBayarTrmst();
    public List<String> getTglSO(String tgl1, String tgl2, short selisihTgl);
    
    public List<String> getCekMutasi();     //sonny, add info: logic default tarik list OM
    public List<String> getTglMutasi(String tgl1, String tgl2, short selisihTgl);   //sonny, add info: logic tarik list OM dgn pilih tanggal
    
    public List<String> getCekORJ();     //sonny, add info: logic default tarik list ORJ
    public List<String> getTglORJ(String tgl1, String tgl2);    //sonny, add info: logic tarik list ORJ dgn pilih tanggal
    
    public List<String> getCekORB();     //sonny, add info: logic default tarik list ORB
    public List<String> getTglORB(String tgl1, String tgl2);    //sonny, add info: logic tarik list ORJ dgn pilih tanggal
    
    public List<String> getCekPO();     //sonny, add info: logic default tarik list PO
    public List<String> getTglPO(String tgl1, String tgl2, short selisihTgl);    //sonny, add info: logic tarik list PO dgn pilih tanggal

    public List<String> getBarang(String nomor);

    public List<String> getCekUser();

    public String getLogin(String noTRMST);

    public String getNoBukti(Integer noBukti);

    public void deleteData(Integer noBukti);
    
    //  method getSO di-off karena tidak dipanggil/tidak digunakan
    //public List<String> getSO(List<String> NoBukti);

}
