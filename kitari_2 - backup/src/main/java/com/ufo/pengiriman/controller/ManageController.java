package com.ufo.pengiriman.controller;

import com.ufo.pengiriman.hibernate.HibernateUtil;
import com.ufo.pengiriman.service.VcekbayartrmstService;
//import org.jboss.logging.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.ufo.pengiriman.service.Vpubtrdet08Service;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.ModelAttribute;
//import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author PROGRAMER
 */
@RestController
public class ManageController {

    //private static final Logger logger = Logger.getLogger(ManageController.class);

    @Autowired
    Vpubtrdet08Service vpubtrdet08Service;

    @Autowired
    VcekbayartrmstService vcekbayartrmstService;

    @Autowired
    HibernateUtil hibernateUtil;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private HttpSession session;

    //Proses Menambah SO Untuk Menjadi Surat Jalan
//    @RequestMapping(value = "/tambahKirim", method = RequestMethod.POST, headers = "Accept=application/json")
    @RequestMapping(value = "/tambahKirim", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView tambahBarang(ModelAndView model) {
        //SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm:ss.SSS");   //eric, off karena tidak digunakan
        
        String[] dataNomorBuktiSO = request.getParameterValues("dataNomorBuktiSO");         //#1
        String[] dataQtyKirimSO = request.getParameterValues("dataQtyKirimSO");             //#2
        String[] dataNamaCustomerSO = request.getParameterValues("dataNamaCustomerSO");     //#3
        String[] dataAlamatCustomerSO = request.getParameterValues("dataAlamatCustomerSO"); //#4
        String[] dataTelpCustomerSO = request.getParameterValues("dataTelpCustomerSO");     //#5
        String[] dataNomorProgLama = request.getParameterValues("dataNomorProgLama");       //#6, NOMORPROGLAMA untuk dibuatkan session di bawah
        String[] dataNomorTRMSTSO = request.getParameterValues("dataNomorTRMSTSO");         //#7
        String[] dataNomorTRDETSO = request.getParameterValues("dataNomorTRDETSO");         //#8
        String[] dataTglKirimSO = request.getParameterValues("dataTglKirimSO");             //#9
        String[] dataSPM = request.getParameterValues("dataSPM");             //#10, NAMA SPM utk get session jasper invoice
        String[] dataNAMA_DRIVER = request.getParameterValues("dataNAMA_DRIVER");             //FAF #11, NAMA DRIVER utk get session jasper invoice
        String[] dataNO_PLAT = request.getParameterValues("dataNO_PLAT");             //FAF #12, NO PLAT utk get session jasper invoice
        /*   ganti ke nomortrmst dan nomortrdet
            String[] dnomor = request.getParameterValues("dtnomor[]");    
        */
        
        int jmlDataTambahBarangSO = dataNomorBuktiSO.length;
        
        //kunci penting SJ disini
        int cekSimpanSJ = vpubtrdet08Service.addSOkirim(jmlDataTambahBarangSO, dataQtyKirimSO, dataTglKirimSO, dataNomorBuktiSO,
                dataNomorTRMSTSO, dataNomorTRDETSO, dataNamaCustomerSO[0], dataAlamatCustomerSO[0], dataTelpCustomerSO[0], dataNAMA_DRIVER[0], dataNO_PLAT[0]); //FAF, Tambahan
        
        System.out.println("\n\n==================================== ManageController: cekSimpanSJ (output return dari addSOkirim)");
        System.out.println("cekSimpanSJ = " + cekSimpanSJ);
        
        if (cekSimpanSJ != 0) {
            session.setAttribute("cekSimpanSJ", cekSimpanSJ);
            session.setAttribute("NOMOR_LAMA", dataNomorProgLama[0]); //dipanggil di jasper SJ dan Invoice (Vpubtrdet08DAOImpl)
            session.setAttribute("SPM", dataSPM[0]); //dipanggil di jasper Invoice (Vpubtrdet08DAOImpl)

/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//            Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();
//            Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();
//            Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
//            DecimalFormat df = new DecimalFormat("###.#");
//
//            //buat session untuk dipanggil di method lain
//            session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));

            //SOUT
//            System.out.println("\n========================= totalSJPerHari, totalMTPerHari, totalBatasKirim (MainController: public ModelAndView listDO)");
//            System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//            System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//            System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));
            
            //set view name (file jsp)
            model.setViewName("success");
        }
        else {
            String status = vcekbayartrmstService.getLogin(dataNomorBuktiSO[0]);
            if (!"null".equals(status)) {
                model.addObject("errUser", true);
                model.addObject("errList", "Data Sudah Pernah disimpan Oleh " + status + ", Harap Refresh Data Pengiriman");
            }
            else {
                model.addObject("errSave", true);
                model.addObject("errList", "Gagal Menyimpan Mohon Untuk Mencoba Lagi");
            }
            model.setViewName("error");
        }
        return model;
    }
    
    //Proses Mutasi Stok Menjadi Surat Mutasi
//    @RequestMapping(value = "/prosesMutasi", method = RequestMethod.POST, headers = "Accept=application/json")
    @RequestMapping(value = "/prosesMutasi", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView prosesMutasi(ModelAndView model) {
//        List<Map<Object, Object>> list_prosesMutasi = new ArrayList<>();
        
        String[] dataMemoMutasi = request.getParameterValues("dataMemoMutasi");         //#1
        String[] dataTglOM = request.getParameterValues("dataTglOM");                   //#2
        String[] dataNomorBuktiOM = request.getParameterValues("dataNomorBuktiOM");     //#3
        String[] dataTokoOM = request.getParameterValues("dataTokoOM");                 //#4
        String[] dataQtyKirimOM = request.getParameterValues("dataQtyKirimOM");         //#5
        String[] dataNomorTRMSTOM = request.getParameterValues("dataNomorTRMSTOM");     //#6
        String[] dataNomorTRDETOM = request.getParameterValues("dataNomorTRDETOM");     //#7
        
        int dataProsesMutasi = dataNomorBuktiOM.length;
        System.out.println("\n\n==================================== dataProsesMutasi");
        System.out.println("dataProsesMutasi = " + dataProsesMutasi);
        
        int cekProsesMutasi = vpubtrdet08Service.addKirimMutasi(dataProsesMutasi, dataNomorBuktiOM, dataNomorTRMSTOM, dataNomorTRDETOM,
                dataQtyKirimOM, dataTglOM, dataMemoMutasi);
        
        System.out.println("\n\n==================================== cekProsesMutasi");
        System.out.println("cekProsesMutasi = " + cekProsesMutasi);
        
        if (cekProsesMutasi != 0) { //perlu dipikirkan klo return 0
            /* bikin session utk diproses di ReportController dan diteruskan ke Jasperreports */
            session.setAttribute("CPM_TRMST_OM", cekProsesMutasi); //sebagai argument method getDataMutasi/getDataMutasi_sub di ModelAndView rcMutasi (ReportController)
            session.setAttribute("CPM_TOKO_OM", dataTokoOM[0]);    //sebagai item tambahan di jasper mainReport, tidak di subreport (cek getDataMutasi)
            
/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//            Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();
//            Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();
//            Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
//            DecimalFormat df = new DecimalFormat("###.#");

            //buat session untuk dipanggil di method lain
//            session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));

            //SOUT
//            System.out.println("\n========================= totalSJPerHari, totalMTPerHari, totalBatasKirim (MainController: public ModelAndView listDO)");
//            System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//            System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//            System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));
            
            //set view name (file jsp)
            model.setViewName("success-mutasi");
            
            /* for, di-looping setiap baris data String[] dataProsesMutasi.
            for (int i = 0; i < dataProsesMutasi; i++) {
                Map<Object, Object> data = new HashMap<>();
               data.put("dataMemoMutasi", dataMemoMutasi[i]);        //#1
                data.put("dataTglOM", dataTglOM[i]);                  //#2
                data.put("dataNomorBuktiOM", dataNomorBuktiOM[i]);    //#3
                data.put("dataTokoOM", dataTokoOM[i]);                //#4
                data.put("dataQtyKirimOM", dataQtyKirimOM[i]);        //#5
                data.put("dataNomorTRMSTOM", dataNomorTRMSTOM[i]);    //#6
                data.put("dataNomorTRDETOM", dataNomorTRDETOM[i]);    //#7
                list_prosesMutasi.add(data);
            }
            model.addObject("list_prosesMutasi", list_prosesMutasi);
            */
        
        }//end if (cekProsesMutasi != 0) 
        
        return model;   
    }
    
    //Proses Menghapus Data Surat Jalan
//    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @RequestMapping(value = "/{id}", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public RedirectView deleteBarang(
            ModelAndView model, 
            @PathVariable("id") String id,
            @RequestParam(value="tSAw", required = false) String tglSuratAwal,
            @RequestParam(value="tSAk", required = false) String tglSuratAkhir,
            RedirectAttributes redir) {
        
        //me-return tempBukti yg bertipe Integer
        Integer sBukti = hibernateUtil.setDecBukti(id);
        
        //me-return noBukti yg bertipe String
        //OFF, krn tdk perlu jk msg dibawah jg OFF => String NoBukti = vcekbayartrmstService.getNoBukti(sBukti);
        
        //OFF, krn tdk muncul klo redirect => model.addObject("msg", "Menghapus Dengan Kode Bukti : " + NoBukti);
        vcekbayartrmstService.deleteData(sBukti);   //jalankan method deleteData dgn argumen sBukti bertipe Integer

        //OFF => List<String> listSuratJalan = vpubtrdet08Service.getSuratJalan();   //diubah ke getTglSuratJalan jg bukan solusi
        //OFF => model.addObject("listsurat", listSuratJalan);
        //OFF => model.setViewName("listsuratjalan");

        model.addObject("cekUser", session.getAttribute("tempUser"));   //untuk di navbar.jsp => pageContext.request.contextPath
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp
        
        System.out.println("\n+++++++++++++++++++####################### ManageController: RequestParam String tglSuratAwal dan String tglSuratAkhir");
        System.out.println("tglSuratAwal = " + tglSuratAwal);
        System.out.println("tglSuratAkhir = " + tglSuratAkhir);
        
        redir.addFlashAttribute("tgl1_deleteBarang", tglSuratAwal);
        redir.addFlashAttribute("tgl2_deleteBarang", tglSuratAkhir);
        return new RedirectView("/listSurat", true);
    }
    
    //Proses Terima Retur Jual Menjadi Surat Retur Jual
//    @RequestMapping(value = "/prosesTRJ", method = RequestMethod.POST, headers = "Accept=application/json")
    @RequestMapping(value = "/prosesTRJ", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView prosesTRJ(ModelAndView model) {
//        List<Map<Object, Object>> list_prosesMutasi = new ArrayList<>();
        
        String[] dataMemoTRJ = request.getParameterValues("dataMemoTRJ");               //#1
        String[] dataTglTRJ = request.getParameterValues("dataTglTRJ");                 //#2
        String[] dataNomorBuktiTRJ = request.getParameterValues("dataNomorBuktiTRJ");   //#3
        String[] dataTokoTRJ = request.getParameterValues("dataTokoTRJ");               //#4
        String[] dataQtyTRJ = request.getParameterValues("dataQtyTRJ");                 //#5
        String[] dataNomorTRMSTTRJ = request.getParameterValues("dataNomorTRMSTTRJ");   //#6
        String[] dataNomorTRDETTRJ = request.getParameterValues("dataNomorTRDETTRJ");   //#7
        
        //UPDATE 2019-11-29: add data trmstph agar tdk hardcode lagi (#8 - #10)
        String[] dataNamaTRMSTPHORJ = request.getParameterValues("dataNamaTRMSTPHORJ");         //#8 request
        String[] dataAlamatTRMSTPHORJ = request.getParameterValues("dataAlamatTRMSTPHORJ");     //#9 request
        String[] dataTelpTRMSTPHORJ = request.getParameterValues("dataTelpTRMSTPHORJ");         //#10 request
        
        int dataProsesTRJ = dataNomorBuktiTRJ.length;
        System.out.println("\n\n==================================== dataProsesTRJ");
        System.out.println("dataProsesTRJ = " + dataProsesTRJ);
        
        int cekProsesTRJ = vpubtrdet08Service.addTerimaRJ(dataProsesTRJ, dataNomorBuktiTRJ, dataNomorTRMSTTRJ, dataNomorTRDETTRJ,
                dataQtyTRJ, dataTglTRJ, dataMemoTRJ, dataNamaTRMSTPHORJ, dataAlamatTRMSTPHORJ, dataTelpTRMSTPHORJ);
        
        System.out.println("\n\n==================================== cekProsesTRJ");
        System.out.println("cekProsesTRJ = " + cekProsesTRJ);
        
        if (cekProsesTRJ != 0) { //perlu dipikirkan klo return 0
            /* bikin session utk diproses di ReportController dan diteruskan ke Jasperreports */
            session.setAttribute("SESS_cekProsesTRJ", cekProsesTRJ); //sebagai argument method getDataTRJ/getDataTRJ_sub di ModelAndView rcTerimaRJ (ReportController)
            session.setAttribute("SESS_TOKO_TRJ", dataTokoTRJ[0]);    //sebagai item tambahan di jasper mainReport, tidak di subreport (cek getDataTRJ)
            

/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//            Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();      //BGMN RETUR JUAL MEMPENGARUHI QTY SJ ?
//            Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();      //BGMN RETUR JUAL MEMPENGARUHI MUTASI ?
            
//            Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
//            DecimalFormat df = new DecimalFormat("###.#");
//
//            //buat session untuk dipanggil di method lain
//            session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));

            //SOUT
//            System.out.println("\n========================= totalSJPerHari, totalMTPerHari, totalBatasKirim (MainController: public ModelAndView listDO)");
//            System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//            System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//            System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));

            //set view name (file jsp)
            model.setViewName("success-retur-jual");
        }//end if (cekProsesTRJ != 0) 
        
        return model;   
    }
    
    //Proses Terima Retur Beli Menjadi Surat Retur Beli
//    @RequestMapping(value = "/prosesTRB", method = RequestMethod.POST, headers = "Accept=application/json")
    @RequestMapping(value = "/prosesTRB", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView prosesTRB(ModelAndView model) {
//        List<Map<Object, Object>> list_prosesMutasi = new ArrayList<>();
        
        String[] dataMemoTRB = request.getParameterValues("dataMemoTRB");               //#1
        String[] dataTglTRB = request.getParameterValues("dataTglTRB");                 //#2
        String[] dataNomorBuktiTRB = request.getParameterValues("dataNomorBuktiTRB");   //#3
        String[] dataTokoTRB = request.getParameterValues("dataTokoTRB");               //#4
        String[] dataQtyTRB = request.getParameterValues("dataQtyTRB");                 //#5
        
        //UPDATE 2019-12-09: add data trmstph agar tdk hardcode lagi (#6, #9, #10)
        String[] dataSupplierTRB = request.getParameterValues("dataSupplierTRB");       //#6 request
        String[] dataNomorTRMSTTRB = request.getParameterValues("dataNomorTRMSTTRB");   //#7
        String[] dataNomorTRDETTRB = request.getParameterValues("dataNomorTRDETTRB");   //#8
        String[] dataAlamatTRMSTPHORB = request.getParameterValues("dataAlamatTRMSTPHORB");     //#9 request
        String[] dataTelpTRMSTPHORB = request.getParameterValues("dataTelpTRMSTPHORB");         //#10 request
        
        int dataProsesTRB = dataNomorBuktiTRB.length;
        System.out.println("\n\n==================================== dataProsesTRB");
        System.out.println("dataProsesTRB = " + dataProsesTRB);
        
        int cekProsesTRB = vpubtrdet08Service.addTerimaRB(dataProsesTRB, dataNomorBuktiTRB, dataNomorTRMSTTRB, dataNomorTRDETTRB,
                dataQtyTRB, dataTglTRB, dataMemoTRB, dataSupplierTRB, dataAlamatTRMSTPHORB, dataTelpTRMSTPHORB);
        
        System.out.println("\n\n==================================== cekProsesTRB");
        System.out.println("cekProsesTRB = " + cekProsesTRB);
        
        if (cekProsesTRB != 0) { //perlu dipikirkan klo return 0
            /* bikin session utk diproses di ReportController dan diteruskan ke Jasperreports */
            session.setAttribute("SESS_cekProsesTRB", cekProsesTRB); //sebagai argument method getDataTRB/getDataTRB_sub di ModelAndView rcTerimaRB (ReportController)
            session.setAttribute("SESS_TOKO_TRB", dataTokoTRB[0]);    //sebagai item tambahan di jasper mainReport, tidak di subreport (cek getDataTRB)
            
/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//            Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();      //BGMN RETUR BELI MEMPENGARUHI QTY SJ ?
//            Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();      //BGMN RETUR JUAL MEMPENGARUHI MUTASI ?
//            
//            Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
//            DecimalFormat df = new DecimalFormat("###.#");

            //buat session untuk dipanggil di method lain
//            session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));
            
            //SOUT
//            System.out.println("\n========================= totalSJPerHari, totalMTPerHari, totalBatasKirim (MainController: public ModelAndView listDO)");
//            System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//            System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//            System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));

            //set view name (file jsp)
            model.setViewName("success-retur-beli");
        }//end if (cekProsesTRB != 0) 
        
        return model;   
    }
    
    //Proses Terima LPB Menjadi Surat LPB
//    @RequestMapping(value = "/prosesLPB", method = RequestMethod.POST, headers = "Accept=application/json")
    @RequestMapping(value = "/prosesLPB", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView prosesLPB(ModelAndView model) {
//        List<Map<Object, Object>> list_prosesMutasi = new ArrayList<>();
        
        String[] dataMemoLPB = request.getParameterValues("dataMemoLPB");               //#1
        String[] dataTglLPB = request.getParameterValues("dataTglLPB");                 //#2
        String[] dataNomorBuktiLPB = request.getParameterValues("dataNomorBuktiLPB");   //#3
        
//        String[] dataTokoLPB = request.getParameterValues("dataTokoLPB"); //KHUSUS LPB OFF

        String[] dataQtyLPB = request.getParameterValues("dataQtyLPB");         //#4
        String[] dataNamaSupplierLPB = request.getParameterValues("dataNamaSupplierLPB");       //#5
        String[] dataAlamatSupplierLPB = request.getParameterValues("dataAlamatSupplierLPB");   //#6
        String[] dataTelpSupplierLPB = request.getParameterValues("dataTelpSupplierLPB");       //#7

        String[] dataNomorTRMSTLPB = request.getParameterValues("dataNomorTRMSTLPB");   //#8
        String[] dataNomorTRDETLPB = request.getParameterValues("dataNomorTRDETLPB");   //#9
        
        int dataProsesLPB = dataNomorBuktiLPB.length;
        System.out.println("\n\n==================================== dataProsesLPB");
        System.out.println("dataProsesLPB = " + dataProsesLPB);
        
        int cekProsesLPB = vpubtrdet08Service.addTerimaLPB(dataProsesLPB, dataNomorBuktiLPB, dataNamaSupplierLPB[0], dataAlamatSupplierLPB[0],
                dataTelpSupplierLPB[0], dataNomorTRMSTLPB, dataNomorTRDETLPB, dataQtyLPB, dataTglLPB, dataMemoLPB);
        
        System.out.println("\n\n==================================== cekProsesLPB");
        System.out.println("cekProsesLPB = " + cekProsesLPB);
        
        if (cekProsesLPB != 0) { //perlu dipikirkan klo return 0
            /* bikin session utk diproses di ReportController dan diteruskan ke Jasperreports */
            session.setAttribute("SESS_cekProsesLPB", cekProsesLPB); //sebagai argument method getDataLPB/getDataLPB_sub di ModelAndView rcTerimaLPB (ReportController)
//            session.setAttribute("SESS_TOKO_LPB", dataTokoLPB[0]);    //sebagai item tambahan di jasper mainReport, tidak di subreport (cek getDataLPB)
            
/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//            Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();      //BGMN RETUR BELI MEMPENGARUHI QTY SJ ?
//            Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();      //BGMN RETUR JUAL MEMPENGARUHI MUTASI ?
//            
//            Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
//            DecimalFormat df = new DecimalFormat("###.#");

            //buat session untuk dipanggil di method lain
//            session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));
            
            //SOUT
//            System.out.println("\n========================= totalSJPerHari, totalMTPerHari, totalBatasKirim (MainController: public ModelAndView listDO)");
//            System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//            System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//            System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));

            //set view name (file jsp)
            model.setViewName("success-pembelian");
        }//end if (cekProsesTRB != 0) 
        
        return model;   
    }

}
