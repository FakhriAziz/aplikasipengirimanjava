package com.ufo.pengiriman.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
/* import class2 ufo pengiriman */
import com.ufo.pengiriman.service.Vpubtrdet08Service;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author PROGRAMER
 */
@RestController
public class ReportController {

    @Autowired
    Vpubtrdet08Service vpubtrdet08Service;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpSession session;

    //controller jasper SJ penjualan
//    @RequestMapping(value = "/cetakPdf", method = RequestMethod.POST)
    @RequestMapping(value = "/psj", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView beranda(ModelAndView model) {
        Integer noTRMSTSJ = Integer.parseInt(session.getAttribute("cekSimpanSJ").toString());

        JRDataSource dsSJpenjualan = vpubtrdet08Service.getDataSJpenjualan(noTRMSTSJ);
        JRDataSource dsSJpenjualan_sub = vpubtrdet08Service.getDataSJpenjualan_sub(noTRMSTSJ);

        Map<String, Object> parameterMap = new HashMap<>();

        parameterMap.put("dsSJpenjualan_reportDataKey", dsSJpenjualan);
        parameterMap.put("dsSJpenjualan_subReportDataKeys", dsSJpenjualan_sub);

        model = new ModelAndView("pdfReport", parameterMap);

        return model;
    }

    @RequestMapping(value = "/re-psj", method = {RequestMethod.POST, RequestMethod.GET})
//    public ModelAndView reprintSJ(@RequestParam(value = "idBukti", required = true) String getNomorTRMST, ModelAndView model) {
    public ModelAndView reprintSJ(ModelAndView model) {

        String[] getNomorTRMST = request.getParameterValues("getNomorTRMST");
        
        int jmlDataKirimSO = getNomorTRMST.length;
        System.out.println("jmlDataKirimSO = " + jmlDataKirimSO);

        for (int i = 0; i < jmlDataKirimSO; i++) {
            int noTRMSTSJ = Integer.parseInt(getNomorTRMST[i]);
            System.out.println("Get No TRMST =" + noTRMSTSJ);

            JRDataSource dsReSJpenjualan = vpubtrdet08Service.regetDataSJpenjualan(noTRMSTSJ);
            JRDataSource dsReSJpenjualan_sub = vpubtrdet08Service.regetDataSJpenjualan_sub(noTRMSTSJ);

            Map<String, Object> parameterMap = new HashMap<>();

//        parameterMap.put("ds_ReSJpenjualan_reportDataKey", dsReSJpenjualan);
//        parameterMap.put("ds_ReSJpenjualan_subReportDataKeys", dsReSJpenjualan_sub);
//        
//        System.out.println("Isi Data =" + dsReSJpenjualan); 
//        System.out.println("Isi Data =" + dsReSJpenjualan_sub); 
//        
//        model = new ModelAndView("pdfReportRePrint", parameterMap);
//        
            parameterMap.put("dsSJpenjualan_reportDataKey", dsReSJpenjualan);
            parameterMap.put("dsSJpenjualan_subReportDataKeys", dsReSJpenjualan_sub);

            model = new ModelAndView("pdfReport", parameterMap);
        
        }
        return model;
    }

    //controller jasper InvoiceSJ
    @RequestMapping(value = "/pinvsj", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcInvoiceSJ(ModelAndView model) {
        Integer noTRMSTSJ = Integer.parseInt(session.getAttribute("cekSimpanSJ").toString());

        JRDataSource dsInvoiceSJ = vpubtrdet08Service.getDataInvoiceSJ(noTRMSTSJ);
        JRDataSource dsInvoiceSJ_sub = vpubtrdet08Service.getDataInvoiceSJ_sub(noTRMSTSJ);

        Map<String, Object> parameterMap = new HashMap<>();

        parameterMap.put("dsInvoiceSJ_reportDataKey", dsInvoiceSJ);
        parameterMap.put("dsInvoiceSJ_subReportDataKeys", dsInvoiceSJ_sub);
        model = new ModelAndView("pdfReportInvoiceSJ", parameterMap);

        return model;
    }

//    @RequestMapping(value = "/cetakPdf-mutasi", method = RequestMethod.POST)
    @RequestMapping(value = "/psmt", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcMutasi(ModelAndView model) {
        /*
        String[] rc_MemoMutasi = request.getParameterValues("rc_MemoMutasi");
        String[] rc_TglOM = request.getParameterValues("rc_TglOM");
        String[] rc_NomorBuktiOM = request.getParameterValues("rc_NomorBuktiOM");
        String[] rc_TokoOM = request.getParameterValues("rc_TokoOM");
        String[] rc_QtyKirimOM = request.getParameterValues("rc_QtyKirimOM");
        String[] rc_NomorTRMSTOM = request.getParameterValues("rc_NomorTRMSTOM");
        String[] rc_NomorTRDETOM = request.getParameterValues("rc_NomorTRDETOM");
         */

 /* ambil session yang dibuat di ModelAndView prosesMutasi (ManageController) */
        Integer rc_kodeMutasi = Integer.parseInt(session.getAttribute("CPM_TRMST_OM").toString());

        System.out.println("\n======================== ReportController: rc_kodeMutasi (public ModelAndView rcMutasi(ModelAndView model))");
        System.out.println("Session rc_kodeMutasi =" + rc_kodeMutasi);

        //tinggal membenahi query di getDataMutasi dan getDataSuratMutasi di Vpubtrdet08DAOImpl
        JRDataSource dsMutasi = vpubtrdet08Service.getDataMutasi(rc_kodeMutasi);
        JRDataSource dsMutasi_sub = vpubtrdet08Service.getDataMutasi_sub(rc_kodeMutasi);

        Map<String, Object> parameterMap = new HashMap<>();

        //utk ke spring-views.xml property => p:reportDataKey
        parameterMap.put("dsMutasi_reportDataKey", dsMutasi);

        /* untuk:
            1) spring-views.xml property => p:subReportDataKeys;
            2) name parameter dan dataSourceExpression di jrxml yg main/master report;
         */
        parameterMap.put("dsMutasi_subReportDataKeys", dsMutasi_sub);

        System.out.println("\n\n======================== ReportController: parameterMap");
        for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        //untuk bean id di spring-views.xml
        model = new ModelAndView("pdfReportMutasi", parameterMap);

        return model;
    }

//    @RequestMapping(value = "/cetakPdf-terimaRJ", method = RequestMethod.POST)
    @RequestMapping(value = "/ptrj", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcTerimaRJ(ModelAndView model) {
        /* ambil session yang dibuat di ModelAndView prosesTRJ (ManageController) */
        Integer rc_kodeTRJ = Integer.parseInt(session.getAttribute("SESS_cekProsesTRJ").toString());

        System.out.println("\n======================== ReportController: rc_kodeTRJ (public ModelAndView rcTerimaRJ(ModelAndView model))");
        System.out.println("Session rc_kodeTRJ =" + rc_kodeTRJ);

        //tinggal membenahi query di getDataTRJ dan getDataTRJ_sub di Vpubtrdet08DAOImpl
        JRDataSource dsTRJ = vpubtrdet08Service.getDataTRJ(rc_kodeTRJ);
        JRDataSource dsTRJ_sub = vpubtrdet08Service.getDataTRJ_sub(rc_kodeTRJ);

        Map<String, Object> parameterMap = new HashMap<>();

        //utk ke spring-views.xml property => p:reportDataKey
        parameterMap.put("dsTRJ_reportDataKey", dsTRJ);

        /* untuk:
            1) spring-views.xml property => p:subReportDataKeys;
            2) name parameter dan dataSourceExpression di jrxml yg main/master report;
         */
        parameterMap.put("dsTRJ_subReportDataKeys", dsTRJ_sub);

        System.out.println("\n\n======================== ReportController: parameterMap");
        for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        //untuk bean id di spring-views.xml
        model = new ModelAndView("pdfReportTRJ", parameterMap);

        return model;
    }

    //controller jasper InvoiceTRJ
    @RequestMapping(value = "/pinvtrj", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcInvoiceTRJ(ModelAndView model) {
        /* ambil session yang dibuat di ModelAndView prosesTRJ (ManageController) */
        Integer noTRMST_TRJ = Integer.parseInt(session.getAttribute("SESS_cekProsesTRJ").toString());

        JRDataSource dsInvoiceTRJ = vpubtrdet08Service.getDataInvoiceTRJ(noTRMST_TRJ);
        JRDataSource dsInvoiceTRJ_sub = vpubtrdet08Service.getDataInvoiceTRJ_sub(noTRMST_TRJ);

        Map<String, Object> parameterMap = new HashMap<>();

        parameterMap.put("dsInvoiceTRJ_reportDataKey", dsInvoiceTRJ);
        parameterMap.put("dsInvoiceTRJ_subReportDataKeys", dsInvoiceTRJ_sub);
        model = new ModelAndView("pdfReportInvoiceTRJ", parameterMap);

        return model;
    }

//    @RequestMapping(value = "/cetakPdf-terimaRB", method = RequestMethod.POST)
    @RequestMapping(value = "/ptrb", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcTerimaRB(ModelAndView model) {
        /* ambil session yang dibuat di ModelAndView prosesTRB (ManageController) */
        Integer rc_kodeTRB = Integer.parseInt(session.getAttribute("SESS_cekProsesTRB").toString());

        System.out.println("\n======================== ReportController: rc_kodeTRB (public ModelAndView rcTerimaRB(ModelAndView model))");
        System.out.println("Session rc_kodeTRB =" + rc_kodeTRB);

        //tinggal membenahi query di getDataTRB dan getDataTRB_sub di Vpubtrdet08DAOImpl
        JRDataSource dsTRB = vpubtrdet08Service.getDataTRB(rc_kodeTRB);
        JRDataSource dsTRB_sub = vpubtrdet08Service.getDataTRB_sub(rc_kodeTRB);

        Map<String, Object> parameterMap = new HashMap<>();

        //utk ke spring-views.xml property => p:reportDataKey
        parameterMap.put("dsTRB_reportDataKey", dsTRB);

        /* untuk:
            1) spring-views.xml property => p:subReportDataKeys;
            2) name parameter dan dataSourceExpression di jrxml yg main/master report;
         */
        parameterMap.put("dsTRB_subReportDataKeys", dsTRB_sub);

        System.out.println("\n\n======================== ReportController: parameterMap");
        for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        //untuk bean id di spring-views.xml
        model = new ModelAndView("pdfReportTRB", parameterMap);

        return model;
    }

    //controller jasper form LPB
    @RequestMapping(value = "/plpb", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView rcTerimaLPB(ModelAndView model) {
        /* ambil session yang dibuat di ModelAndView prosesLPB (ManageController) */
        Integer rc_kodeLPB = Integer.parseInt(session.getAttribute("SESS_cekProsesLPB").toString());

        System.out.println("\n======================== ReportController: rc_kodeLPB (public ModelAndView rcTerimaLPB(ModelAndView model))");
        System.out.println("Session rc_kodeTRB =" + rc_kodeLPB);

        //tinggal membenahi query di getDataLPB dan getDataLPB_sub di Vpubtrdet08DAOImpl
        JRDataSource dsLPB = vpubtrdet08Service.getDataLPB(rc_kodeLPB);
        JRDataSource dsLPB_sub = vpubtrdet08Service.getDataLPB_sub(rc_kodeLPB);

        Map<String, Object> parameterMap = new HashMap<>();

        //utk ke spring-views.xml property => p:reportDataKey
        parameterMap.put("dsLPB_reportDataKey", dsLPB);

        /* untuk:
            1) spring-views.xml property => p:subReportDataKeys;
            2) name parameter dan dataSourceExpression di jrxml yg main/master report;
         */
        parameterMap.put("dsLPB_subReportDataKeys", dsLPB_sub);

        System.out.println("\n\n======================== ReportController: parameterMap");
        for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        //untuk bean id di spring-views.xml
        model = new ModelAndView("pdfReportLPB", parameterMap);

        return model;
    }
}
