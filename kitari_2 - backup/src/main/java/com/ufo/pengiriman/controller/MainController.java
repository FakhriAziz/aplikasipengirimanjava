package com.ufo.pengiriman.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
/* import class2 ufo pengiriman */
import com.ufo.pengiriman.service.VcekbayartrmstService;
import com.ufo.pengiriman.service.Vpubtrdet08Service;
import java.text.DecimalFormat;
import java.time.Duration;
import org.springframework.ui.Model;

/**
 *
 * @author PROGRAMER
 */
@RestController
//@RequestMapping("/json/data")   //tidak bisa klo tidak class sendiri
public class MainController {

    //private static final Logger logger = Logger.getLogger(MainController.class);
    @Autowired
    VcekbayartrmstService vcekbayartrmstService;

    @Autowired
    Vpubtrdet08Service vpubtrdet08Service;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpSession session;

    
    /**
     * Proses Login Atau Logout
     * @param error
     * @param logout
     * @return 
     */
//    @RequestMapping(value = "/", method = RequestMethod.GET)
    @RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        //instansiasi model
        ModelAndView model = new ModelAndView();

        if (error != null) {
            model.addObject("error", "username dan password tidak sama!");
        }
        
        if (logout != null) {
            session.invalidate();
            model.addObject("msg", "logout");
        }
        
        model.setViewName("login");
        return model;
    }
    
    
    /**
     * TEST JSON SAJA..
     * @return 
     */
    @RequestMapping("/json/beranda")
    public List<Map<Object, Object>> json_beranda() {

        //INFORMASI USER YG LOGIN
        List<String> InfoUser = vcekbayartrmstService.getCekUser();
        Iterator iter = InfoUser.iterator();
        Map map = (Map) iter.next();    //krn hanya satu baris, tdk usah while(iter.hasNext())
        
        Map<Object, Object> data = new HashMap<>();
        data.put("lokasi", map.get("LOKASI"));
        data.put("kodeuser", map.get("KODEUSER"));
        data.put("nomordivisi", map.get("NOMORDIVISI"));
        
        List<Map<Object, Object>> listData = new ArrayList<>();
        listData.add(data); //add to ArrayList listData

        return listData;
    }

    
    /**
     * Proses Menampilkan Halaman Awal (dashboard)
     * @param model
     * @return 
     */
    @RequestMapping(value = "/beranda", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView beranda(ModelAndView model) {
        //INFORMASI USER YG LOGIN
        List<String> InfoUser = vcekbayartrmstService.getCekUser();

        //dapatkan lokasi, kode user, nomor divisi dari user yg login (InfoUser)
        Iterator iter = InfoUser.iterator();
        Map map = (Map) iter.next();    //krn hanya satu baris, tdk usah while(iter.hasNext())
        Object infoLokasi = map.get("LOKASI");  //case-sensitive key, walaupun di query getCekUser "Lokasi"
        Object infoKodeUser = map.get("KODEUSER");
        Object infoNomorDivisi = map.get("NOMORDIVISI");

        /*  //tdk bisa cast java.util.ArrayList ke java.util.Map
            Map map = (Map) InfoUser;
            Map.Entry mapEntry = (Map.Entry) map.entrySet().iterator().next();
            System.out.println("mapEntry.getKey(): " + mapEntry.getKey() +", mapEntry.getValue(): " + mapEntry.getValue());
            
            //tdk bisa cast java.util.HashMap ke java.lang.String
            Map<String,String> map = new HashMap<String,String>();
            for (String i : InfoUser){
                System.out.println("map.get(i): " + map.get(i));
            }
         */
        session.setAttribute("tempUser", InfoUser);     //ini generalnya info user yang sukses login, untuk dipass ke model.addObject("cekUser")
        session.setAttribute("infoLokasi", infoLokasi);             //bikin session lokasi user login (nomor lokasi)
        session.setAttribute("infoKodeUser", infoKodeUser);         //bikin session kode user login
        session.setAttribute("infoNomorDivisi", infoNomorDivisi);   //bikin session NomorDivisi login

        //add object untuk di-bypass ke jsp view
        model.addObject("cekUser", InfoUser);

        //SOUT
        System.out.println("\n========================= InfoUser (induknya)");
        System.out.println("InfoUser: " + InfoUser);
        System.out.println("\n========================= infoLokasi, infoKodeUser, infoNomorDivisi");
        System.out.println("infoLokasi: " + infoLokasi);
        System.out.println("infoKodeUser: " + infoKodeUser);
        System.out.println("infoNomorDivisi: " + infoNomorDivisi);
        
/* sonny, update 16/04/19 set off, MULAI PROSES COUNTER DASHBOARD JUMLAH ORDER SO DAN JUMLAH ORDER MUTASI UNTUK PENGIRIMAN */
//        int jmlSO = 0;
//        int jmlMutasi = 0;
//        int jmlORJ = 0;
//        int jmlORB = 0;
        
        //untuk status jumlah pengiriman dan mutasi di dashboard
//        List<Object> cekDashboard = vcekbayartrmstService.getCountDashboard();

        //dapatkan nilai status jumlah pengiriman dan mutasi di tiap iterasi (cekDashboard)
//        Iterator iter2 = cekDashboard.iterator();

//        while (iter2.hasNext()) {
//            Map map2 = (Map) iter2.next();
//            Short dashbTRANSAKSI = (Short) map2.get("TRANSAKSI");
//            Double dashbSISABAYAR = (Double) map2.get("SISABAYAR");
//
//            if (dashbSISABAYAR == null) {
//                if (dashbTRANSAKSI >= 8500 && dashbTRANSAKSI < 8600) {
//                    jmlMutasi++;
//                }
//                if (dashbTRANSAKSI >= 8600 && dashbTRANSAKSI < 8700) {
//                    jmlORJ++;
//                }
//                if (dashbTRANSAKSI >= 8700 && dashbTRANSAKSI < 8800) {
//                    jmlORB++;
//                }
//            } else if (dashbTRANSAKSI < 8200 && dashbSISABAYAR == 0.0) {
//                jmlSO++;
//            }
//        }

        //add object untuk di-bypass ke jsp view
//        model.addObject("jumlahSO", jmlSO);
//        model.addObject("jumlahMutasi", jmlMutasi);
//        model.addObject("jumlahORJ", jmlORJ);
//        model.addObject("jumlahORB", jmlORB);

        //SOUT
//        System.out.println("\n========================= jmlSO, jmlMutasi");
//        System.out.println("jmlSO = " + jmlSO);
//        System.out.println("jmlMutasi = " + jmlMutasi);
//        System.out.println("jmlORJ = " + jmlORJ);
//        System.out.println("jmlORB = " + jmlORB);


/* sonny, update 16/04/19 set off, MULAI PROSES BATASAN QTY KIRIM GUDANG PER HARINYA */
//        Double totalSJPerHari = vcekbayartrmstService.getTotalSJPerHari();
//        Double totalMTPerHari = vcekbayartrmstService.getTotalMTPerHari();
//        Double totalRJPerHari = vcekbayartrmstService.getTotalRJPerHari();
//        Double totalRBPerHari = vcekbayartrmstService.getTotalRBPerHari();
        
        //var totalBatasKirim
//        Double totalBatasKirim = totalSJPerHari + totalMTPerHari;
        

        //add object untuk di-bypass ke jsp view
//        DecimalFormat df = new DecimalFormat("###.#");
//        model.addObject("totalSJPerHari", df.format(totalSJPerHari));
//        model.addObject("totalMTPerHari", df.format(totalMTPerHari));
//        model.addObject("totalRJPerHari", df.format(totalRJPerHari));
//        model.addObject("totalRBPerHari", df.format(totalRBPerHari));
        
        //add object totalBatasKirim
//        model.addObject("totalBatasKirim", df.format(totalBatasKirim));

        //buat session untuk dipanggil di method lain
//        session.setAttribute("totalBatasKirim", df.format(totalBatasKirim));

        //SOUT
//        System.out.println("\n========================= TOTAL TRANSAKSI KIRIM HARI INI (MainController: public ModelAndView beranda)");
//        System.out.println("totalSJPerHari = " + df.format(totalSJPerHari));
//        System.out.println("totalMTPerHari = " + df.format(totalMTPerHari));
//        System.out.println("totalRJPerHari = " + df.format(totalRJPerHari));
//        System.out.println("totalRBPerHari = " + df.format(totalRBPerHari));
//        System.out.println("totalBatasKirim = " + df.format(totalBatasKirim));
        
        //set view name (file jsp)
        model.setViewName("beranda");
        
        /*
        //model.addObject("mode", "dashboard");     //off krn tidak diperlukan
        //COBA PRINT SEMUA SESSION 
        Enumeration enumSession = (Enumeration) session.getAttributeNames();
        System.out.println("\n\n============$$$$$$$============ Print All Sessions (Enumeration) ============$$$$$$$============");
        System.out.println("enumSession: " + enumSession);
        while (enumSession.hasMoreElements()) {
            Object objSess = enumSession.nextElement();
            if(objSess != null) {
                System.out.println("\nenumSession.nextElement(): " + (String) objSess);
                System.out.println("Session ID: " + session.getId());
                System.out.println("Session Attribute: " + session.getAttribute((String) objSess));
            }
        }
        */
        
        return model;
    }
    
    /**
     * TEST JSON untuk data Daftar Kirim Penjualan
     * @return 
     */
    @RequestMapping("/json/list")
    public List<Map<Object, Object>> json_listDO(List<String> listSO){
        System.out.println("\n+++++++++++++--------- CEK json_listDO");
        
        Iterator iter = listSO.iterator();

        List<Map<Object, Object>> listData = new ArrayList<>();
        
        while(iter.hasNext()){
            Map map = (Map) iter.next();
            
            Map<Object, Object> data = new HashMap<>();
            
            data.put("tgl_so", map.get("TGL_SO"));
            data.put("nomorbukti_so", map.get("NOMORBUKTI_SO"));
            data.put("tgl_ir", map.get("TGL_IR"));
            data.put("nomorbukti_ir", map.get("NOMORBUKTI_IR"));
            data.put("toko_so", map.get("TOKO_SO"));
            data.put("kodestok_so", map.get("KODESTOK_SO"));
            data.put("namastok_so", map.get("NAMASTOK_SO"));
            data.put("qty_so", map.get("QTY_SO"));
            data.put("ket_so", map.get("KET_SO"));
            data.put("subket_so", map.get("SUBKET_SO"));
            data.put("namacustomer_so", map.get("NAMACUSTOMER_SO"));
            data.put("alamatcustomer_so", map.get("ALAMATCUSTOMER_SO"));
            data.put("telpcustomer_so", map.get("TELPCUSTOMER_SO"));
            data.put("nomorproglama", map.get("NOMORPROGLAMA"));
            
            /*--- mulai gudang smpe tglkirim_so, mungkin tdk perlu krn di jsp hidden input type ---*/
            data.put("gudang_sj", map.get("GUDANG_SJ"));
            data.put("nomortrmst_so", map.get("NOMORTRMST_SO"));
            data.put("nomortrdet_so", map.get("NOMORTRDET_SO"));
            data.put("tglkirim_so", map.get("TGLKIRIM_SO"));
            
            data.put("spm", map.get("SPM"));
            data.put("nama_supir", map.get("NAMA")); //FAF, Tambahan
            data.put("no_plat", map.get("PLAT")); //FAF, Tambahan
            listData.add(data); //add to ArrayList listData
        }
        
        return listData;
    }

    /**
     * Proses Menampilkan Serta Pengecekan Daftar Kirim Penjualan
     * @param model
     * @param ApproveKirim
     * @param tglAwal
     * @param tglAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listDO(
            ModelAndView model,
            @RequestParam(value = "btnKirim", required = false) String ApproveKirim,
            String tglAwal,
            String tglAkhir) throws ParseException {
        
        List<Map<Object, Object>> listDataJson = new ArrayList<>(); //untuk JSON
                        
        if (ApproveKirim != null) {
            //ArrayList<String> SO = new ArrayList<>();     //off karena tidak digunakan
            String cekTgl1 = tglAwal;
            String cekTgl2 = tglAkhir;
            
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTgl1) && !"".equals(cekTgl2)) {
                Date Dtglawal = df.parse(tglAwal);
                Date Dtglakhir = df.parse(tglAkhir);
                
                //sonny, add logic utk dapatkan durasi dan selisih antara dua tanggal, lalu lempar sbg param utk limit query tarik list SO
                Duration durasi = Duration.between(Dtglawal.toInstant(), Dtglakhir.toInstant());
                short selisihTgl = (short) durasi.toDays();
                
                if (Dtglakhir.compareTo(Dtglawal) != 0) {
                    if (Dtglakhir.compareTo(Dtglawal) > 0) {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        
                        //sonny, add info: query tarik list SO utk pengiriman dengan input tanggal awal dan tanggal akhir
                        //sonny, add param ke-3 sbg flag untuk limit query
                        List<String> listSO = vcekbayartrmstService.getTglSO(df2.format(Dtglawal), df2.format(Dtglakhir), selisihTgl);
                        int jmlDataListSO = listSO.size();
                        model.addObject("datakirim", listSO);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListSO");
                        System.out.println("jmlDataListSO = " + jmlDataListSO);
                        
                        //call json_listDO 
                        listDataJson = json_listDO(listSO);
                        System.out.println("listDataJson = " + listDataJson);
                        
                        model.addObject("datakirimJson", listDataJson);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        //sonny, add info: default query tarik list SO utk pengiriman dari 1 hari terakhir
        else {
            List<String> daftarSO = vcekbayartrmstService.getCekBayarTrmst();
            int jmlDataDaftarSO = daftarSO.size();
            model.addObject("datakirim", daftarSO);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarSO");
            System.out.println("jmlDataDaftarSO = " + jmlDataDaftarSO);
            
            //call json_listDO 
            listDataJson = json_listDO(daftarSO);
            System.out.println("listDataJson = " + listDataJson);

            model.addObject("datakirimJson", listDataJson);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));   //utk status di navbar.jsp => pageContext.request.contextPath

//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("listkirim");
        return model;
    }

    /**
     * Proses Pengecekan Untuk Daftar Kirim Penjualan Terpilih untuk dikirim (menjadi SJ)
     * @param model
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/kirim", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView kirim(ModelAndView model) throws ParseException {
        System.out.println("\n======================= MainController: jmlDataKirimSO (public ModelAndView kirim(ModelAndView model))");
        String cekNoBukti = "";
        String cekNama = "";
        String cekAlamat = "";
        List<Map<Object, Object>> listData = new ArrayList<>();
        
        if (request.getParameterValues("getSPM") != null) {
            String[] getTglSO = request.getParameterValues("getTglSO");                 //#1 request
            String[] getNomorBuktiSO = request.getParameterValues("getNomorBuktiSO");   //#2 request

            //sonny, add tanggal bayar dan nobukti pembayaran
            String[] getTglIR = request.getParameterValues("getTglIR");                 //#3 request
            String[] getNomorBuktiIR = request.getParameterValues("getNomorBuktiIR");   //#4 request

            String[] getTokoSO = request.getParameterValues("getTokoSO");               //#5 request
            String[] getKodeStokSO = request.getParameterValues("getKodeStokSO");       //#6 request
            String[] getNamaStokSO = request.getParameterValues("getNamaStokSO");       //#7 request
            String[] getQtyKirimSO = request.getParameterValues("getQtyKirimSO");       //#8 request
            String[] getQtySO = request.getParameterValues("getQtySO");                 //#9 request, qty order/qty SO
            String[] getKetSO = request.getParameterValues("getKetSO");                 //#10 request, keterangan SO yg master
            String[] getSubKetSO = request.getParameterValues("getSubKetSO");           //#11 request, keterangan SO yg detail
            //JENIS BARANG DIHAPUS
            String[] getNamaCustomerSO = request.getParameterValues("getNamaCustomerSO");       //#12 request
            String[] getAlamatCustomerSO = request.getParameterValues("getAlamatCustomerSO");   //#13 request
            String[] getTelpCustomerSO = request.getParameterValues("getTelpCustomerSO");       //#14 request
            String[] getNomorProgLama = request.getParameterValues("getNomorProgLama");         //#15 request
            String[] getGudangSJ = request.getParameterValues("getGudangSJ");                   //#16 request, gudang kirim
            String[] getNomorTRMSTSO = request.getParameterValues("getNomorTRMSTSO");           //#17 request
            String[] getNomorTRDETSO = request.getParameterValues("getNomorTRDETSO");           //#18 request
            String[] getTglKirimSO = request.getParameterValues("getTglKirimSO");               //#19 request
            String[] getSPM = request.getParameterValues("getSPM");                     //#20 request, NAMA SPM utk get session jasper invoice
            String[] getNAMA_DRIVER = request.getParameterValues("getNAMA_DRIVER");       //#21 request FAF, Tambahan
            String[] getNO_PLAT = request.getParameterValues("getNO_PLAT");       //#22 request FAF, Tambahan

            
            int jmlDataKirimSO = getSPM.length;
            System.out.println("jmlDataKirimSO = " + jmlDataKirimSO);

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            //for, di-looping setiap baris data list daftar SO yang akan dikirim. jika difilter dulu dgn pencarian maka jmlData menyesuaikan/lebih sedikit.
            for (int i = 0; i < jmlDataKirimSO; i++) {
                //jika qty kirim tdk sm dgn 0 
//            if (Integer.parseInt(getQtyKirimSO[i]) != 0) {
                if (Float.parseFloat(getQtyKirimSO[i]) != 0) {  //ubah ke parseFloat untuk tipe pecahan
                    //jika nama customer bukan CASH
                    if (!"CASH".equals(getNamaCustomerSO[i].trim())) {
                        /* if berikut utk kondisi pertama yg or dulu yg dijalankan, setelah itu membandingkan cekNama dengan nama[i] di tiap iterasi yg terpenuhi
                    //eric membingungkan => if (cekNama.trim().equalsIgnoreCase(nama[i].trim()) || "".equals(cekNama) && "".equals(cekKode)) {
                         */
                        if (cekNama.trim().equalsIgnoreCase(getNamaCustomerSO[i].trim()) || "".equals(cekNama)) {
                            System.out.println("\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ SEBELUM ASSIGN VALUE");
                            System.out.println("cekNama = " + cekNama);
                            System.out.println("getNamaCustomerSO[" + i + "] = " + getNamaCustomerSO[i]);

                            cekNama = getNamaCustomerSO[i].trim();   //trik validasi cek nama customer harus sama

                            System.out.println("\n%%%%%%%%%%%%%%%%%%%%%%%%% SETELAH ASSIGN VALUE");
                            System.out.println("cekNama = " + cekNama);
                            System.out.println("getNamaCustomerSO[" + i + "] = " + getNamaCustomerSO[i]);

                            if (cekAlamat.trim().equalsIgnoreCase(getAlamatCustomerSO[i].trim()) || "".equals(cekAlamat)) {
                                System.out.println("\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ SEBELUM ASSIGN VALUE");
                                System.out.println("cekAlamat = " + cekAlamat);
                                System.out.println("getAlamatCustomerSO[" + i + "] = " + getAlamatCustomerSO[i]);

                                cekAlamat = getAlamatCustomerSO[i].trim();   //trik validasi cek alamat customer harus sama

                                System.out.println("\n%%%%%%%%%%%%%%%%%%%%%%%%% SETELAH ASSIGN VALUE");
                                System.out.println("cekAlamat = " + cekAlamat);
                                System.out.println("getAlamatCustomerSO[" + i + "] = " + getAlamatCustomerSO[i]);

                                Map<Object, Object> data = new HashMap<>();
                                //#1
                                Date DTglSO = df.parse(getTglSO[i]);
                                data.put("mapTglSO", DTglSO);
                                //#2
                                data.put("mapNomorBuktiSO", getNomorBuktiSO[i]);

                                //sonny, add tanggal bayar dan nobukti pembayaran
                                //#3
                                System.out.println("\n\n ********** getNAMA_DRIVER[i] yg bawah = " + getNAMA_DRIVER[i]);
                                System.out.println("\n\n ********** getNO_PLAT[i] yg bawah = " + getNO_PLAT[i]);
                                System.out.println("\n\n ********** getTglIR[i] yg atas = " + getTglIR[i]);
                                if (getTglIR[i].trim().equals("")) {
                                    data.put("mapTglIR", getTglIR[i]);
                                } else {
                                    Date DTglIR = df.parse(getTglIR[i]);
                                    data.put("mapTglIR", DTglIR);
                                }
                                //#4
                                data.put("mapNomorBuktiIR", getNomorBuktiIR[i]);

                                //#5
                                data.put("mapTokoSO", getTokoSO[i]);
                                //#6
                                data.put("mapKodeStokSO", getKodeStokSO[i]);
                                //#7
                                data.put("mapNamaStokSO", getNamaStokSO[i]);
                                //#8
                                data.put("mapQtyKirimSO", getQtyKirimSO[i]);
                                //#9
                                data.put("mapQtySO", getQtySO[i]);
                                //#10
                                data.put("mapKetSO", getKetSO[i]);
                                //#11
                                data.put("mapSubKetSO", getSubKetSO[i]);
                                //#12
                                data.put("mapNamaCustomerSO", getNamaCustomerSO[i]);
                                //#13
                                data.put("mapAlamatCustomerSO", getAlamatCustomerSO[i]);
                                //#14
                                data.put("mapTelpCustomerSO", getTelpCustomerSO[i]);
                                //#15
                                data.put("mapNomorProgLama", getNomorProgLama[i]);
                                //#16
                                data.put("mapGudangSJ", getGudangSJ[i]);
                                //#17
                                data.put("mapNomorTRMSTSO", getNomorTRMSTSO[i]);
                                //#18
                                data.put("mapNomorTRDETSO", getNomorTRDETSO[i]);
                                //#19
                                Date DTglKirimSO = df.parse(getTglKirimSO[i]);
                                data.put("mapTglKirimSO", DTglKirimSO);
                                //#20
                                data.put("mapSPM", getSPM[i]);
                                //#21
                                data.put("mapNAMA_DRIVER", getNAMA_DRIVER[i]); //FAF, Tambahan
                                //#22
                                data.put("mapNO_PLAT", getNO_PLAT[i]); //FAF, Tambahan
                                
                                System.out.println("\n\n ********** GET DATA1 = " + data);
                                
                                listData.add(data); //add to ArrayList listData (diatas, baris 304)
                            }
                            //JIKA ALAMAT SEBELUMNYA TIDAK SAMA DGN ALAMAT SEKARANG
                            else {
                                Arrays.fill(getNomorBuktiSO, null);
                                model.addObject("errAlamat", true);
                                model.addObject("errList", "Mohon Cek Beda Alamat: " + getAlamatCustomerSO[i]);
                                break;
                            }
                        }
                        //JIKA NAMA SEBELUMNYA TIDAK SAMA DGN NAMA SEKARANG
                        else {
                            Arrays.fill(getNomorBuktiSO, null);
                            model.addObject("errNama", true);
                            model.addObject("errList", "Mohon Cek Beda Nama: " + getNamaCustomerSO[i]);
                            break;
                        }
                    }
                    //VALIDASI JIKA NAMA CUSTOMER CASH ATAU STRING KOSONG
                    else if ("".equals(cekNama)) {
                        /* VALIDASI CEKNOBUKTI (NOMOR BUKTI SO)
                    //eric, membingungkan -> if (kode[i] == null ? kode[i] == null : cekKode.trim().equals(kode[i].trim()) || "".equals(cekKode)) {
                         */
                        //JIKA CEKNOBUKTI SEBELUMNYA SAMA DGN CEKNOBUKTI SEKARANG ATAU JIKA CEKNOBUKTI STRING KOSONG
                        if (cekNoBukti.trim().equalsIgnoreCase(getNomorBuktiSO[i].trim()) || "".equals(cekNoBukti)) {
                            cekNoBukti = getNomorBuktiSO[i].trim();  //TRIK VALIDASI CEK NOMOR BUKTI
                            Map<Object, Object> data = new HashMap<>();
                            //#1
                            Date DTglSO = df.parse(getTglSO[i]);
                            data.put("mapTglSO", DTglSO);
                            //#2
                            data.put("mapNomorBuktiSO", getNomorBuktiSO[i]);

                            //sonny, add cek tanggal bayar dan nobukti pembayaran
                            //#3
                            System.out.println("\n\n ********** getTglIR[i] yg bawah = " + getTglIR[i]);
                            if (getTglIR[i].trim().equals("")) {
                                data.put("mapTglIR", getTglIR[i]);
                            } else {
                                Date DTglIR = df.parse(getTglIR[i]);
                                data.put("mapTglIR", DTglIR);
                            }
                            //#4
                            data.put("mapNomorBuktiIR", getNomorBuktiIR[i]);

                            //#5
                            data.put("mapTokoSO", getTokoSO[i]);
                            //#6
                            data.put("mapKodeStokSO", getKodeStokSO[i]);
                            //#7
                            data.put("mapNamaStokSO", getNamaStokSO[i]);
                            //#8
                            data.put("mapQtyKirimSO", getQtyKirimSO[i]);
                            //#9
                            data.put("mapQtySO", getQtySO[i]);
                            //#10
                            data.put("mapKetSO", getKetSO[i]);
                            //#11
                            data.put("mapSubKetSO", getSubKetSO[i]);
                            //#12
                            data.put("mapNamaCustomerSO", getNamaCustomerSO[i]);
                            //#13
                            data.put("mapAlamatCustomerSO", getAlamatCustomerSO[i]);
                            //#14
                            data.put("mapTelpCustomerSO", getTelpCustomerSO[i]);
                            //#15
                            data.put("mapNomorProgLama", getNomorProgLama[i]);
                            //#16
                            data.put("mapGudangSJ", getGudangSJ[i]);
                            //#17
                            data.put("mapNomorTRMSTSO", getNomorTRMSTSO[i]);
                            //#18
                            data.put("mapNomorTRDETSO", getNomorTRDETSO[i]);
                            //#19
                            Date DTglKirimSO = df.parse(getTglKirimSO[i]);
                            data.put("mapTglKirimSO", DTglKirimSO);
                            //#20
                            data.put("mapSPM", getSPM[i]);
                            //#21
                            data.put("mapNAMA_DRIVER", getNAMA_DRIVER[i]); //FAF, Tambahan
                            //#22
                            data.put("mapNO_PLAT", getNO_PLAT[i]); //FAF, Tambahan
                            
                            System.out.println("\n\n ********** GET DATA2 = " + data);

                            listData.add(data); //add to ArrayList listData (diatas, baris 304)
                        }
                        //JIKA CEKNOBUKTI SEBELUMNYA TIDAK SAMA DGN CEKNOBUKTI SEKARANG
                        else {
                            Arrays.fill(getNomorBuktiSO, null);
                            model.addObject("errNomorBukti", true);
                            model.addObject("errList", "Nomor Bukti Harus Sama Untuk Nama Customer CASH");
                            break;
                        }
                    }
                    //JIKA NAMA BUKAN STRING KOSONG DAN NAMA CASH KETEMU DGN NAMA SELAIN CASH
                    else {
                        Arrays.fill(getNomorBuktiSO, null);
                        model.addObject("errNama", true);
                        model.addObject("errList", "Mohon Cek Beda Nama: " + getNamaCustomerSO[i]);
                        break;
                    }
                }
            } //end for loop
            
            if (getNomorBuktiSO[0] != null) {
                System.out.println("\n\n ********** GET DATA3 = " + listData);

                model.addObject("daftarkirim", listData);
                model.setViewName("datapengirman");
            }
            else {
                model.setViewName("error");
            }
        }
        return model;
    }

    /**
     * Proses logic action dari klik lihat detail di halaman Daftar Surat Jalan
     * @param idBarang
     * @param model
     * @return 
     */
//    @RequestMapping(value = "/findBarang", method = RequestMethod.GET, params = {"idBukti"})
    @RequestMapping(value = "/findBarang", method = {RequestMethod.POST, RequestMethod.GET}, params = {"idBukti"})
    public ModelAndView detailBarang(
            @RequestParam(value = "idBukti", required = true) String idBarang,
            ModelAndView model) {
        List<String> InfoBarang = vcekbayartrmstService.getBarang(idBarang);
        model.addObject("detailBarang", InfoBarang);
        model.setViewName("detail");
        return model;
    }
    
    /**
     * Proses Menampilkan Serta Pengecekan Daftar Surat Jalan
     * @param model
     * @param modelSon
     * @param ApproveSurat
     * @param tglSuratAwal
     * @param tglSuratAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listSurat", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView listSuratjalan(
            ModelAndView model,
            Model modelSon,
            @RequestParam(value = "btnSurat", required = false) String ApproveSurat,
            String tglSuratAwal,
            String tglSuratAkhir) throws ParseException {

        //get nilai tgl1_deleteBarang dan tgl2_deleteBarang dari deleteBarang (ManageController). modelSon object dari org.springframework.ui.Model
        String tgl1 = (String) modelSon.asMap().get("tgl1_deleteBarang");
        String tgl2 = (String) modelSon.asMap().get("tgl2_deleteBarang");
        
        //jika ada klik btnSurat ATAU jika ada nilai tgl1 dan tgl2 dari deleteBarang (ManageController). request hanya sekali jalan, bukan session.
        if (ApproveSurat != null || (tgl1 != null && tgl2 != null)) {
            
            System.out.println("\n========================= MainController: nilai tgl1 dan tgl2 dari deleteBarang (ManageController)");
            System.out.println("tgl1 = " + tgl1);
            System.out.println("tgl2 = " + tgl2);
            
            //assign var cekTglSurat1 dan cekTglSurat2 secara kondisional
            String cekTglSurat1 = (tgl1 != null) ? tgl1 : tglSuratAwal;
            String cekTglSurat2 = (tgl2 != null) ? tgl2 : tglSuratAkhir;

            System.out.println("\n========================= MainController: cekTglSurat1 dan cekTglSurat2");
            System.out.println("cekTglSurat1 = " + cekTglSurat1);
            System.out.println("cekTglSurat2 = " + cekTglSurat2);

            //format pertama
            DateFormat dfTgl = new SimpleDateFormat("dd/MM/yyyy");

            if (!"".equals(cekTglSurat1) && !"".equals(cekTglSurat2)) {
                //mekanisme date yg rumit dan njelimett. bisa disederhanakan?
                Date DtglawalSurat = dfTgl.parse(cekTglSurat1);
                Date DtglakhirSurat = dfTgl.parse(cekTglSurat2);

                if (DtglakhirSurat.compareTo(DtglawalSurat) != 0) {
                    if (DtglakhirSurat.compareTo(DtglawalSurat) > 0) {
                        //format kedua
                        DateFormat dfTgl2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        List<String> listSuratJalan = vpubtrdet08Service.getTglSuratJalan(dfTgl2.format(DtglawalSurat), dfTgl2.format(DtglakhirSurat));
                        int jmlDataListSuratJalan = listSuratJalan.size();
                        model.addObject("listsurat", listSuratJalan);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListSuratJalan");
                        System.out.println("jmlDataListSuratJalan = " + jmlDataListSuratJalan);
                        System.out.println("DataListSuratJalan = " + listSuratJalan);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarSuratJalan = vpubtrdet08Service.getSuratJalan();
            int jmlDataDaftarSuratJalan = daftarSuratJalan.size();
            model.addObject("listsurat", daftarSuratJalan);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarSuratJalan");
            System.out.println("jmlDataListSuratJalan = " + jmlDataDaftarSuratJalan);
            System.out.println("DataListSuratJalan = " + daftarSuratJalan);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("listsuratjalan");
        return model;
    }

    /**
     * Proses Menampilkan Serta Pengecekan Daftar Laporan Pengiriman
     * @param model
     * @param tglLaporanAwal
     * @param tglLaporanAkhir
     * @param ApproveReport
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listReport", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listReport(
            ModelAndView model,
            String tglLaporanAwal,
            String tglLaporanAkhir,
            @RequestParam(value = "btnReport", required = false) String ApproveReport) throws ParseException {
        if (ApproveReport != null) {
            String cekTglLaporan1 = tglLaporanAwal;
            String cekTglLaporan2 = tglLaporanAkhir;
            DateFormat dfTglLaporan = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTglLaporan1) && !"".equals(cekTglLaporan2)) {
                Date DtglawalLaporan = dfTglLaporan.parse(tglLaporanAwal);
                Date DtglakhirLaporan = dfTglLaporan.parse(tglLaporanAkhir);
                if (DtglakhirLaporan.compareTo(DtglawalLaporan) != 0) {
                    if (DtglakhirLaporan.compareTo(DtglawalLaporan) > 0) {
                        DateFormat dfTglLaporan2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        List<String> listTglReportSJPenjualan = vpubtrdet08Service.getTglReport(dfTglLaporan2.format(DtglawalLaporan), dfTglLaporan2.format(DtglakhirLaporan));
                        int jmlDataListTglReportSJPenjualan = listTglReportSJPenjualan.size();
                        model.addObject("datalaporan", listTglReportSJPenjualan);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListTglReportSJPenjualan");
                        System.out.println("jmlDataListTglReportSJPenjualan = " + jmlDataListTglReportSJPenjualan);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarReportSJPenjualan = vpubtrdet08Service.getReport();
            int jmlDataDaftarReportSJPenjualan = daftarReportSJPenjualan.size();
            model.addObject("datalaporan", daftarReportSJPenjualan);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarReportSJPenjualan");
            System.out.println("jmlDataDaftarReportSJPenjualan = " + jmlDataDaftarReportSJPenjualan);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off        
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("report");
        return model;
    }

    /**
     * Proses logic menampilkan serta pengecekan Daftar Order Mutasi
     * @param model
     * @param ApproveKirim
     * @param tglAwal
     * @param tglAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listMutasi", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listMutasi(
            ModelAndView model,
            @RequestParam(value = "btnMutasi", required = false) String ApproveKirim,
            String tglAwal,
            String tglAkhir) throws ParseException {
        if (ApproveKirim != null) {
            String cekTgl1 = tglAwal;
            String cekTgl2 = tglAkhir;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTgl1) && !"".equals(cekTgl2)) {
                Date Dtglawal = df.parse(tglAwal);
                Date Dtglakhir = df.parse(tglAkhir);
                
                //sonny, add logic utk dapatkan durasi dan selisih antara dua tanggal, lalu lempar sbg param utk limit query tarik list OM
                Duration durasi = Duration.between(Dtglawal.toInstant(), Dtglakhir.toInstant());
                short selisihTgl = (short) durasi.toDays();
                
                if (Dtglakhir.compareTo(Dtglawal) != 0) {
                    if (Dtglakhir.compareTo(Dtglawal) > 0) {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        
                        //sonny, add info: query tarik list OM utk pengiriman dengan input tanggal awal dan tanggal akhir
                        //sonny, add param ke-3 sbg flag untuk limit query
                        List<String> listMutasi = vcekbayartrmstService.getTglMutasi(df2.format(Dtglawal), df2.format(Dtglakhir), selisihTgl);
                        int jmlDataListMutasi = listMutasi.size();
                        model.addObject("datamutasi", listMutasi);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListMutasi");
                        System.out.println("jmlDataListMutasi = " + jmlDataListMutasi);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        //sonny, add info: default query tarik list OM utk pengiriman mutasi dari 1 hari terakhir
        else {
            List<String> daftarMutasi = vcekbayartrmstService.getCekMutasi();
            int jmlDataDaftarMutasi = daftarMutasi.size();
            model.addObject("datamutasi", daftarMutasi);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarMutasi");
            System.out.println("jmlDataDaftarMutasi = " + jmlDataDaftarMutasi);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("list-mutasi");
        return model;
    }

    /**
     * Proses Pengecekan dan Pemuatan Daftar Mutasi Terpilih untuk dikirim
     * @param model
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/kirimMutasi", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView kirimMutasi(ModelAndView model) throws ParseException {
        List<Map<Object, Object>> list_kirimMutasi = new ArrayList<>();

        String[] getTglOM = request.getParameterValues("getTglOM");                 //#1 request
        String[] getNomorBuktiOM = request.getParameterValues("getNomorBuktiOM");   //#2 request
        String[] getTokoOM = request.getParameterValues("getTokoOM");               //#3 request
        String[] getKodeStokOM = request.getParameterValues("getKodeStokOM");       //#4 request
        String[] getNamaStokOM = request.getParameterValues("getNamaStokOM");       //#5 request
        String[] getQtyKirimOM = request.getParameterValues("getQtyKirimOM");       //#6 request
        String[] getQtyOM = request.getParameterValues("getQtyOM");                 //#7 request
        String[] getKetOM = request.getParameterValues("getKetOM");                 //#8 request
        String[] getSubKetOM = request.getParameterValues("getSubKetOM");           //#9 request
        String[] getNomorTRMSTOM = request.getParameterValues("getNomorTRMSTOM");   //#10 request
        String[] getNomorTRDETOM = request.getParameterValues("getNomorTRDETOM");   //#11 request

        int jmlDataKirimOM = getNomorBuktiOM.length;  //jml length sama semua karena 1 String[] ekivalen 1 baris data list

        System.out.println("\n========================= MainController: jmlDataKirimOM (public ModelAndView kirimMutasi(ModelAndView model))");
        System.out.println("jmlData : " + jmlDataKirimOM);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        //for, di-looping setiap baris data list. jika difilter dulu dgn "cari data" maka jmlData akan menyesuaikan/iterasi lebih sedikit.
        for (int i = 0; i < jmlDataKirimOM; i++) {
            /* trik untuk index agar record data di view data-mutasi.jsp jd urut asc per page (revert ordering) jika pake jquery append/prepend
            int iterJmlData = (jmlData - 1) - i;
             */
            //JIKA QTY KIRIM TIDAK SAMA DGN 0 
            if (Integer.parseInt(getQtyKirimOM[i]) != 0) {
                Map<Object, Object> data = new HashMap<>();
                //#1
                Date DTglOM = df.parse(getTglOM[i]);
                data.put("mapTglOM", DTglOM);
                //#2
                data.put("mapNomorBuktiOM", getNomorBuktiOM[i]);
                //#3
                data.put("mapTokoOM", getTokoOM[i]);
                //#4
                data.put("mapKodeStokOM", getKodeStokOM[i]);
                //#5
                data.put("mapNamaStokOM", getNamaStokOM[i]);
                //#6
                data.put("mapQtyKirimOM", getQtyKirimOM[i]);
                //#7
                data.put("mapQtyOM", getQtyOM[i]);
                //#8
                data.put("mapKetOM", getKetOM[i]);
                //#9
                data.put("mapSubKetOM", getSubKetOM[i]);
                //#10
                data.put("mapNomorTRMSTOM", getNomorTRMSTOM[i]);
                //#11
                data.put("mapNomorTRDETOM", getNomorTRDETOM[i]);
                list_kirimMutasi.add(data);
            }
        }
        model.addObject("daftarKirimMutasi", list_kirimMutasi);
        model.setViewName("data-mutasi");
        return model;
    }

    /**
     * Proses Menampilkan Serta Pengecekan Daftar Laporan Pengiriman Mutasi
     * @param model
     * @param tglLaporanAwal
     * @param tglLaporanAkhir
     * @param ApproveReport
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listLapMutasi", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listLapMutasi(
            ModelAndView model,
            String tglLaporanAwal,
            String tglLaporanAkhir,
            @RequestParam(value = "btnReport", required = false) String ApproveReport) throws ParseException {
        if (ApproveReport != null) {
            String cekTglLaporan1 = tglLaporanAwal;
            String cekTglLaporan2 = tglLaporanAkhir;
            DateFormat dfTglLaporan = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTglLaporan1) && !"".equals(cekTglLaporan2)) {
                Date DtglawalLaporan = dfTglLaporan.parse(tglLaporanAwal);
                Date DtglakhirLaporan = dfTglLaporan.parse(tglLaporanAkhir);
                if (DtglakhirLaporan.compareTo(DtglawalLaporan) != 0) {
                    if (DtglakhirLaporan.compareTo(DtglawalLaporan) > 0) {
                        DateFormat dfTglLaporan2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        List<String> listLapMutasi = vpubtrdet08Service.getTglLapMutasi(dfTglLaporan2.format(DtglawalLaporan), dfTglLaporan2.format(DtglakhirLaporan));
                        int jmlDataListLapMutasi = listLapMutasi.size();
                        model.addObject("dataLapMutasi", listLapMutasi);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListLapMutasi");
                        System.out.println("jmlDataListLapMutasi = " + jmlDataListLapMutasi);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarReportMutasi = vpubtrdet08Service.getLapMutasi();
            int jmlDataDaftarReportMutasi = daftarReportMutasi.size();
            model.addObject("dataLapMutasi", daftarReportMutasi);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarReportMutasi");
            System.out.println("jmlDataDaftarReportMutasi = " + jmlDataDaftarReportMutasi);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("report-mutasi");
        return model;
    }

    /**
     * Proses logic menampilkan dan pengecekan Daftar Order Retur Jual
     * @param model
     * @param ApproveKirim
     * @param tglAwal
     * @param tglAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listORJ", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listORJ(
            ModelAndView model,
            @RequestParam(value = "btnORJ", required = false) String ApproveKirim,
            String tglAwal,
            String tglAkhir) throws ParseException {
        if (ApproveKirim != null) {
            String cekTgl1 = tglAwal;
            String cekTgl2 = tglAkhir;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTgl1) && !"".equals(cekTgl2)) {
                Date Dtglawal = df.parse(tglAwal);
                Date Dtglakhir = df.parse(tglAkhir);
                if (Dtglakhir.compareTo(Dtglawal) != 0) {
                    if (Dtglakhir.compareTo(Dtglawal) > 0) {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        List<String> listORJ = vcekbayartrmstService.getTglORJ(df2.format(Dtglawal), df2.format(Dtglakhir));
                        int jmlDataListORJ = listORJ.size();
                        model.addObject("dataORJ", listORJ);

                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListORJ");
                        System.out.println("jmlDataListORJ = " + jmlDataListORJ);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarORJ = vcekbayartrmstService.getCekORJ();
            int jmlDataDaftarORJ = daftarORJ.size();
            model.addObject("dataORJ", daftarORJ);

            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarORJ");
            System.out.println("jmlDataDaftarORJ = " + jmlDataDaftarORJ);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));

//sonny, update 16/04/19 set off        
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("list-retur-jual");
        return model;
    }
    
    /**
     * Proses Pengecekan dan Pemuatan Daftar ORJ Terpilih untuk dikirim (menjadi TRJ)
     * @param model
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/terimaRJ", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView terimaRJ(ModelAndView model) throws ParseException {
        List<Map<Object, Object>> list_terimaRJ = new ArrayList<>();

        String[] getTglORJ = request.getParameterValues("getTglORJ");                 //#1 request
        String[] getNomorBuktiORJ = request.getParameterValues("getNomorBuktiORJ");   //#2 request
        String[] getTokoORJ = request.getParameterValues("getTokoORJ");               //#3 request
        String[] getKodeStokORJ = request.getParameterValues("getKodeStokORJ");       //#4 request
        String[] getNamaStokORJ = request.getParameterValues("getNamaStokORJ");       //#5 request
        String[] getQtyTRJ = request.getParameterValues("getQtyTRJ");                 //#6 request
        String[] getQtyORJ = request.getParameterValues("getQtyORJ");                 //#7 request
        String[] getKetORJ = request.getParameterValues("getKetORJ");                 //#8 request
        String[] getNomorTRMSTORJ = request.getParameterValues("getNomorTRMSTORJ");   //#9 request
        String[] getNomorTRDETORJ = request.getParameterValues("getNomorTRDETORJ");   //#10 request
        
        //UPDATE 2019-11-29: add data trmstph agar tdk hardcode lagi (#11 - #13)
        String[] getNamaTRMSTPHORJ = request.getParameterValues("getNamaTRMSTPHORJ");       //#11 request
        String[] getAlamatTRMSTPHORJ = request.getParameterValues("getAlamatTRMSTPHORJ");   //#12 request
        String[] getTelpTRMSTPHORJ = request.getParameterValues("getTelpTRMSTPHORJ");       //#13 request

        int jmlDataTRJ = getNomorBuktiORJ.length;  //jml length sama semua karena 1 String[] ekivalen 1 baris data list

        System.out.println("\n========================= MainController: jmlDataTRJ (public ModelAndView terimaRJ(ModelAndView model))");
        System.out.println("jmlData : " + jmlDataTRJ);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        //for, di-looping setiap baris data list. jika difilter dulu dgn "cari data" maka jmlData akan menyesuaikan/iterasi lebih sedikit.
        for (int i = 0; i < jmlDataTRJ; i++) {
            /* trik untuk index agar record data di view data-retur-jual.jsp jd urut asc per page (revert ordering) jika pake jquery append/prepend
            int iterJmlData = (jmlData - 1) - i;
             */
            //JIKA QTY TERIMA RETUR JUAL TIDAK SAMA DGN 0 
//            if (Integer.parseInt(getQtyTRJ[i]) != 0) {
            if (Float.parseFloat(getQtyTRJ[i]) != 0.0) {    //ubah ke parseFloat untuk tipe pecahan
                Map<Object, Object> data = new HashMap<>();
                //#1
                Date DTglORJ = df.parse(getTglORJ[i]);
                data.put("mapTglTRJ", DTglORJ);
                //#2
                data.put("mapNomorBuktiTRJ", getNomorBuktiORJ[i]);
                //#3
                data.put("mapTokoTRJ", getTokoORJ[i]);
                //#4
                data.put("mapKodeStokTRJ", getKodeStokORJ[i]);
                //#5
                data.put("mapNamaStokTRJ", getNamaStokORJ[i]);
                //#6
                data.put("mapQtyTRJ", getQtyTRJ[i]);
                //#7
                data.put("mapQtyORJ", getQtyORJ[i]);
                //#8
                data.put("mapKetTRJ", getKetORJ[i]);
                //#9
                data.put("mapNomorTRMSTTRJ", getNomorTRMSTORJ[i]);
                //#10
                data.put("mapNomorTRDETTRJ", getNomorTRDETORJ[i]);
                //#11
                data.put("mapNamaTRMSTPHORJ", getNamaTRMSTPHORJ[i]);
                //#12
                data.put("mapAlamatTRMSTPHORJ", getAlamatTRMSTPHORJ[i]);
                //#13
                data.put("mapTelpTRMSTPHORJ", getTelpTRMSTPHORJ[i]);
                list_terimaRJ.add(data);
            }
        }
        model.addObject("daftarTerimaRJ", list_terimaRJ);
        model.setViewName("data-retur-jual");
        return model;
    }
    
    /**
     * Proses logic menampilkan serta pengecekan Daftar Order Retur Beli
     * @param model
     * @param ApproveKirim
     * @param tglAwal
     * @param tglAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listORB", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listORB(
            ModelAndView model,
            @RequestParam(value = "btnORB", required = false) String ApproveKirim,
            String tglAwal,
            String tglAkhir) throws ParseException {
        if (ApproveKirim != null) {
            String cekTgl1 = tglAwal;
            String cekTgl2 = tglAkhir;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTgl1) && !"".equals(cekTgl2)) {
                Date Dtglawal = df.parse(tglAwal);
                Date Dtglakhir = df.parse(tglAkhir);
                if (Dtglakhir.compareTo(Dtglawal) != 0) {
                    if (Dtglakhir.compareTo(Dtglawal) > 0) {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        List<String> listORB = vcekbayartrmstService.getTglORB(df2.format(Dtglawal), df2.format(Dtglakhir));
                        int jmlDataListORB = listORB.size();    //cek size/ukuran banyaknya data List<String> listORB
                        /*
                        //dapatkan harga beli utk di-set format Rp-nya (listORB)
                        Iterator iter = listORB.iterator();
                        while (iter.hasNext()) {
                            Map map = (Map) iter.next();
                            DecimalFormat dfhb = new DecimalFormat("#,###.00");
                            Object infoHargaBeli = dfhb.format(map.get("HARGABELI_ORB"));
                            map.put("HARGABELI_ORB", infoHargaBeli);
                        }
                        //SOUT LIST listORB
                        System.out.println("\n========================= MainController: List<String> listORB, dengan arg tgl awal & tgl akhir");
                        System.out.println("listORB = " + listORB);
                         */
                        model.addObject("dataORB", listORB);
                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListORB");
                        System.out.println("jmlDataListORB = " + jmlDataListORB);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarORB = vcekbayartrmstService.getCekORB();
            int jmlDataDaftarORB = daftarORB.size();    //cek size/ukuran banyaknya data List<String> daftarORB
/*
            //dapatkan harga beli utk di-set format Rp-nya (daftarORB)
            Iterator iter = daftarORB.iterator();
            while (iter.hasNext()) {
                Map map = (Map) iter.next();
                DecimalFormat dfhb = new DecimalFormat("#,###.00");
                Object infoHargaBeli = dfhb.format(map.get("HARGABELI_ORB"));
                map.put("HARGABELI_ORB", infoHargaBeli);
            }
            //SOUT LIST daftarORB
            System.out.println("\n========================= MainController: List<String> daftarORB");
            System.out.println("daftarORB = " + daftarORB);
*/
            model.addObject("dataORB", daftarORB);
            
            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarORB");
            System.out.println("jmlDataDaftarORB = " + jmlDataDaftarORB);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("list-retur-beli");
        return model;
    }
    
    /**
     * Proses Pengecekan dan Pemuatan Daftar ORB Terpilih untuk dikirim (menjadi TRB)
     * @param model
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/terimaRB", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView terimaRB(ModelAndView model) throws ParseException {
        List<Map<Object, Object>> list_terimaRB = new ArrayList<>();

        String[] getTglORB = request.getParameterValues("getTglORB");                   //#1 request
        String[] getNomorBuktiORB = request.getParameterValues("getNomorBuktiORB");     //#2 request
        String[] getTokoORB = request.getParameterValues("getTokoORB");                 //#3 request
        String[] getKodeStokORB = request.getParameterValues("getKodeStokORB");         //#4 request
        String[] getNamaStokORB = request.getParameterValues("getNamaStokORB");         //#5 request
        String[] getQtyTRB = request.getParameterValues("getQtyTRB");                   //#6 request
        String[] getQtyORB = request.getParameterValues("getQtyORB");                   //#7 request
//        String[] getHargaBeliORB = request.getParameterValues("getHargaBeliORB");       //#8 request
        String[] getSupplierORB = request.getParameterValues("getSupplierORB");         //#9 request
        String[] getKetORB = request.getParameterValues("getKetORB");                   //#10 request
        String[] getSubKetORB = request.getParameterValues("getSubKetORB");             //#11 request

        String[] getNomorTRMSTORB = request.getParameterValues("getNomorTRMSTORB");     //#12 request
        String[] getNomorTRDETORB = request.getParameterValues("getNomorTRDETORB");     //#13 request
        
        //UPDATE 2019-12-09: add data trmstph agar tdk hardcode lagi (#14 - #15)
        String[] getAlamatTRMSTPHORB = request.getParameterValues("getAlamatTRMSTPHORB");   //#14 request
        String[] getTelpTRMSTPHORB = request.getParameterValues("getTelpTRMSTPHORB");       //#15 request

        int jmlDataTRB = getNomorBuktiORB.length;  //jml length sama semua karena 1 String[] ekivalen 1 baris data list

        System.out.println("\n========================= MainController: jmlDataTRB (public ModelAndView terimaRB(ModelAndView model))");
        System.out.println("jmlData : " + jmlDataTRB);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        //for, di-looping setiap baris data list. jika difilter dulu dgn "cari data" maka jmlData akan menyesuaikan/iterasi lebih sedikit.
        for (int i = 0; i < jmlDataTRB; i++) {
            /* trik untuk index agar record data di view data-retur-beli.jsp jd urut asc per page (revert ordering) jika pake jquery append/prepend
            int iterJmlData = (jmlData - 1) - i;
             */
            //JIKA QTY TERIMA RETUR BELI TIDAK SAMA DGN 0 
//            if (Integer.parseInt(getQtyTRB[i]) != 0) {
            if (Float.parseFloat(getQtyTRB[i]) != 0) {  //ubah ke parseFloat untuk tipe pecahan
                Map<Object, Object> data = new HashMap<>();
                //#1
                Date DTglORB = df.parse(getTglORB[i]);
                data.put("mapTglTRB", DTglORB);
                //#2
                data.put("mapNomorBuktiTRB", getNomorBuktiORB[i]);
                //#3
                data.put("mapTokoTRB", getTokoORB[i]);
                //#4
                data.put("mapKodeStokTRB", getKodeStokORB[i]);
                //#5
                data.put("mapNamaStokTRB", getNamaStokORB[i]);
                //#6
                data.put("mapQtyTRB", getQtyTRB[i]);
                //#7
                data.put("mapQtyORB", getQtyORB[i]);
                //#8
//                data.put("mapHargaBeliTRB", getHargaBeliORB[i]);
                //#9
                data.put("mapSupplierTRB", getSupplierORB[i]);
                //#10
                data.put("mapKetTRB", getKetORB[i]);
                //#11
                data.put("mapSubKetTRB", getSubKetORB[i]);
                //#12
                data.put("mapNomorTRMSTTRB", getNomorTRMSTORB[i]);
                //#13
                data.put("mapNomorTRDETTRB", getNomorTRDETORB[i]);
                //#14
                data.put("mapAlamatTRMSTPHORB", getAlamatTRMSTPHORB[i]);
                //#15
                data.put("mapTelpTRMSTPHORB", getTelpTRMSTPHORB[i]);
                list_terimaRB.add(data);
            }
        }
        model.addObject("daftarTerimaRB", list_terimaRB);
        model.setViewName("data-retur-beli");
        return model;
    }
    
    /**
     * Proses logic menampilkan serta pengecekan Daftar PO
     * @param model
     * @param ApproveKirim
     * @param tglAwal
     * @param tglAkhir
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/listPO", method = {RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json")
    public ModelAndView listPO(
            ModelAndView model,
            @RequestParam(value = "btnPO", required = false) String ApproveKirim,
            String tglAwal,
            String tglAkhir) throws ParseException {
        if (ApproveKirim != null) {
            String cekTgl1 = tglAwal;
            String cekTgl2 = tglAkhir;
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (!"".equals(cekTgl1) && !"".equals(cekTgl2)) {
                Date Dtglawal = df.parse(tglAwal);
                Date Dtglakhir = df.parse(tglAkhir);
                
                //sonny, add logic utk dapatkan durasi dan selisih antara dua tanggal, lalu lempar sbg param utk limit query tarik list OM
                Duration durasi = Duration.between(Dtglawal.toInstant(), Dtglakhir.toInstant());
                short selisihTgl = (short) durasi.toDays();
                
                if (Dtglakhir.compareTo(Dtglawal) != 0) {
                    if (Dtglakhir.compareTo(Dtglawal) > 0) {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        
                        //sonny, add info: query tarik list PO utk LPB dengan input tanggal awal dan tanggal akhir
                        //sonny, add param ke-3 sbg flag untuk limit query
                        List<String> listPO = vcekbayartrmstService.getTglPO(df2.format(Dtglawal), df2.format(Dtglakhir), selisihTgl);
                        int jmlDataListPO = listPO.size();    //cek size/ukuran banyaknya data List<String> listPO
                        /*
                        //dapatkan harga beli utk di-set format Rp-nya (listPO)
                        Iterator iter = listPO.iterator();
                        while (iter.hasNext()) {
                            Map map = (Map) iter.next();
                            DecimalFormat dfhb = new DecimalFormat("#,###.00");
                            Object infoHargaBeli = dfhb.format(map.get("HARGABELI_PO"));
                            map.put("HARGABELI_PO", infoHargaBeli);
                        }
                        //SOUT LIST listPO
                        System.out.println("\n========================= MainController: List<String> listPO, dengan arg tgl awal & tgl akhir");
                        System.out.println("listPO = " + listPO);
                         */
                        model.addObject("dataPO", listPO);
                        //SOUT
                        System.out.println("\n========================= MainController: jmlDataListPO");
                        System.out.println("jmlDataListPO = " + jmlDataListPO);
                    } else {
                        model.addObject("errtgl", "Tanggal Akhir Harus Lebih Besar Dibandingkan Tanggal Awal");
                    }
                } else {
                    model.addObject("errtgl", "Tanggal Awal Harus Beda Dengan Tanggal Akhir Atau Tanggal Awal");
                }
            } else {
                model.addObject("errtgl", "Tanggal Awal dan Tanggal Akhir Harus Terisi");
            }
        }
        else {
            List<String> daftarPO = vcekbayartrmstService.getCekPO();
            int jmlDataDaftarPO = daftarPO.size();    //cek size/ukuran banyaknya data List<String> daftarPO
/*
            //dapatkan harga beli utk di-set format Rp-nya (daftarPO)
            Iterator iter = daftarPO.iterator();
            while (iter.hasNext()) {
                Map map = (Map) iter.next();
                DecimalFormat dfhb = new DecimalFormat("#,###.00");
                Object infoHargaBeli = dfhb.format(map.get("HARGABELI_PO"));
                map.put("HARGABELI_PO", infoHargaBeli);
            }
            //SOUT LIST daftarPO
            System.out.println("\n========================= MainController: List<String> daftarPO");
            System.out.println("daftarPO = " + daftarPO);
*/
            model.addObject("dataPO", daftarPO);
            
            //SOUT
            System.out.println("\n========================= MainController: jmlDataDaftarPO");
            System.out.println("jmlDataDaftarPO = " + jmlDataDaftarPO);
        }

        model.addObject("cekUser", session.getAttribute("tempUser"));
        
//sonny, update 16/04/19 set off
//        model.addObject("totalBatasKirim", session.getAttribute("totalBatasKirim"));    //utk status di navbar.jsp

        model.setViewName("list-pembelian");
        return model;
    }
    
    /**
     * Proses Pengecekan dan Pemuatan Daftar PO Terpilih untuk dikirim (menjadi LPB)
     * @param model
     * @return
     * @throws ParseException 
     */
    @RequestMapping(value = "/terimaLPB", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json")
    public ModelAndView terimaLPB(ModelAndView model) throws ParseException {
        List<Map<Object, Object>> list_terimaLPB = new ArrayList<>();

        String[] getTglPO = request.getParameterValues("getTglPO");                 //#1 request
        String[] getNomorBuktiPO = request.getParameterValues("getNomorBuktiPO");   //#2 request
        String[] getTokoPO = request.getParameterValues("getTokoPO");               //#3 request
        String[] getKodeStokPO = request.getParameterValues("getKodeStokPO");       //#4 request
        String[] getNamaStokPO = request.getParameterValues("getNamaStokPO");       //#5 request
        String[] getQtyLPB = request.getParameterValues("getQtyLPB");               //#6 request
        String[] getQtyPO = request.getParameterValues("getQtyPO");                 //#7 request
//        String[] getHargaBeliPO = request.getParameterValues("getHargaBeliPO");   //#8 request
        String[] getNamaSupplierPO = request.getParameterValues("getNamaSupplierPO");       //#9 request
        String[] getAlamatSupplierPO = request.getParameterValues("getAlamatSupplierPO");   //#10 request
        String[] getTelpSupplierPO = request.getParameterValues("getTelpSupplierPO");       //#11 request
        String[] getKetPO = request.getParameterValues("getKetPO");                   //#12 request
        String[] getSubKetPO = request.getParameterValues("getSubKetPO");             //#13 request
        String[] getNomorTRMSTPO = request.getParameterValues("getNomorTRMSTPO");     //#14 request
        String[] getNomorTRDETPO = request.getParameterValues("getNomorTRDETPO");     //#15 request

        int jmlDataLPB = getNomorBuktiPO.length;  //jml length sama semua karena 1 String[] ekivalen 1 baris data list

        System.out.println("\n========================= MainController: jmlDataLPB (public ModelAndView terimaLPB(ModelAndView model))");
        System.out.println("jmlData : " + jmlDataLPB);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        //for, di-looping setiap baris data list. jika difilter dulu dgn "cari data" maka jmlData akan menyesuaikan/iterasi lebih sedikit.
        for (int i = 0; i < jmlDataLPB; i++) {
            /* trik untuk index agar record data di view data-pembelian.jsp jd urut asc per page (revert ordering) jika pake jquery append/prepend
            int iterJmlData = (jmlData - 1) - i;
             */
            //JIKA QTY TERIMA LPB TIDAK SAMA DGN 0 
            if (Float.parseFloat(getQtyLPB[i]) != 0) {
                Map<Object, Object> data = new HashMap<>();
                //#1
                Date DTglLPB = df.parse(getTglPO[i]);
                data.put("mapTglLPB", DTglLPB);
                //#2
                data.put("mapNomorBuktiLPB", getNomorBuktiPO[i]);
                //#3
                data.put("mapTokoLPB", getTokoPO[i]);
                //#4
                data.put("mapKodeStokLPB", getKodeStokPO[i]);
                //#5
                data.put("mapNamaStokLPB", getNamaStokPO[i]);
                //#6
                data.put("mapQtyLPB", getQtyLPB[i]);
                //#7
                data.put("mapQtyPO", getQtyPO[i]);
                //#8
//                data.put("mapHargaBeliLPB", getHargaBeliPO[i]);
                //#9
                data.put("mapNamaSupplierLPB", getNamaSupplierPO[i]);
                //#10
                data.put("mapAlamatSupplierLPB", getAlamatSupplierPO[i]);
                //#9
                data.put("mapTelpSupplierLPB", getTelpSupplierPO[i]);
                //#10
                data.put("mapKetLPB", getKetPO[i]);
                //#11
                data.put("mapSubKetLPB", getSubKetPO[i]);
                //#12
                data.put("mapNomorTRMSTLPB", getNomorTRMSTPO[i]);
                //#13
                data.put("mapNomorTRDETLPB", getNomorTRDETPO[i]);
                list_terimaLPB.add(data);
            }
        }
        model.addObject("daftarTerimaLPB", list_terimaLPB);
        model.setViewName("data-pembelian");
        return model;
    }
}
