package com.ufo.pengiriman.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractDao<PK extends Serializable, T> {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private HttpServletRequest request;

    
    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public T getByKey(PK key) {
        return (T) getSession().get(persistentClass, key);
    }
    
    public void persist(T entity) {
        getSession().persist(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    protected Criteria createEntityCriteria() {
        return getSession().createCriteria(persistentClass);
    }

    public final String getLokasi() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();
        Query idLokasi = getSession().createSQLQuery("SELECT a.LOKASI FROM USERID a where a.KODE = :cekUser").setParameter("cekUser", username);
        List<Integer> result = idLokasi.list();
        return String.valueOf(result.get(0));
    }
    
    //agar tidak memanggil getLokasi() method diatas, panggil session infoLokasi yang dibuat di ModelAndView beranda (MainController)
    public final String getInfoLokasi() {
        HttpSession session = request.getSession(true);
        return (String) session.getAttribute("infoLokasi").toString();
    }
    
    //agar tidak memanggil hibernateUtil.getIdUser(username), panggil session infoKodeUser yang dibuat di ModelAndView beranda (MainController)
    public final String getInfoKodeUser() {
        HttpSession session = request.getSession(true);
        return (String) session.getAttribute("infoKodeUser").toString();
    }
    
    //agar tidak memanggil hibernateUtil.getIdUser(username), panggil session infoNomorDivisi yang dibuat di ModelAndView beranda (MainController)
    public final String getInfoNomorDivisi() {
        HttpSession session = request.getSession(true);
        return (String) session.getAttribute("infoNomorDivisi").toString();
    }
}
