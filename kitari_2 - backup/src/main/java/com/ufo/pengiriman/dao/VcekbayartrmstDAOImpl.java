package com.ufo.pengiriman.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
/* import class2 ufo pengiriman */
import com.ufo.pengiriman.hibernate.HibernateUtil;
import com.ufo.pengiriman.model.Trmst;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author PROGRAMER
 */
@Repository
public class VcekbayartrmstDAOImpl extends AbstractDao<Integer, Trmst> implements VcekbayartrmstDAO {

    @Autowired
    HibernateUtil hibernateUtil;
    
    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    @Override
    public String getNoBukti(Integer noTRMST) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: method String getNoBukti(Integer noTRMST)");
        System.out.println("\n parameter noTRMST: " + noTRMST);
        Query cekNo = getSession().getNamedQuery("Trmst.findByNomor").setParameter("nomor", noTRMST);
        List kode = cekNo.list();
        String noBukti = String.valueOf(kode.get(0));
        System.out.println("\n return noBukti: " + noBukti);
        return noBukti;
    }

    @Override
    public List<String> getBarang(String nomor) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getBarang(), dapatkan informasi detail transaksi SO/OM dari 1 SJ/MT");
        return getSession().createSQLQuery("SELECT f.NOBUKTI as NOBUKTI_SO_OM, g.KODE as KODESTOK, g.NAMA as NAMASTOK,"
                + " d.BANYAKNYA as JMLORDER," //eric: d.BANYAKNYA as banyak
                + " b.BANYAKNYA as JMLKIRIM,"  //eric: b.BANYAKNYA as jumlah
                + " a.TRANSAKSI as KODETRANSAKSI, l.NAMA as TOKO, a.NOBUKTI as NOBUKTI_SJ_MT, tph.NAMA as NAMACUSTOMER,"
                + " tph.ALAMAT as ALAMATCUSTOMER, tph.TELP as TELPCUSTOMER, trdk.KET0 as KETERANGAN"    
                + " FROM TRMST a"
                + " INNER JOIN TRDET b ON a.NOMOR = b.NOMORTRMST"               //#1, TRDET SJ/MT
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"               //#2
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"                 //#3, TRDET SO/OM
                + " INNER JOIN TRMSTPH tph ON a.NOMOR = tph.NOMOR"              //#4, tambah utk nama, alamat, telp
                + " INNER JOIN TRMST f ON f.NOMOR = d.NOMORTRMST"               //#5
                + " INNER JOIN TRANSAKSILOKASI tl ON f.TRANSAKSI = tl.TRANSAKSI"//#6
                + " INNER JOIN LOKASI l ON tl.LOKASI = l.LOKASI"                //#7, tambah utk nama LOKASI toko
                + " INNER JOIN STOK g ON d.NOMORSTOK = g.NOMOR"                 //#8
                + " LEFT JOIN TRDETKET trdk ON d.NOMOR = trdk.NOMOR"            //#9, tambah utk keterangan SO/OM yg detail
                + " WHERE a.NOBUKTI= :ceknomor").setParameter("ceknomor", nomor)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    //untuk mendapatkan info user yg posting pengiriman terakhir, dipanggil di public ModelAndView tambahBarang (ManageController.java) 
    @Override
    public String getLogin(String noBukti) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getLogin(String noBukti), dapatkan info user yg posting pengiriman terakhir");
        Query cekLogin = getSession().createSQLQuery("SELECT f.LOGIN"
                + " FROM TRMST c INNER JOIN VPUBTRDETSISA b ON c.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF a ON a.NOMORREF = b.NOMOR"
                + " INNER JOIN TRDET e ON e.NOMOR = a.NOMOR"
                + " INNER JOIN TRMST f ON e.NOMORTRMST = f.NOMOR"
                + " WHERE C.NOBUKTI = :nomor").setParameter("nomor", noBukti);
        List login = cekLogin.list();
        if ("[]".equals(login.toString())) {
            login.add(null);
        }
        return String.valueOf(login.get(0));
    }

//  method getSO di-off karena tidak dipanggil/tidak digunakan
//    @Override
//    public List<String> getSO(List<String> NoBukti) {
//        List<String> listSO = new ArrayList<String>();
//        List<String> tambahSO = new ArrayList<String>();
//        for (int i = 0; i < NoBukti.size(); i++) {
//            listSO = getSession().createSQLQuery("SELECT a.TGL AS Tanggal, a.NOBUKTI AS NomorBukti, "
//                    + "v.KODESTOK AS KODESTOK_SO,a.NAMA AS Nama,v.BANYAKNYA AS Jumlah "
//                    + "FROM VCEKBAYARTRMST a "
//                    + "inner join VPUBTRDET08 v on v.NOMORTRMST = a.NOMOR "
//                    + "left join VPUBTRDETKETTGL d on v.NOMOR = d.NOMOR "
//                    + "inner join VPUBTRDETSISA c on v.NOMORTRMST = c.NOMORTRMST "
//                    + "where a.TOTALNETTO = a.TOTALBAYAR and a.NOBUKTI = '" + NoBukti.get(i) + "'").setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
//                    .list();
//            tambahSO.addAll(listSO);
//        }
//        return tambahSO;
//    }

    @Override
    public void deleteData(Integer noTRMST) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: method deleteData(Integer noTRMST), HIBERNATE WAYS...");
        Trmst trmst = getByKey(noTRMST);
        delete(trmst);
    }

    @Override
    public List<String> getCekUser() {
        //skrip auth dibawah ini jg ada di method addSOkirim (Vpubtrdet08DAOImpl) dan di method getLokasi (AbstractDAO)
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();
        
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekUser(), cek informasi user yg sukses login");
        return getSession().createSQLQuery("SELECT a.LOKASI as Lokasi, a.NAMA as NamaLokasi, T.NAMA as NamaToko, u.KODE AS KodeUser,"
                + " u.NAMA AS NamaUser, S.DIVISINOMOR as NomorDivisi"   //pake nomor divisi krn untuk insert ke trmst dgn session infoNomorDivisi
                //+ " S.DIVISIKODE as divisi"   //eric pake ini. oki, di navbar var="Divisi", di listsuratjalan.jsp test="${Divisi=='MANAGER'}"
                + " FROM LOKASI a INNER JOIN TOKO T ON a.TOKO = T.TOKO"
                + " INNER JOIN USERID u on u.LOKASI = a.LOKASI"
                + " INNER JOIN VDIVISIUSERID01 s on s.USERIDNOMOR = u.NOMOR"
                + " where u.KODE = :cekuser")
                .setParameter("cekuser", username)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    /**
     * logic dashboard jumlah pengiriman dan mutasi jadi satu
     * @return 
     */
    @Override
    public List<Object> getCountDashboard() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCountDashboard(), cek jumlah kirim SO dan OM dari 1 hari terakhir");
        Query query = getSession().createSQLQuery("SELECT a.TRANSAKSI, a.SISABAYAR"
                + " FROM VCEKBAYARTRMST a"
                + " INNER JOIN VPUBTRDETSISA c on a.NOMOR = c.NOMORTRMST"
                + " INNER JOIN TRDETLOKASI2 t ON c.NOMOR = t.NOMOR"
                + " WHERE t.LOKASI2 = " + getInfoLokasi()
                + " and c.QTYSISA > 0 and a.SELISIHTGL BETWEEN 0 and 1")   //cek list order SO & MT mulai kemarin hingga hari ini
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<Object> result = query.list();
        return result;
    }
    
    /**
     * query total kirim order penjualan untuk batasan qty kirim gudang per harinya
     * @return 
     */
    @Override
    public Double getTotalSJPerHari() {
        Date Tdate = new Date();
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTransaksiSO = (flagInfoLokasi > 10) ? ("61" + getInfoLokasi()) : ("610" + getInfoLokasi());
        
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTotalSJPerHari(), get total kirim order penjualan untuk batasan qty kirim gudang per harinya");
        Query query = getSession().createSQLQuery("SELECT sum(a.BANYAKNYA) AS JUMLAHKIRIMHARIINI"
                + " FROM VPRVTRDET01 a"
                + " INNER JOIN VPRVTRDETKETTGL b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRMST c ON a.NOMORTRMST = c.NOMOR"
                + " WHERE CAST(b.TGL2 As Date) = '" + sdf.format(Tdate) + "'"
                + " and c.TRANSAKSI = " + nomorTransaksiSO
                + " GROUP BY c.TRANSAKSI, CAST(b.TGL2 As Date)");
                //.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);  //tdk perlu krn uniqueResult/single fetch result
        Object result = query.uniqueResult();
        if (result != null) {
            return ((Double) result);
        }
        return 0.0;
    }
    
    /**
     * query total kirim order mutasi untuk batasan qty kirim gudang per harinya
     * @return 
     */
    @Override
    public Double getTotalMTPerHari() {
        Date Tdate = new Date();
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTransaksiMT = (flagInfoLokasi > 10) ? ("51" + getInfoLokasi()) : ("510" + getInfoLokasi());
        
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTotalMTPerHari(), get total kirim order mutasi untuk batasan qty kirim gudang per harinya");
        Query query = getSession().createSQLQuery("SELECT sum(a.BANYAKNYA) AS JUMLAHKIRIMHARIINI"
                + " FROM VPRVTRDET01 a"
                + " INNER JOIN VPRVTRDETKETTGL b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRMST c ON a.NOMORTRMST = c.NOMOR"
                + " WHERE CAST(b.TGL2 As Date) = '" + sdf.format(Tdate) + "'"
                + " and c.TRANSAKSI = " + nomorTransaksiMT
                + " GROUP BY c.TRANSAKSI, CAST(b.TGL2 As Date)");
        Object result = query.uniqueResult();
        if (result != null) {
            return ((Double) result);
        }
        return 0.0;
    }
    
    /**
     * query total kirim order retur jual hari ini
     * @return 
     */
    @Override
    public Double getTotalRJPerHari() {
        Date Tdate = new Date();
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTransaksiRJ = (flagInfoLokasi > 10) ? ("31" + getInfoLokasi()) : ("310" + getInfoLokasi());
        
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTotalRJPerHari(), get total kirim order retur jual");
        Query query = getSession().createSQLQuery("SELECT sum(a.BANYAKNYA) AS JUMLAHKIRIMHARIINI"
                + " FROM VPRVTRDET01 a"
                + " INNER JOIN VPRVTRDETKETTGL b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRMST c ON a.NOMORTRMST = c.NOMOR"
                + " WHERE CAST(b.TGL2 As Date) = '" + sdf.format(Tdate) + "'"
                + " and c.TRANSAKSI = " + nomorTransaksiRJ
                + " GROUP BY c.TRANSAKSI, CAST(b.TGL2 As Date)");
        Object result = query.uniqueResult();
        if (result != null) {
            return ((Double) result);
        }
        return 0.0;
    }
    
    /**
     * query total kirim order retur beli hari ini
     * @return 
     */
    @Override
    public Double getTotalRBPerHari() {
        Date Tdate = new Date();
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTransaksiRB = (flagInfoLokasi > 10) ? ("71" + getInfoLokasi()) : ("710" + getInfoLokasi());
        
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTotalRBPerHari(), get total kirim order retur beli");
        Query query = getSession().createSQLQuery("SELECT sum(a.BANYAKNYA) AS JUMLAHKIRIMHARIINI"
                + " FROM VPRVTRDET01 a"
                + " INNER JOIN VPRVTRDETKETTGL b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRMST c ON a.NOMORTRMST = c.NOMOR"
                + " WHERE CAST(b.TGL2 As Date) = '" + sdf.format(Tdate) + "'"
                + " and c.TRANSAKSI = " + nomorTransaksiRB
                + " GROUP BY c.TRANSAKSI, CAST(b.TGL2 As Date)");
        Object result = query.uniqueResult();
        if (result != null) {
            return ((Double) result);
        }
        return 0.0;
    }
    
    /**
     * query tarik list SO utk pengiriman dari 1 hari terakhir
     * @return 
     */
    @Override
    public List<String> getCekBayarTrmst() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekBayarTrmst(), query tarik list SO utk pengiriman dari 1 hari terakhir");
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_SO, r.TGL as TGL_SO, r.TRDETKETTGL1 as TGLKIRIM_SO" +
                ", coalesce(r.NOBUKTIBAYAR, '-') as NOMORBUKTI_IR" +
                ", r.BAYARMSTTGL as TGL_IR" +   //utk date jangan di-coalesce nanti error di jsp
                ", r.KODESTOK as KODESTOK_SO, r.NAMASTOK as NAMASTOK_SO, r.QTYSISAORDER as QTY_SO" +
                ", r.NOMOR_LOKASI2, r.LOKASI2 as GUDANG_SJ" +
                ", r.PHKODE, r.PHNAMA as NAMACUSTOMER_SO, r.ALAMAT as ALAMATCUSTOMER_SO, r.TELP as TELPCUSTOMER_SO" +
                ", r.TRMSTNOMOR as NOMORTRMST_SO, r.TRDETNOMOR as NOMORTRDET_SO" +
                ", coalesce(r.TRMSTKETNOBUKTI, '-') AS NOMORPROGLAMA" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_SO" +
                ", r.TRMSTPHKET as KET_SO, r.TRMSTKETKET1 as SUBKET_SO" +
                ", r.LOGIN as SPM" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8100 and r.TRANSAKSI < 8200)" +
                " and r.SISABAYAR = 0" +
                //sonny, off where cek tanggal bayar
//                " and (" +
//                " (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
//                " or" +
//                " (current_date - cast(r.BAYARMSTTGL as date) >= 0 and current_date - cast(r.BAYARMSTTGL as date) < 2)" +
//                " )" +
                " and (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " order by r.TRDETNOMOR asc")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list SO utk pengiriman dengan input tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @return 
     */
    @Override
    public List<String> getTglSO(String tgl1, String tgl2, short selisihTgl) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTglSO(), query tarik list SO utk pengiriman dengan input tanggal awal dan tanggal akhir");
        System.out.println("tgl1(pengiriman) =" + tgl1);
        System.out.println("tgl2(pengiriman) =" + tgl2);
        System.out.println("\n*** CEK selisihTgl SO -> SJ = " + selisihTgl + " hari");
        
        //sonny, add limit query, use first amount sql
        String limit = (selisihTgl >= 30) ? "first 15" : "";
        
        return getSession().createSQLQuery("SELECT " + limit + " r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_SO, r.TGL as TGL_SO, r.TRDETKETTGL1 as TGLKIRIM_SO" +
                ", coalesce(r.NOBUKTIBAYAR, '-') as NOMORBUKTI_IR" +
                ", r.BAYARMSTTGL as TGL_IR" +   //utk date jangan di-coalesce nanti error di jsp
                ", r.KODESTOK as KODESTOK_SO, r.NAMASTOK as NAMASTOK_SO, r.QTYSISAORDER as QTY_SO" +
                ", r.NOMOR_LOKASI2, r.LOKASI2 as GUDANG_SJ" +
                ", r.PHKODE, r.PHNAMA as NAMACUSTOMER_SO, r.ALAMAT as ALAMATCUSTOMER_SO, r.TELP as TELPCUSTOMER_SO" +
                ", r.TRMSTNOMOR as NOMORTRMST_SO, r.TRDETNOMOR as NOMORTRDET_SO" +
                ", coalesce(r.TRMSTKETNOBUKTI, '-') AS NOMORPROGLAMA" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_SO" +
                ", r.TRMSTPHKET as KET_SO, r.TRMSTKETKET1 as SUBKET_SO" +
                ", r.LOGIN as SPM" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8100 and r.TRANSAKSI < 8200)" +
                " and r.SISABAYAR = 0" +
                //sonny, off where cek tanggal bayar
//                " and (" +
//                " (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
//                " or" +
//                " (r.BAYARMSTTGL >= cast(:tglawal as timestamp) and r.BAYARMSTTGL < cast(:tglakhir as timestamp)+1)" +
//                " )" +
                " and (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
                " and r.QTYSISAORDER > 0" +
//                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " and r.NOMOR_LOKASI2 = 5" + //hardcode untuk CEK JSON
                " order by r.TRDETNOMOR asc")
                .setParameter("tglawal", tgl1)
                .setParameter("tglakhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list OM utk pengiriman mutasi dari 1 hari terakhir
     * @return 
     */
    @Override
    public List<String> getCekMutasi() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekMutasi(), query tarik list OM utk pengiriman mutasi dari 1 hari terakhir");
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_OM, r.TGL as TGL_OM" +
                ", r.KODESTOK as KODESTOK_OM, r.NAMASTOK as NAMASTOK_OM, r.QTYSISAORDER as QTY_OM" +
                ", r.TRMSTNOMOR as NOMORTRMST_OM, r.TRDETNOMOR as NOMORTRDET_OM" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_OM" +
                ", r.TRMSTKETKET1 as KET_OM, r.TRDETKETKET0 as SUBKET_OM" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8500 and r.TRANSAKSI < 8600)" +
                " and (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() + 
                " order by r.TRDETNOMOR asc")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list OM utk pengiriman mutasi dengan input tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @return 
     */
    @Override
    public List<String> getTglMutasi(String tgl1, String tgl2, short selisihTgl) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTglMutasi(), cek list mutasi dengan input tgl1 dan tgl2");
        System.out.println("tgl1(mutasi) =" + tgl1);
        System.out.println("tgl2(mutasi) =" + tgl2);
        System.out.println("\n*** CEK selisihTgl OM -> MT = " + selisihTgl + " hari");
        
        //sonny, add limit query, use first amount sql
        String limit = (selisihTgl >= 30) ? "first 15" : "";
        
        return getSession().createSQLQuery("SELECT " + limit + " r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_OM, r.TGL as TGL_OM" +
                ", r.KODESTOK as KODESTOK_OM, r.NAMASTOK as NAMASTOK_OM, r.QTYSISAORDER as QTY_OM" +
                ", r.TRMSTNOMOR as NOMORTRMST_OM, r.TRDETNOMOR as NOMORTRDET_OM" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_OM" +
                ", r.TRMSTKETKET1 as KET_OM, r.TRDETKETKET0 as SUBKET_OM" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8500 and r.TRANSAKSI < 8600)" +
                " and (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() + 
                " order by r.TRDETNOMOR asc")
                .setParameter("tglawal", tgl1)
                .setParameter("tglakhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list ORJ utk pengiriman TRJ dari 1 hari terakhir
     * @return 
     */
    @Override
    public List<String> getCekORJ() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekORJ(), query tarik list ORJ utk pengiriman TRJ dari 1 hari terakhir");
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_ORJ, r.TGL as TGL_ORJ" +
                ", r.KODESTOK as KODESTOK_ORJ, r.NAMASTOK as NAMASTOK_ORJ, r.QTYSISAORDER as QTY_ORJ" +
                ", r.TRMSTNOMOR as NOMORTRMST_ORJ, r.TRDETNOMOR as NOMORTRDET_ORJ" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_ORJ" +
                ", r.TRMSTKETKET1 as KET_ORJ" +
                ", r.PHNAMA as NAMA_TRMSTPH_ORJ, r.ALAMAT as ALAMAT_TRMSTPH_ORJ, r.TELP as TELP_TRMSTPH_ORJ" + //update 2019-11-29: add data trmstph agar tdk hardcode lagi
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8600 and r.TRANSAKSI < 8700)" +
                " and (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() + 
                " order by r.TRDETNOMOR asc")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list ORJ utk pengiriman TRJ dengan input tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @return 
     */
    @Override
    public List<String> getTglORJ(String tgl1, String tgl2) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTglORJ(), query tarik list ORJ utk pengiriman TRJ dengan input tanggal awal dan tanggal akhir");
        System.out.println("tgl1(ORJ) =" + tgl1);
        System.out.println("tgl2(ORJ) =" + tgl2);
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_ORJ, r.TGL as TGL_ORJ" +
                ", r.KODESTOK as KODESTOK_ORJ, r.NAMASTOK as NAMASTOK_ORJ, r.QTYSISAORDER as QTY_ORJ" +
                ", r.TRMSTNOMOR as NOMORTRMST_ORJ, r.TRDETNOMOR as NOMORTRDET_ORJ" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_ORJ" +
                ", r.TRMSTKETKET1 as KET_ORJ" +
                ", r.PHNAMA as NAMA_TRMSTPH_ORJ, r.ALAMAT as ALAMAT_TRMSTPH_ORJ, r.TELP as TELP_TRMSTPH_ORJ" + //update 2019-11-29: add data trmstph agar tdk hardcode lagi
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8600 and r.TRANSAKSI < 8700)" +
                " and (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() + 
                " order by r.TRDETNOMOR asc")
                .setParameter("tglawal", tgl1)
                .setParameter("tglakhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list ORB utk pengiriman TRB dari 1 hari terakhir
     * @return 
     */
    @Override
    public List<String> getCekORB() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekORB(), query tarik list ORB utk pengiriman TRB dari 1 hari terakhir");
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_ORB, r.TGL as TGL_ORB" +
                ", r.KODESTOK as KODESTOK_ORB, r.NAMASTOK as NAMASTOK_ORB, r.QTYSISAORDER as QTY_ORB" +
                ", r.TRMSTNOMOR as NOMORTRMST_ORB, r.TRDETNOMOR as NOMORTRDET_ORB" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_ORB" +
                ", r.TRMSTKETKET1 as KET_ORB" +
                ", r.TRDETKETKET0 as SUBKET_ORB" +
                ", r.PHNAMA as SUPPLIER_ORB, r.ALAMAT as ALAMAT_TRMSTPH_ORB, r.TELP as TELP_TRMSTPH_ORB" +  //update 2019-12-09: add data trmstph agar tdk hardcode lagi
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8700 and r.TRANSAKSI < 8800)" +
                " and (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " order by r.TRDETNOMOR asc")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list ORB utk pengiriman TRB dengan input tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @return 
     */
    @Override
    public List<String> getTglORB(String tgl1, String tgl2) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTglORB(), cek list tgl order retur beli dengan input tgl1 dan tgl2");
        System.out.println("tgl1(ORB) =" + tgl1);
        System.out.println("tgl2(ORB) =" + tgl2);
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_ORB, r.TGL as TGL_ORB" +
                ", r.KODESTOK as KODESTOK_ORB, r.NAMASTOK as NAMASTOK_ORB, r.QTYSISAORDER as QTY_ORB" +
                ", r.TRMSTNOMOR as NOMORTRMST_ORB, r.TRDETNOMOR as NOMORTRDET_ORB" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_ORB" +
                ", r.TRMSTKETKET1 as KET_ORB" +
                ", r.TRDETKETKET0 as SUBKET_ORB" +
                ", r.PHNAMA as SUPPLIER_ORB, r.ALAMAT as ALAMAT_TRMSTPH_ORB, r.TELP as TELP_TRMSTPH_ORB" +  //update 2019-12-09: add data trmstph agar tdk hardcode lagi
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8700 and r.TRANSAKSI < 8800)" +
                " and (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " order by r.TRDETNOMOR asc")
                .setParameter("tglawal", tgl1)
                .setParameter("tglakhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list PO utk LPB dari 1 hari terakhir
     * @return 
     */
    @Override
    public List<String> getCekPO() {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getCekPO(), query tarik list PO utk LPB dari 1 hari terakhir");
        return getSession().createSQLQuery("SELECT r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_PO, r.TGL as TGL_PO" +
                ", r.KODESTOK as KODESTOK_PO, r.NAMASTOK as NAMASTOK_PO, r.QTYSISAORDER as QTY_PO" +
                ", r.TRMSTNOMOR as NOMORTRMST_PO, r.TRDETNOMOR as NOMORTRDET_PO" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_PO" +
                ", r.TRMSTPHKET as KET_PO" +
                ", r.TRDETKETKET0 as SUBKET_PO" +
                ", r.PHNAMA as NAMA_SUPPLIER_PO, r.ALAMAT as ALAMAT_SUPPLIER_PO, r.TELP as TELP_SUPPLIER_PO" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8200 and r.TRANSAKSI < 8300)" +
                " and (current_date - cast(r.TGL as date) >= 0 and current_date - cast(r.TGL as date) < 2)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " order by r.TRDETNOMOR asc")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    /**
     * query tarik list PO utk LPB dengan input tanggal awal dan tanggal akhir
     * @param tgl1
     * @param tgl2
     * @return 
     */
    @Override
    public List<String> getTglPO(String tgl1, String tgl2, short selisihTgl) {
        System.out.println("\n======================== VcekbayartrmstDAOImpl: QUERY getTglPO(), cek list tgl PO dengan input tgl1 dan tgl2");
        System.out.println("tgl1(PO) =" + tgl1);
        System.out.println("tgl2(PO) =" + tgl2);
        System.out.println("\n*** CEK selisihTgl PO -> LPB = " + selisihTgl + " hari");
        
        //sonny, add limit query, use first amount sql
        String limit = (selisihTgl >= 30) ? "first 15" : "";
        
        return getSession().createSQLQuery("SELECT " + limit + " r.TRANSAKSI" +
                ", r.NOBUKTI as NOMORBUKTI_PO, r.TGL as TGL_PO" +
                ", r.KODESTOK as KODESTOK_PO, r.NAMASTOK as NAMASTOK_PO, r.QTYSISAORDER as QTY_PO" +
                ", r.TRMSTNOMOR as NOMORTRMST_PO, r.TRDETNOMOR as NOMORTRDET_PO" +
                ", r.NOMOR_LOKASI1, r.LOKASI1 as TOKO_PO" +
                ", r.TRMSTPHKET as KET_PO" +
                ", r.TRDETKETKET0 as SUBKET_PO" +
                ", r.PHNAMA as NAMA_SUPPLIER_PO, r.ALAMAT as ALAMAT_SUPPLIER_PO, r.TELP as TELP_SUPPLIER_PO" +
                " FROM VSETELAHORDER01 r" +
                " WHERE (r.TRANSAKSI >= 8200 and r.TRANSAKSI < 8300)" +
                " and (r.TGL >= cast(:tglawal as timestamp) and r.TGL < cast(:tglakhir as timestamp)+1)" +
                " and r.QTYSISAORDER > 0" +
                " and r.NOMOR_LOKASI2 = " + getInfoLokasi() +
                " order by r.TRDETNOMOR asc")
                .setParameter("tglawal", tgl1)
                .setParameter("tglakhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
}
