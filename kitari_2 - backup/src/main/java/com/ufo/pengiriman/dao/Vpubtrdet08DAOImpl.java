package com.ufo.pengiriman.dao;

import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.dao.DataAccessResourceFailureException;
/*  import class2 ufo pengiriman */
import com.ufo.pengiriman.hibernate.HibernateUtil;
import com.ufo.pengiriman.model.Vpubtrdet08;
import com.ufo.pengiriman.service.HSoftTerbilang;
import com.ufo.pengiriman.service.VcekbayartrmstService;
import java.util.Enumeration;


/**
 *
 * @author PROGRAMER
 */
@Repository
public class Vpubtrdet08DAOImpl extends AbstractDao<Integer, Vpubtrdet08> implements Vpubtrdet08DAO {

    @Autowired
    HibernateUtil hibernateUtil;

    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private HttpSession session;
    
    @Autowired
    VcekbayartrmstService vcekbayartrmstService;

    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");   //untuk daftar surat jalan hari ini, laporan pengiriman hari ini
    
    //SUBRUTIN TRANSAKSI INSERT KIRIM PENJUALAN (addSOkirim)
    @Override
    public int addSOkirim(int jmlDataTambahBarangSO, String[] dataQtyKirimSO, String[] dataTglKirimSO, String[] dataNomorBuktiSO,
            String[] dataNomorTRMSTSO, String[] dataNomorTRDETSO, String dataNamaCustomerSO, String dataAlamatCustomerSO, String dataTelpCustomerSO, String dataNAMA_DRIVER, String dataNO_PLAT)
    { //FAF, Tambahan
        
        System.out.println("\n\n ========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================\n"
                + " MEMASUKI SUBRUTIN TRANSAKSI INSERT PENGIRIMAN (addSOkirim)\n"
                + "========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================");

        //Double HPP = null;    //sejak awal sm eric tidak digunakan
        Session session = null;
        Transaction tx = null;
        Object[] objHPP = null;
        
        //SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE artinya "BACA NOMOR TRMST+1 DARI GENERATOR"
        String idTRMST = hibernateUtil.getNomorTRMST();

        /* mulai rutin untuk mendapatkan info login. mungkin bisa disederhanakan. */
        System.out.println("\n ======================== MULAI loggedInUser = SecurityContextHolder.getContext().getAuthentication()");
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("\n loggedInUser = " + loggedInUser);

        System.out.println("\n ======================== MULAI username = loggedInUser.getName()");
        String username = loggedInUser.getName();   //ambil username
        System.out.println("\n username = " + username);

        System.out.println("\n ======================== MULAI cekRowUser = (Object[]) hibernateUtil.getIdUser(username)");
        Object[] cekRowUser = (Object[]) hibernateUtil.getIdUser(username);     //SELECT v.DIVISINOMOR, u.LOKASI
        System.out.println("\n cekRowUser = " + Arrays.toString(cekRowUser));

        System.out.println("\n ======================== MULAI String noTR = cekRowUser[2].toString() dan int flag = Integer.parseInt(noTR)");
        String noTR = cekRowUser[1].toString();     //untuk nomor transaksi yang melihat nomor lokasi
        int flag = Integer.parseInt(noTR);          //buat flag untuk trik lokasi 1 digit atau 2 digit
        System.out.println("noTR = " + noTR);
        System.out.println("flag = " + flag);

        System.out.println("\n ======================== MULAI getKeteranganSO = hibernateUtil.getKetSO(kode[0])");
        String getKeteranganSO = hibernateUtil.getKetSO(dataNomorBuktiSO[0]);  //ambil data field keterangan di tabel TRMSTPH
        System.out.println("\n getKeteranganSO = " + getKeteranganSO);

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();

            //inisialisasi noTR
            noTR = (flag > 10) ? ("61" + noTR) : ("610" + noTR);

            System.out.println("\n ======================== MULAI INSERT INTO TRMST");
            session.createSQLQuery("INSERT INTO TRMST(NOMOR, TRANSAKSI, NOBUKTI, TGL, TGLISI, TGLPOSTING, LOGIN, DIVISI, NOMORPH)"
                    + " VALUES(:nomorTRMST, :nomorTransaksi, '', 'NOW', 'NOW', 'NOW', :userLogin, :userDivisi, :kodePH)")
                    .setParameter("nomorTRMST", idTRMST)
                    .setParameter("nomorTransaksi", noTR)
                    .setParameter("userLogin", username)
                    .setParameter("userDivisi", cekRowUser[0].toString())
                    .setParameter("kodePH", hibernateUtil.setNomorPH(dataNomorBuktiSO[0]))
                    .executeUpdate();

            System.out.println("\n ======================== MULAI INSERT INTO TRMSTPH");
            session.createSQLQuery("INSERT INTO TRMSTPH(NOMOR, NAMA, ALAMAT, TELP, KETERANGAN)"
                    + " VALUES(:nomorTRMST,:nama,:alamat,:telp,:ket)")
                    .setParameter("nomorTRMST", idTRMST)
                    .setParameter("nama", dataNamaCustomerSO)
                    .setParameter("alamat", dataAlamatCustomerSO)
                    .setParameter("telp", dataTelpCustomerSO)
                    .setParameter("ket", getKeteranganSO)
                    .executeUpdate();
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
                //flush->clear->commit dulu, lalu open session baru dan getNoBukti TRMST diatas untuk insert noBuktiSJ TRMSTKET dibawah
                session.flush();
                session.clear();
                tx.commit();
                session = sessionFactory.openSession();
                tx = session.beginTransaction();
                String noBuktiSJ = vcekbayartrmstService.getNoBukti(Integer.parseInt(idTRMST));
            */
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTKET");
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglKirimSO[i] yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO TRMSTKET(NOMOR, TGLKIRIM, TGLJATUHTEMPO, TGLINVOICE, NOSURATJALAN, NOBUKTI, KET1, KET2)"
                    + "VALUES(:nomorTRMST, 'NOW', NULL, NULL, NULL, NULL, :getKeteranganSO, NULL)")
                    .setParameter("nomorTRMST", idTRMST)
                    .setParameter("getKeteranganSO", getKeteranganSO)   //isi keterangan sama dengan keterangan TRMSTPH
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO SUPIR"); //FAF, query insert ke tabel supir
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglKirimSO[i] yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO SUPIR(NOMOR, NAMA, PLAT, KETERANGAN)"
                    + "VALUES(:nomorTRMST, :nama, :plat, :keterangan)")
                    .setParameter("nomorTRMST", idTRMST)
                    .setParameter("nama", dataNAMA_DRIVER)   //isi keterangan sama dengan keterangan TRMSTPH
                    .setParameter("plat", dataNO_PLAT)   //isi keterangan sama dengan keterangan TRMSTPH
                    .setParameter("keterangan", dataNO_PLAT)   //isi keterangan sama dengan keterangan TRMSTPH
                    .executeUpdate();
            
//            System.out.println("\n ======================== MULAI INSERT INTO DIVISI"); //FAF, Tambahan COBA COBA
//            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglKirimSO[i] yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
//            session.createSQLQuery("INSERT INTO DIVISI(NOMOR, KODE, KETERANGAN)"
//                    + "VALUES(:nomorTRMST, :nama, :plat)")
//                    .setParameter("nomorTRMST", idTRMST)
//                    .setParameter("nama", dataNAMA_DRIVER)   //isi keterangan sama dengan keterangan TRMSTPH
//                    .setParameter("plat", dataNO_PLAT)   //isi keterangan sama dengan keterangan TRMSTPH
//                    .executeUpdate();
//            
                    /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
                    + "VALUES(:nomorTRMST, 'NOW', NULL, NULL, NULL, :noBuktiSJ, :getKeteranganSO, NULL)")
                    .setParameter("noBuktiSJ", noBuktiSJ)   //isi nobukti sama dgn nobukti TRMST utk nomorTRMST yg sama
                    */
                    
            for (int i = 0; i < jmlDataTambahBarangSO; i++) {
                //tambah argumen nomortrmst dan nomortrdet
                Object[] cekRowTrdet = (Object[]) hibernateUtil.setTRDET(dataNomorBuktiSO[i], dataNomorTRDETSO[i], dataNomorTRMSTSO[i], false);

                //utk nambah kurang stok dan hitung hpp
                System.out.println("\n #############==============############# MULAI EXECUTE PROCEDURE SPPUBTRDETINSERTDATA");
                Query noHPP = session.createSQLQuery("EXECUTE PROCEDURE SPPUBTRDETINSERTDATA("
                        + ":pNomorTrMst,"   //1     //#1 non hibernateUtil.setTRDET, langsung dari argumen/variabel method (idTRMST)
                        + ":pNomorStok,"    //2
                        + ":pBanyaknya,"    //3     //#2 non hibernateUtil.setTRDET, (dataQtyKirimSO[i])
                        + ":pSatuan,"       //4
                        + ":pHarga,"        //5
                        + ":pSatuan0,"      //6
                        + ":pSatuan1,"      //7
                        + ":pFaktor,"       //8
                        + ":pLokasi2,"      //9
                        + ":pDiscPersen0,"  //10
                        + ":pDiscRp0,"      //11
                        + ":pDiscPersen1,"  //12
                        + ":pDiscRp1,"      //13
                        + ":pPpnPersen,"    //14
                        + ":pPpnRp,"        //15
                        + ":pBiayaRp0,"     //16
                        + ":pBiayaRp1,"     //17
                        + ":pKet0,"         //18
                        + ":pNamaStok,"     //19
                        + ":pNomorRef,"     //20
                        + ":pTgl1,"         //21,   //#3 non hibernateUtil.setTRDET, (dataTglKirimSO[i]), INI YG JADI TANGGAL KIRIM SO 
                        + "'NOW')")         //22    //#4 non hibernateUtil.setTRDET, fungsi firebird, INI YG JADI TANGGAL CETAK SJ
                        .setParameter("pNomorTrMst", idTRMST)
                        .setParameter("pNomorStok", cekRowTrdet[0].toString())
                        .setParameter("pBanyaknya", dataQtyKirimSO[i])
                        .setParameter("pSatuan", cekRowTrdet[1].toString())
                        .setParameter("pHarga", cekRowTrdet[2].toString())
                        .setParameter("pSatuan0", cekRowTrdet[3].toString())
                        .setParameter("pSatuan1", cekRowTrdet[4].toString())
                        .setParameter("pFaktor", cekRowTrdet[5].toString())
                        .setParameter("pLokasi2", cekRowTrdet[6].toString())
                        .setParameter("pDiscPersen0", cekRowTrdet[7].toString())
                        .setParameter("pDiscRp0", cekRowTrdet[8].toString())
                        .setParameter("pDiscPersen1", cekRowTrdet[9].toString())
                        .setParameter("pDiscRp1", cekRowTrdet[10].toString())
                        .setParameter("pPpnPersen", cekRowTrdet[11].toString())
                        .setParameter("pPpnRp", cekRowTrdet[12].toString())
                        .setParameter("pBiayaRp0", cekRowTrdet[13].toString())
                        .setParameter("pBiayaRp1", cekRowTrdet[14].toString())
                        .setParameter("pKet0", cekRowTrdet[15].toString())
                        .setParameter("pNamaStok", cekRowTrdet[16].toString())
                        .setParameter("pNomorRef", cekRowTrdet[17].toString())
                        .setParameter("pTgl1", dataTglKirimSO[i]);
                //muat hasil query execute SP berupa RHPP bertipe double ke dalam List(ArrayList) bertipe double juga
                List<Double> alHPP = noHPP.list();
                //objHPP bertipe Object[] di-cast menjadi Array
                objHPP = alHPP.toArray();
                System.out.println("\n ======================== KETERANGAN PARAMETER SPPUBTRDETINSERTDATA");
                System.out.println("noHPP = " + noHPP);
                System.out.println("alHPP = " + alHPP);
            }

            System.out.println("\n ======================== objHPP");
            System.out.println("objHPP = " + Arrays.toString(objHPP));

            if ((Double) objHPP[0] != null) {
                tx.commit();
            } else {
                idTRMST = "0";
                tx.rollback();  //rollback tx disini (tx = session.beginTransaction();)
            }
        } catch (HibernateException | NullPointerException ex) {
            idTRMST = "0";
            tx.rollback();  //ada rollback tx lg disini
        }

        //JIKA ADA/BERHASIL GENERATE NOMOR TRMST BARU
        if (!"0".equals(idTRMST)) {
            for (int j = 0; j < jmlDataTambahBarangSO; j++) {
                Double statusKirim = hibernateUtil.cekStatus(Integer.valueOf(dataNomorTRDETSO[j]));
                System.out.println("\n ======================== CEK STATUS KIRIM");
                System.out.println("statusKirim = " + statusKirim);

                //jika stok barang yang sdh dikirim NEGATIF ATAU MINUS...
                if (statusKirim < 0.0) {
                    int nmTrmst = Integer.parseInt(idTRMST);
                    tx = session.beginTransaction();
                    /* FLAG */
                    session.createQuery("DELETE FROM Trmst WHERE nomor = :ntrmst")
                            .setParameter("ntrmst", nmTrmst)
                            .executeUpdate();
                    tx.commit();
                    idTRMST = "0";
                    break;
                }
            }
        }
        session.clear();
        session.close();
        return Integer.parseInt(idTRMST);
    }    
    
    //SUBRUTIN TRANSAKSI INSERT KIRIM MUTASI (addKirimMutasi)
    @Override
    public int addKirimMutasi(int dataProsesMutasi, String[] dataNomorBuktiOM, String[] dataNomorTRMSTOM, String[] dataNomorTRDETOM,
        String[] dataQtyKirimOM, String[] dataTglOM, String[] dataMemoMutasi)
    {
        System.out.println("\n\n ========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================\n"
                + "MASUK SUBRUTIN TRANSAKSI INSERT KIRIM MUTASI (addKirimMutasi)\n"
                + "========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================");
        
        //SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE
        Integer flagIDTRMST = Integer.parseInt(hibernateUtil.getNomorTRMST());
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("51" + getInfoLokasi()) : ("510" + getInfoLokasi()); 
        
        //local variable
        Session session = null;
        Transaction tx = null;
        Object[] objHPP = null;
        Query RHPP;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            
            //ambil data field keterangan master OM di tabel TRMSTKET
            String ketMutasi = hibernateUtil.getKetMutasi(dataNomorBuktiOM[0]);
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMST");
            session.createSQLQuery("INSERT INTO TRMST(NOMOR, TRANSAKSI, NOBUKTI, TGL, TGLISI, TGLPOSTING, LOGIN, DIVISI, NOMORPH)"
                    + " VALUES(:nomorTRMST, :nomorTransaksi, '', 'NOW', 'NOW', 'NOW', :userLogin, :userDivisi, :kodePH)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nomorTransaksi", nomorTRANSAKSI)
                    .setParameter("userLogin", getInfoKodeUser())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("userDivisi", getInfoNomorDivisi())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("kodePH", hibernateUtil.setNomorPH(dataNomorBuktiOM[0]))
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTPH");
            session.createSQLQuery("INSERT INTO TRMSTPH(NOMOR, NAMA, ALAMAT, TELP, KETERANGAN)"
                    + " VALUES(:nomorTRMST,:nama,:alamat,:telp,:ket)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nama", "-")         //hardcode, mungkin perlu konstanta?
                    .setParameter("alamat", "-")     //hardcode
                    .setParameter("telp", "-")         //hardcode
                    .setParameter("ket", ketMutasi)     //isi dgn keterangan master (ket1 dari trmstket yg om). keterangan ini muncul di jasper.
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
                //flush->clear->commit dulu, lalu open session baru dan getNoBukti TRMST diatas untuk insert noBuktiKirimMutasi TRMSTKET dibawah
                session.flush();
                session.clear();
                tx.commit();
                session = sessionFactory.openSession();
                tx = session.beginTransaction();
                String noBuktiKirimMutasi = vcekbayartrmstService.getNoBukti(flagIDTRMST);
            */
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTKET");
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglOM[i] (tanggal order mutasi) yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO TRMSTKET(NOMOR, TGLKIRIM, TGLJATUHTEMPO, TGLINVOICE, NOSURATJALAN, NOBUKTI, KET1, KET2)"
                    + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, NULL, :ketMUT, :memoMUT)")  //utk concat firebird: ||
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("ketMUT", ketMutasi)      //isi dgn keterangan master (ket1 dari trmstket yg om). keterangan ini muncul di jasper.
                    .setParameter("memoMUT", dataMemoMutasi[0])
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
            + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, :noBuktiKirimMutasi, :ketMUT, :memoMUT)")  //utk concat firebird: ||
            .setParameter("noBuktiKirimMutasi", noBuktiKirimMutasi) //isi nobukti sama dgn nobukti TRMST utk nomorTRMST yg sama
            */
            
            for (int i = 0; i < dataProsesMutasi; i++) {
                
                //tambah argumen nomortrmst dan nomortrdet
                Object[] cekRowTrdet = (Object[]) hibernateUtil.setTRDET(dataNomorBuktiOM[i], dataNomorTRDETOM[i], dataNomorTRMSTOM[i], true);

                //utk nambah kurang stok dan hitung hpp
                System.out.println("\n #############==============############# MULAI EXECUTE PROCEDURE SPPUBTRDETINSERTDATA KE: " + i);
                RHPP = session.createSQLQuery("EXECUTE PROCEDURE SPPUBTRDETINSERTDATA("
                        + ":pNomorTrMst," //1,    //#1 non hibernateUtil.setTRDET, langsung dari argumen/variabel method
                        + ":pNomorStok," //2
                        + ":pBanyaknya," //3,    //#2 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, ini adalah jml kirim mutasi
                        + ":pSatuan," //4
                        + ":pHarga," //5
                        + ":pSatuan0," //6
                        + ":pSatuan1," //7
                        + ":pFaktor," //8
                        + ":pLokasi2," //9
                        + ":pDiscPersen0," //10
                        + ":pDiscRp0," //11
                        + ":pDiscPersen1," //12
                        + ":pDiscRp1," //13
                        + ":pPpnPersen," //14
                        + ":pPpnRp," //15
                        + ":pBiayaRp0," //16
                        + ":pBiayaRp1," //17
                        + ":pKet0," //18
                        + ":pNamaStok," //19
                        + ":pNomorRef," //20
                        + ":pTgl1," //21,   //#3 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, INI YG JADI TANGGAL ORDER MUTASI
                        + "'NOW')") //22    //#4 non hibernateUtil.setTRDET, fungsi firebird, INI YG JADI TANGGAL CETAK SJ
                        .setParameter("pNomorTrMst", flagIDTRMST.toString())
                        .setParameter("pNomorStok", cekRowTrdet[0].toString())
                        .setParameter("pBanyaknya", dataQtyKirimOM[i])
                        .setParameter("pSatuan", cekRowTrdet[1].toString())
                        .setParameter("pHarga", cekRowTrdet[2].toString())
                        .setParameter("pSatuan0", cekRowTrdet[3].toString())
                        .setParameter("pSatuan1", cekRowTrdet[4].toString())
                        .setParameter("pFaktor", cekRowTrdet[5].toString())
                        .setParameter("pLokasi2", cekRowTrdet[6].toString())
                        .setParameter("pDiscPersen0", cekRowTrdet[7].toString())
                        .setParameter("pDiscRp0", cekRowTrdet[8].toString())
                        .setParameter("pDiscPersen1", cekRowTrdet[9].toString())
                        .setParameter("pDiscRp1", cekRowTrdet[10].toString())
                        .setParameter("pPpnPersen", cekRowTrdet[11].toString())
                        .setParameter("pPpnRp", cekRowTrdet[12].toString())
                        .setParameter("pBiayaRp0", cekRowTrdet[13].toString())
                        .setParameter("pBiayaRp1", cekRowTrdet[14].toString())
                        .setParameter("pKet0", cekRowTrdet[15].toString())
                        .setParameter("pNamaStok", cekRowTrdet[16].toString())
                        .setParameter("pNomorRef", cekRowTrdet[17].toString())
                        .setParameter("pTgl1", dataTglOM[i]);
                //muat hasil query execute SP berupa RHPP bertipe double ke dalam List(ArrayList) bertipe double juga
                List<Double> alHPP = RHPP.list();
                //objHPP bertipe Object[] di-cast menjadi Array
                objHPP = alHPP.toArray();

                System.out.println("\n ======================== Query RHPP = EXECUTE PROCEDURE SPPUBTRDETINSERTDATA");
                System.out.println("RHPP = " + RHPP);

                System.out.println("\n ======================== RESULT SPPUBTRDETINSERTDATA -> alHPP, objHPP");
                System.out.println("alHPP = " + alHPP);
                //System.out.println("objHPP[" + i + "] = " + objHPP[i]);   //ArrayIndexOutOfBoundsException: 1, krn lihat dibawah
                System.out.println("objHPP[0] = " + objHPP[0]); //session.createSQLQuery "SPPUBTRDETINSERTDATA" selalu return 1 value = index 0

            }//end for-loop

            if ((Double) objHPP[0] != null) {
                session.flush();
                session.clear();
                tx.commit();
                System.out.println("\n ##################### TRANSAKSI BERHASIL COMMIT");
            } else {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI objHPP[0] == null");
            }

        } catch (HibernateException ex) {  //jangan catch NullPointerException
            if (tx != null) {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI catch HibernateException");
            }
            throw new DataAccessResourceFailureException("Hibernate Session GAGAL. Scroll ke bawah untuk lihat penyebabnya.", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flagIDTRMST;
    }
    
    /**
     * sonny, add SUBRUTIN TRANSAKSI INSERT TERIMA RETUR JUAL (addTerimaRJ)
     * UPDATE 2019-11-29: add data trmstph agar tdk hardcode lagi
     * @param dataProsesTRJ
     * @param dataNomorBuktiTRJ
     * @param dataNomorTRMSTTRJ
     * @param dataNomorTRDETTRJ
     * @param dataQtyTRJ
     * @param dataTglTRJ
     * @param dataMemoTRJ
     * @param dataNamaTRMSTPHORJ
     * @param dataAlamatTRMSTPHORJ
     * @param dataTelpTRMSTPHORJ
     * @return 
     */
    @Override
    public int addTerimaRJ(int dataProsesTRJ, String[] dataNomorBuktiTRJ, String[] dataNomorTRMSTTRJ, String[] dataNomorTRDETTRJ,
        String[] dataQtyTRJ, String[] dataTglTRJ, String[] dataMemoTRJ, String[] dataNamaTRMSTPHORJ, String[] dataAlamatTRMSTPHORJ,
        String[] dataTelpTRMSTPHORJ)
    {
        System.out.println("\n\n ========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================\n"
                + "MASUK SUBRUTIN TRANSAKSI INSERT TERIMA RETUR JUAL (addTerimaRJ)\n"
                + "========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================");
        
        //SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE
        Integer flagIDTRMST = Integer.parseInt(hibernateUtil.getNomorTRMST());
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("31" + getInfoLokasi()) : ("310" + getInfoLokasi()); 
        
        //local variable
        Session session = null;
        Transaction tx = null;
        Object[] objHPP = null;
        Query RHPP;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            
            //ambil data field keterangan master ORJ di tabel TRMSTKET untuk disimpan di ketTRJ
            String ketTRJ = hibernateUtil.getKetReturJual(dataNomorBuktiTRJ[0]);
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMST");
            session.createSQLQuery("INSERT INTO TRMST(NOMOR, TRANSAKSI, NOBUKTI, TGL, TGLISI, TGLPOSTING, LOGIN, DIVISI, NOMORPH)"
                    + " VALUES(:nomorTRMST, :nomorTransaksi, '', 'NOW', 'NOW', 'NOW', :userLogin, :userDivisi, :kodePH)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nomorTransaksi", nomorTRANSAKSI)
                    .setParameter("userLogin", getInfoKodeUser())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("userDivisi", getInfoNomorDivisi())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("kodePH", hibernateUtil.setNomorPH(dataNomorBuktiTRJ[0]))
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTPH");
            session.createSQLQuery("INSERT INTO TRMSTPH(NOMOR, NAMA, ALAMAT, TELP, KETERANGAN)"
                    + " VALUES(:nomorTRMST,:nama,:alamat,:telp,:ket)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nama", dataNamaTRMSTPHORJ[0])
                    .setParameter("alamat", dataAlamatTRMSTPHORJ[0])
                    .setParameter("telp", dataTelpTRMSTPHORJ[0])
                    .setParameter("ket", ketTRJ)     //isi dgn keterangan master (ket1 dari trmstket yg ORJ). keterangan ini muncul di jasper.
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
                //flush->clear->commit dulu, lalu open session baru dan getNoBukti TRMST diatas untuk insert noBuktiKirimMutasi TRMSTKET dibawah
                session.flush();
                session.clear();
                tx.commit();
                session = sessionFactory.openSession();
                tx = session.beginTransaction();
                String noBuktiKirimMutasi = vcekbayartrmstService.getNoBukti(flagIDTRMST);
            */
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTKET");
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglTRJ[i] (tanggal terima retur jual) yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO TRMSTKET(NOMOR, TGLKIRIM, TGLJATUHTEMPO, TGLINVOICE, NOSURATJALAN, NOBUKTI, KET1, KET2)"
                    + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, NULL, :ketTRJ, :memoTRJ)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("ketTRJ", ketTRJ)      //isi dgn keterangan master (ket1 dari trmstket yg ORJ). keterangan ini muncul di jasper.
                    .setParameter("memoTRJ", dataMemoTRJ[0])
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
            + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, NULL, :ketTRJ, :memoTRJ)")  //utk concat firebird: ||
            */
            
            for (int i = 0; i < dataProsesTRJ; i++) {
                //tambah argumen nomortrmst dan nomortrdet
                Object[] cekRowTrdet = (Object[]) hibernateUtil.setTRDET(dataNomorBuktiTRJ[i], dataNomorTRDETTRJ[i], dataNomorTRMSTTRJ[i], false);

                //utk nambah kurang stok dan hitung hpp
                System.out.println("\n #############==============############# MULAI EXECUTE PROCEDURE SPPUBTRDETINSERTDATA KE: " + i);
                RHPP = session.createSQLQuery("EXECUTE PROCEDURE SPPUBTRDETINSERTDATA("
                        + ":pNomorTrMst," //1,    //#1 non hibernateUtil.setTRDET, langsung dari argumen/variabel method
                        + ":pNomorStok," //2
                        + ":pBanyaknya," //3,    //#2 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, ini adalah jml terima RJ
                        + ":pSatuan," //4
                        + ":pHarga," //5
                        + ":pSatuan0," //6
                        + ":pSatuan1," //7
                        + ":pFaktor," //8
                        + ":pLokasi2," //9
                        + ":pDiscPersen0," //10
                        + ":pDiscRp0," //11
                        + ":pDiscPersen1," //12
                        + ":pDiscRp1," //13
                        + ":pPpnPersen," //14
                        + ":pPpnRp," //15
                        + ":pBiayaRp0," //16
                        + ":pBiayaRp1," //17
                        + ":pKet0," //18
                        + ":pNamaStok," //19
                        + ":pNomorRef," //20
                        + ":pTgl1," //21,   //#3 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, INI YG JADI TANGGAL ORDER RJ
                        + "'NOW')") //22    //#4 non hibernateUtil.setTRDET, fungsi firebird, INI YG JADI TANGGAL CETAK SURAT RJ
                        .setParameter("pNomorTrMst", flagIDTRMST.toString())
                        .setParameter("pNomorStok", cekRowTrdet[0].toString())
                        .setParameter("pBanyaknya", dataQtyTRJ[i])
                        .setParameter("pSatuan", cekRowTrdet[1].toString())
                        .setParameter("pHarga", cekRowTrdet[2].toString())
                        .setParameter("pSatuan0", cekRowTrdet[3].toString())
                        .setParameter("pSatuan1", cekRowTrdet[4].toString())
                        .setParameter("pFaktor", cekRowTrdet[5].toString())
                        .setParameter("pLokasi2", cekRowTrdet[6].toString())
                        .setParameter("pDiscPersen0", cekRowTrdet[7].toString())
                        .setParameter("pDiscRp0", cekRowTrdet[8].toString())
                        .setParameter("pDiscPersen1", cekRowTrdet[9].toString())
                        .setParameter("pDiscRp1", cekRowTrdet[10].toString())
                        .setParameter("pPpnPersen", cekRowTrdet[11].toString())
                        .setParameter("pPpnRp", cekRowTrdet[12].toString())
                        .setParameter("pBiayaRp0", cekRowTrdet[13].toString())
                        .setParameter("pBiayaRp1", cekRowTrdet[14].toString())
                        .setParameter("pKet0", cekRowTrdet[15].toString())
                        .setParameter("pNamaStok", cekRowTrdet[16].toString())
                        .setParameter("pNomorRef", cekRowTrdet[17].toString())
                        .setParameter("pTgl1", dataTglTRJ[i]);
                //muat hasil query execute SP berupa RHPP bertipe double ke dalam List(ArrayList) bertipe double juga
                List<Double> alHPP = RHPP.list();
                //objHPP bertipe Object[] di-cast menjadi Array
                objHPP = alHPP.toArray();

                System.out.println("\n ======================== Query RHPP = EXECUTE PROCEDURE SPPUBTRDETINSERTDATA");
                System.out.println("RHPP = " + RHPP);

                System.out.println("\n ======================== RESULT SPPUBTRDETINSERTDATA -> alHPP, objHPP");
                System.out.println("alHPP = " + alHPP);
                //System.out.println("objHPP[" + i + "] = " + objHPP[i]);   //ArrayIndexOutOfBoundsException: 1, krn lihat dibawah
                System.out.println("objHPP[0] = " + objHPP[0]); //session.createSQLQuery "SPPUBTRDETINSERTDATA" selalu return 1 value = index 0

            }//end for-loop

            if ((Double) objHPP[0] != null) {
                session.flush();
                session.clear();
                tx.commit();
                System.out.println("\n ##################### TRANSAKSI BERHASIL COMMIT");
            } else {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI objHPP[0] == null");
            }

        } catch (HibernateException ex) {  //jangan catch NullPointerException
            if (tx != null) {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI catch HibernateException");
            }
            throw new DataAccessResourceFailureException("Hibernate Session GAGAL. Scroll ke bawah untuk lihat penyebabnya.", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flagIDTRMST;
    }
    
    /**
     * SUBRUTIN TRANSAKSI INSERT TERIMA RETUR BELI (addTerimaRB)
     * UPDATE 2019-12-09: add data trmstph agar tdk hardcode lagi
     * @param dataProsesTRB
     * @param dataNomorBuktiTRB
     * @param dataNomorTRMSTTRB
     * @param dataNomorTRDETTRB
     * @param dataQtyTRB
     * @param dataTglTRB
     * @param dataMemoTRB
     * @param dataSupplierTRB
     * @param dataAlamatTRMSTPHORB
     * @param dataTelpTRMSTPHORB
     * @return 
     */
    @Override
    public int addTerimaRB(int dataProsesTRB, String[] dataNomorBuktiTRB, String[] dataNomorTRMSTTRB, String[] dataNomorTRDETTRB,
        String[] dataQtyTRB, String[] dataTglTRB, String[] dataMemoTRB, String[] dataSupplierTRB, String[] dataAlamatTRMSTPHORB,
        String[] dataTelpTRMSTPHORB)
    {
        System.out.println("\n\n ========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================\n"
                + "MASUK SUBRUTIN TRANSAKSI INSERT TERIMA RETUR BELI (addTerimaRB)\n"
                + "========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================");
        
        //SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE
        Integer flagIDTRMST = Integer.parseInt(hibernateUtil.getNomorTRMST());
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("71" + getInfoLokasi()) : ("710" + getInfoLokasi()); 
        
        //local variable
        Session session = null;
        Transaction tx = null;
        Object[] objHPP = null;
        Query RHPP;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            
            //ambil data field keterangan master ORB di tabel TRMSTKET
            String ketTRB = hibernateUtil.getKetReturBeli(dataNomorBuktiTRB[0]);
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMST");
            session.createSQLQuery("INSERT INTO TRMST(NOMOR, TRANSAKSI, NOBUKTI, TGL, TGLISI, TGLPOSTING, LOGIN, DIVISI, NOMORPH)"
                    + " VALUES(:nomorTRMST, :nomorTransaksi, '', 'NOW', 'NOW', 'NOW', :userLogin, :userDivisi, :kodePH)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nomorTransaksi", nomorTRANSAKSI)
                    .setParameter("userLogin", getInfoKodeUser())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("userDivisi", getInfoNomorDivisi())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("kodePH", hibernateUtil.setNomorPH(dataNomorBuktiTRB[0]))
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTPH");
            session.createSQLQuery("INSERT INTO TRMSTPH(NOMOR, NAMA, ALAMAT, TELP, KETERANGAN)"
                    + " VALUES(:nomorTRMST,:nama,:alamat,:telp,:ket)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nama", dataSupplierTRB[0])
                    .setParameter("alamat", dataAlamatTRMSTPHORB[0])
                    .setParameter("telp", dataTelpTRMSTPHORB[0])
                    .setParameter("ket", ketTRB)     //isi dgn keterangan master (ket1 dari trmstket yg ORB). keterangan ini muncul di jasper.
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
                //flush->clear->commit dulu, lalu open session baru dan getNoBukti TRMST diatas untuk insert noBuktiKirimMutasi TRMSTKET dibawah
                session.flush();
                session.clear();
                tx.commit();
                session = sessionFactory.openSession();
                tx = session.beginTransaction();
                String noBuktiKirimMutasi = vcekbayartrmstService.getNoBukti(flagIDTRMST);
            */
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTKET");
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglTRJ[i] (tanggal terima retur jual) yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO TRMSTKET(NOMOR, TGLKIRIM, TGLJATUHTEMPO, TGLINVOICE, NOSURATJALAN, NOBUKTI, KET1, KET2)"
                    + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, NULL, :ketTRB, :memoTRB)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("ketTRB", ketTRB)      //isi dgn keterangan master (ket1 dari trmstket yg ORB). keterangan ini muncul di jasper.
                    .setParameter("memoTRB", dataMemoTRB[0])
                    .executeUpdate();
            
            /* OFF KRN BUKA TUTUP SESSION HIBERNATE BIKIN MEMORY LEAK OUT OF RESOURCE
            + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, :noBuktiKirimMutasi, :ketTRB, :memoTRB)")  //utk concat firebird: ||
            */
            
            for (int i = 0; i < dataProsesTRB; i++) {
                //tambah argumen nomortrmst dan nomortrdet
                Object[] cekRowTrdet = (Object[]) hibernateUtil.setTRDET(dataNomorBuktiTRB[i], dataNomorTRDETTRB[i], dataNomorTRMSTTRB[i], false);

                //utk nambah kurang stok dan hitung hpp
                System.out.println("\n #############==============############# MULAI EXECUTE PROCEDURE SPPUBTRDETINSERTDATA KE: " + i);
                RHPP = session.createSQLQuery("EXECUTE PROCEDURE SPPUBTRDETINSERTDATA("
                        + ":pNomorTrMst," //1,    //#1 non hibernateUtil.setTRDET, langsung dari argumen/variabel method
                        + ":pNomorStok," //2
                        + ":pBanyaknya," //3,    //#2 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, ini adalah jml terima RB
                        + ":pSatuan," //4
                        + ":pHarga," //5
                        + ":pSatuan0," //6
                        + ":pSatuan1," //7
                        + ":pFaktor," //8
                        + ":pLokasi2," //9
                        + ":pDiscPersen0," //10
                        + ":pDiscRp0," //11
                        + ":pDiscPersen1," //12
                        + ":pDiscRp1," //13
                        + ":pPpnPersen," //14
                        + ":pPpnRp," //15
                        + ":pBiayaRp0," //16
                        + ":pBiayaRp1," //17
                        + ":pKet0," //18
                        + ":pNamaStok," //19
                        + ":pNomorRef," //20
                        + ":pTgl1," //21,   //#3 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, INI YG JADI TANGGAL ORDER RB
                        + "'NOW')") //22    //#4 non hibernateUtil.setTRDET, fungsi firebird, INI YG JADI TANGGAL CETAK SURAT RB
                        .setParameter("pNomorTrMst", flagIDTRMST.toString())
                        .setParameter("pNomorStok", cekRowTrdet[0].toString())
                        .setParameter("pBanyaknya", dataQtyTRB[i])
                        .setParameter("pSatuan", cekRowTrdet[1].toString())
                        .setParameter("pHarga", cekRowTrdet[2].toString())
                        .setParameter("pSatuan0", cekRowTrdet[3].toString())
                        .setParameter("pSatuan1", cekRowTrdet[4].toString())
                        .setParameter("pFaktor", cekRowTrdet[5].toString())
                        .setParameter("pLokasi2", cekRowTrdet[6].toString())
                        .setParameter("pDiscPersen0", cekRowTrdet[7].toString())
                        .setParameter("pDiscRp0", cekRowTrdet[8].toString())
                        .setParameter("pDiscPersen1", cekRowTrdet[9].toString())
                        .setParameter("pDiscRp1", cekRowTrdet[10].toString())
                        .setParameter("pPpnPersen", cekRowTrdet[11].toString())
                        .setParameter("pPpnRp", cekRowTrdet[12].toString())
                        .setParameter("pBiayaRp0", cekRowTrdet[13].toString())
                        .setParameter("pBiayaRp1", cekRowTrdet[14].toString())
                        .setParameter("pKet0", cekRowTrdet[15].toString())
                        .setParameter("pNamaStok", cekRowTrdet[16].toString())
                        .setParameter("pNomorRef", cekRowTrdet[17].toString())
                        .setParameter("pTgl1", dataTglTRB[i]);
                //muat hasil query execute SP berupa RHPP bertipe double ke dalam List(ArrayList) bertipe double juga
                List<Double> alHPP = RHPP.list();
                //objHPP bertipe Object[] di-cast menjadi Array
                objHPP = alHPP.toArray();

                System.out.println("\n ======================== Query RHPP = EXECUTE PROCEDURE SPPUBTRDETINSERTDATA");
                System.out.println("RHPP = " + RHPP);

                System.out.println("\n ======================== RESULT SPPUBTRDETINSERTDATA -> alHPP, objHPP");
                System.out.println("alHPP = " + alHPP);
                //System.out.println("objHPP[" + i + "] = " + objHPP[i]);   //ArrayIndexOutOfBoundsException: 1, krn lihat dibawah
                System.out.println("objHPP[0] = " + objHPP[0]); //session.createSQLQuery "SPPUBTRDETINSERTDATA" selalu return 1 value = index 0

            }//end for-loop

            if ((Double) objHPP[0] != null) {
                session.flush();
                session.clear();
                tx.commit();
                System.out.println("\n ##################### TRANSAKSI BERHASIL COMMIT");
            } else {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI objHPP[0] == null");
            }

        } catch (HibernateException ex) {  //jangan catch NullPointerException
            if (tx != null) {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI catch HibernateException");
            }
            throw new DataAccessResourceFailureException("Hibernate Session GAGAL. Scroll ke bawah untuk lihat penyebabnya.", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flagIDTRMST;
    }
    
    //SUBRUTIN TRANSAKSI INSERT TERIMA LPB (addTerimaLPB)
    @Override
    public int addTerimaLPB(int dataProsesLPB, String[] dataNomorBuktiLPB, String dataNamaSupplierLPB, String dataAlamatSupplierLPB, 
            String dataTelpSupplierLPB, String[] dataNomorTRMSTLPB, String[] dataNomorTRDETLPB, String[] dataQtyLPB, String[] dataTglLPB,
            String[] dataMemoLPB)
    {
        System.out.println("\n\n ========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================\n"
                + "MASUK SUBRUTIN TRANSAKSI INSERT TERIMA LPB (addTerimaLPB)\n"
                + "========================$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$========================");
        
        //SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE
        Integer flagIDTRMST = Integer.parseInt(hibernateUtil.getNomorTRMST());
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("11" + getInfoLokasi()) : ("110" + getInfoLokasi()); 
        
        //local variable
        Session session = null;
        Transaction tx = null;
        Object[] objHPP = null;
        Query RHPP;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            
            //khusus LPB, ambil data field keterangan master PO di tabel TRMSTPH untuk nanti concat/merge dengan MEMO.
            String ketLPB = hibernateUtil.getKetPO(dataNomorBuktiLPB[0]);
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMST");
            session.createSQLQuery("INSERT INTO TRMST(NOMOR, TRANSAKSI, NOBUKTI, TGL, TGLISI, TGLPOSTING, LOGIN, DIVISI, NOMORPH)"
                    + " VALUES(:nomorTRMST, :nomorTransaksi, '', 'NOW', 'NOW', 'NOW', :userLogin, :userDivisi, :kodePH)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nomorTransaksi", nomorTRANSAKSI)
                    .setParameter("userLogin", getInfoKodeUser())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("userDivisi", getInfoNomorDivisi())   //agar tidak menggunakan Object[] cekRowUser
                    .setParameter("kodePH", hibernateUtil.setNomorPH(dataNomorBuktiLPB[0]))
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTPH");
            session.createSQLQuery("INSERT INTO TRMSTPH(NOMOR, NAMA, ALAMAT, TELP, KETERANGAN)"
                    + " VALUES(:nomorTRMST,:nama,:alamat,:telp,:ket)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("nama", dataNamaSupplierLPB)      //khusus LPB
                    .setParameter("alamat", dataAlamatSupplierLPB)  //khusus LPB
                    .setParameter("telp", dataTelpSupplierLPB)      //khusus LPB
//                    .setParameter("ket", dataMemoLPB[0])     //khusus LPB, isi dgn memo. keterangan ini muncul di jasper.
                    .setParameter("ket", ketLPB)     //isi dgn keterangan master (TRMSTPH.keterangan yg PO). keterangan ini muncul di jasper.
                    .executeUpdate();
            
            System.out.println("\n ======================== MULAI INSERT INTO TRMSTKET");
            //TGLKIRIM TRMSTKET dibawah ini beda dgn dataTglTRJ[i] (tanggal terima retur jual) yg ke TRDETKETTGL (execute SPPUBTRDETINSERTDATA)
            session.createSQLQuery("INSERT INTO TRMSTKET(NOMOR, TGLKIRIM, TGLJATUHTEMPO, TGLINVOICE, NOSURATJALAN, NOBUKTI, KET1, KET2)"
                    + " VALUES(:nomorTRMST,'NOW', NULL, NULL, NULL, NULL, :ketLPB, :memoLPB)")
                    .setParameter("nomorTRMST", flagIDTRMST.toString())
                    .setParameter("ketLPB", ketLPB)      //isi dgn keterangan master (ket1 dari trmstket yg ORB). keterangan ini muncul di jasper.
                    .setParameter("memoLPB", dataMemoLPB[0])
                    .executeUpdate();
            
            for (int i = 0; i < dataProsesLPB; i++) {
                //tambah argumen nomortrmst dan nomortrdet
                Object[] cekRowTrdet = (Object[]) hibernateUtil.setTRDET(dataNomorBuktiLPB[i], dataNomorTRDETLPB[i], dataNomorTRMSTLPB[i], false);

                //utk nambah kurang stok dan hitung hpp
                System.out.println("\n #############==============############# MULAI EXECUTE PROCEDURE SPPUBTRDETINSERTDATA KE: " + i);
                RHPP = session.createSQLQuery("EXECUTE PROCEDURE SPPUBTRDETINSERTDATA("
                        + ":pNomorTrMst," //1,    //#1 non hibernateUtil.setTRDET, langsung dari argumen/variabel method
                        + ":pNomorStok," //2
                        + ":pBanyaknya," //3,    //#2 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, ini adalah jml terima RB
                        + ":pSatuan," //4
                        + ":pHarga," //5
                        + ":pSatuan0," //6
                        + ":pSatuan1," //7
                        + ":pFaktor," //8
                        + ":pLokasi2," //9
                        + ":pDiscPersen0," //10
                        + ":pDiscRp0," //11
                        + ":pDiscPersen1," //12
                        + ":pDiscRp1," //13
                        + ":pPpnPersen," //14
                        + ":pPpnRp," //15
                        + ":pBiayaRp0," //16
                        + ":pBiayaRp1," //17
                        + ":pKet0," //18
                        + ":pNamaStok," //19
                        + ":pNomorRef," //20
                        + ":pTgl1," //21,   //#3 non hibernateUtil.setTRDET, langsung dari argumen/variabel method, INI YG JADI TANGGAL ORDER RB
                        + "'NOW')") //22    //#4 non hibernateUtil.setTRDET, fungsi firebird, INI YG JADI TANGGAL CETAK SURAT RB
                        .setParameter("pNomorTrMst", flagIDTRMST.toString())
                        .setParameter("pNomorStok", cekRowTrdet[0].toString())
                        .setParameter("pBanyaknya", dataQtyLPB[i])
                        .setParameter("pSatuan", cekRowTrdet[1].toString())
                        .setParameter("pHarga", cekRowTrdet[2].toString())
                        .setParameter("pSatuan0", cekRowTrdet[3].toString())
                        .setParameter("pSatuan1", cekRowTrdet[4].toString())
                        .setParameter("pFaktor", cekRowTrdet[5].toString())
                        .setParameter("pLokasi2", cekRowTrdet[6].toString())
                        .setParameter("pDiscPersen0", cekRowTrdet[7].toString())
                        .setParameter("pDiscRp0", cekRowTrdet[8].toString())
                        .setParameter("pDiscPersen1", cekRowTrdet[9].toString())
                        .setParameter("pDiscRp1", cekRowTrdet[10].toString())
                        .setParameter("pPpnPersen", cekRowTrdet[11].toString())
                        .setParameter("pPpnRp", cekRowTrdet[12].toString())
                        .setParameter("pBiayaRp0", cekRowTrdet[13].toString())
                        .setParameter("pBiayaRp1", cekRowTrdet[14].toString())
                        .setParameter("pKet0", cekRowTrdet[15].toString())
                        .setParameter("pNamaStok", cekRowTrdet[16].toString())
                        .setParameter("pNomorRef", cekRowTrdet[17].toString())
                        .setParameter("pTgl1", dataTglLPB[i]);
                //muat hasil query execute SP berupa RHPP bertipe double ke dalam List(ArrayList) bertipe double juga
                List<Double> alHPP = RHPP.list();
                //objHPP bertipe Object[] di-cast menjadi Array
                objHPP = alHPP.toArray();

                System.out.println("\n ======================== Query RHPP = EXECUTE PROCEDURE SPPUBTRDETINSERTDATA");
                System.out.println("RHPP = " + RHPP);

                System.out.println("\n ======================== RESULT SPPUBTRDETINSERTDATA -> alHPP, objHPP");
                System.out.println("alHPP = " + alHPP);
                //System.out.println("objHPP[" + i + "] = " + objHPP[i]);   //ArrayIndexOutOfBoundsException: 1, krn lihat dibawah
                System.out.println("objHPP[0] = " + objHPP[0]); //session.createSQLQuery "SPPUBTRDETINSERTDATA" selalu return 1 value = index 0

            }//end for-loop

            if ((Double) objHPP[0] != null) {
                session.flush();
                session.clear();
                tx.commit();
                System.out.println("\n ##################### TRANSAKSI BERHASIL COMMIT");
            } else {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI objHPP[0] == null");
            }

        } catch (HibernateException ex) {  //jangan catch NullPointerException
            if (tx != null) {
                flagIDTRMST = 0;
                tx.rollback();
                System.out.println("\n ##################### TRANSAKSI ROLLBACK DARI catch HibernateException");
            }
            throw new DataAccessResourceFailureException("Hibernate Session GAGAL. Scroll ke bawah untuk lihat penyebabnya.", ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return flagIDTRMST;
    }

    //untuk daftar surat jalan hari ini, tanpa pilih tanggal
    @Override
    public List<String> getSuratJalan() {
        Date Sdate = new Date();    //untuk tanggal hari ini
        System.out.println("\n======================== Vpubtrdet08DAOImpl: QUERY getSuratJalan(), cek daftar SJ per hari ini");
        return getSession().createSQLQuery("SELECT DISTINCT a.NOBUKTI as NOMORBUKTI_SJ_MT, a.TGL as TGL_SJ_MT, a.NOMOR AS NOMORTRMST_SJ_MT,"
                + " a.TRANSAKSI, b.KET1 as KET_OM, e.KETERANGAN as KET_SO"
                //+ " c.NOMOR AS NOMORTRDET"     //off NOMORTRDET supaya tidak berulang/bisa distinct
                //untuk d.LOKASI2 ga perlu ditampilkan. e.NAMA,e.ALAMAT,e.TELP ditaruh di detail
                + " FROM TRMST a"
                + " INNER JOIN TRMSTKET b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMOR = c.NOMORTRMST"
                + " INNER JOIN TRDETLOKASI2 d ON c.NOMOR = d.NOMOR" //untuk ambil lokasi2 ke klausa where
                + " INNER JOIN TRMSTPH e ON a.NOMOR = e.NOMOR"  //untuk keterangan SO yg master
                + " where (a.TRANSAKSI >= 5100 and a.TRANSAKSI < 6200)"
                + " and d.LOKASI2 = " + getInfoLokasi()     //eric -> + " and substring(a.TRANSAKSI from 4 for 1)?? yg lokasi 2 digit gmn?
                + " AND CAST(a.TGL As Date) = '" + sdf.format(Sdate) + "' ORDER BY a.NOMOR ASC")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    //untuk daftar surat jalan dengan pilih tanggal
    @Override
    public List<String> getTglSuratJalan(String tgl1, String tgl2) {
        System.out.println("\n======================== Vpubtrdet08DAOImpl: QUERY getTglSuratJalan(), cek daftar SJ dengan input tgl1 dan tgl2");
        System.out.println("tgl1(daftar SJ) =" + tgl1);
        System.out.println("tgl2(daftar SJ) =" + tgl2);
        return getSession().createSQLQuery("SELECT DISTINCT a.NOBUKTI as NOMORBUKTI_SJ_MT, a.TGL as TGL_SJ_MT, a.NOMOR AS NOMORTRMST_SJ_MT,"
                + " a.TRANSAKSI, a.TGLISI, b.KET1 as KET_OM, e.KETERANGAN as KET_SO"
                + " FROM TRMST a"
                + " INNER JOIN TRMSTKET b ON a.NOMOR = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMOR = c.NOMORTRMST"
                + " INNER JOIN TRDETLOKASI2 d ON c.NOMOR = d.NOMOR"
                + " INNER JOIN TRMSTPH e ON a.NOMOR = e.NOMOR"
                + " where (a.TRANSAKSI >= 5100 and a.TRANSAKSI < 6200)"
                + " and d.LOKASI2 = " + getInfoLokasi()   
                //eric, gak muncul tgl awalnya krn tdk di-cast as date -> + " AND a.TGL between :tglAwal and :tglAkhir"
                + " and cast(a.TGLISI as date) between cast((cast(:tglAwal as timestamp)) as date) and cast((cast(:tglAkhir as timestamp)) as date)"
                + " ORDER BY a.NOMOR ASC")
                .setParameter("tglAwal", tgl1)
                .setParameter("tglAkhir", tgl2)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    //Laporan pengiriman default
    @Override
    public List<String> getReport() {
        Date Rdate = new Date();
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("61" + getInfoLokasi()) : ("610" + getInfoLokasi());
        
        System.out.println("\n ======================== Vpubtrdet08DAOImpl: QUERY getReport(), laporan pengiriman per hari ini");
        return getSession().createSQLQuery("SELECT f.TGL2 as TGL_SJ, b.NOBUKTI as NOBUKTI_SJ, d.TGL AS TGL_SO, d.NOBUKTI as NOBUKTI_SO,"
                + " e.KODE AS KODESTOK_SJ, e.NAMA AS NAMASTOK_SJ,"
                + " a.TOTALORDER, a.TOTALKIRIMPARSIAL, a.QTYORDER as KIRIMPERSJ, a.SISAKIRIMPARSIAL as SISAKIRIM,"
                + " (CASE WHEN a.SISAKIRIMPARSIAL > 0 THEN 'Belum Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL = 0 THEN 'Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL < 0  THEN 'Lebih Kirim' END) as RESULT,"
                + " l.NAMA as TOKO_SO, coalesce(t2.KETERANGAN,'-') AS KET_SO, coalesce(t3.KET0,'-') as SUBKET_SO," //tambah toko, ket master dan detail so
                + " d.NOMOR as NOTRMST_SO, b.NOMOR as NOTRMST_SJ, c.NOMOR as NOTRDET_SO, a.NOMOR as NOTRDET_SJ, a.NOMORREF_ORDER"
                + " FROM VPUBTRDETSISAPARSIAL a"
                + " INNER JOIN TRMST b ON a.NOMORTRMST = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMORREF_ORDER = c.NOMOR"
                + " INNER JOIN TRMST d ON c.NOMORTRMST = d.NOMOR"
                + " INNER JOIN STOK e ON a.NOMORSTOK = e.NOMOR"
                + " INNER JOIN TRDETKETTGL f ON f.NOMOR = a.NOMOR"
                + " INNER JOIN TRDETLOKASI2 g ON g.NOMOR = a.NOMOR"
                + " INNER JOIN TRANSAKSILOKASI tl ON d.TRANSAKSI = tl.TRANSAKSI"    //tambah utk lokasi toko
                + " INNER JOIN LOKASI l ON tl.LOKASI = l.LOKASI"                    //tambah utk nama LOKASI toko
                + " LEFT JOIN TRMSTPH t2 ON d.NOMOR = t2.NOMOR"         //utk get keterangan SO yg master
                + " LEFT JOIN TRDETKET t3 ON c.NOMOR = t3.NOMOR"        //utk get keterangan SO yg detail
                + " WHERE b.TRANSAKSI = " + nomorTRANSAKSI  //+ " AND g.LOKASI2 = " + getInfoLokasi(), tdk perlu krn sdh di-filter b.TRANSAKSI
                + " AND CAST(f.TGL2 As Date) = '" + sdf.format(Rdate) + "' ORDER BY a.NOMOR ASC")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    //Laporan pengiriman dengan input tglawal dan tglakhir
    @Override
    public List<String> getTglReport(String tglawal, String tglakhir) {
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("61" + getInfoLokasi()) : ("610" + getInfoLokasi());
        
        System.out.println("\n ======================== Vpubtrdet08DAOImpl: QUERY getTglReport(), cek laporan pengiriman dengan input tglawal dan tglakhir");
        System.out.println("tglawal (Laporan Pengiriman) =" + tglawal);
        System.out.println("tglakhir (Laporan Pengiriman) =" + tglakhir);
        return getSession().createSQLQuery("SELECT f.TGL2 as TGL_SJ, b.NOBUKTI as NOBUKTI_SJ, d.TGL AS TGL_SO, d.NOBUKTI as NOBUKTI_SO,"
                + " e.KODE AS KODESTOK_SJ, e.NAMA AS NAMASTOK_SJ,"
                + " a.TOTALORDER, a.TOTALKIRIMPARSIAL, a.QTYORDER as KIRIMPERSJ, a.SISAKIRIMPARSIAL as SISAKIRIM,"
                + " (CASE WHEN a.SISAKIRIMPARSIAL > 0 THEN 'Belum Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL = 0 THEN 'Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL < 0  THEN 'Lebih Kirim' END) as RESULT,"
                + " l.NAMA as TOKO_SO, coalesce(t2.KETERANGAN,'-') AS KET_SO, coalesce(t3.KET0,'-') as SUBKET_SO,"
                + " d.NOMOR as NOTRMST_SO, b.NOMOR as NOTRMST_SJ, c.NOMOR as NOTRDET_SO, a.NOMOR as NOTRDET_SJ, a.NOMORREF_ORDER"
                + " FROM VPUBTRDETSISAPARSIAL a"
                + " INNER JOIN TRMST b ON a.NOMORTRMST = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMORREF_ORDER = c.NOMOR"
                + " INNER JOIN TRMST d ON c.NOMORTRMST = d.NOMOR"
                + " INNER JOIN STOK e ON a.NOMORSTOK = e.NOMOR"
                + " INNER JOIN TRDETKETTGL f ON f.NOMOR = a.NOMOR"
                + " INNER JOIN TRDETLOKASI2 g ON g.NOMOR = a.NOMOR"
                + " INNER JOIN TRANSAKSILOKASI tl ON d.TRANSAKSI = tl.TRANSAKSI"    //tambah utk lokasi toko
                + " INNER JOIN LOKASI l ON tl.LOKASI = l.LOKASI"                    //tambah utk nama LOKASI toko
                + " LEFT JOIN TRMSTPH t2 ON d.NOMOR = t2.NOMOR"         //utk get keterangan SO yg master
                + " LEFT JOIN TRDETKET t3 ON c.NOMOR = t3.NOMOR"        //utk get keterangan SO yg detail
                + " WHERE b.TRANSAKSI = " + nomorTRANSAKSI
                //eric -> + " AND e.TGL2 between :tglAwal and :tglAkhir"
                + " and cast(f.TGL2 as date) between cast((cast(:tglAwal as timestamp)) as date) and cast((cast(:tglAkhir as timestamp)) as date)"
                + " ORDER BY a.NOMOR ASC")
                .setParameter("tglAwal", tglawal)
                .setParameter("tglAkhir", tglakhir)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
    
    //Laporan pengiriman mutasi (MT) default
    @Override
    public List<String> getLapMutasi() {
        Date Rdate = new Date();
        
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("51" + getInfoLokasi()) : ("510" + getInfoLokasi());
        
        System.out.println("\n ======================== Vpubtrdet08DAOImpl: QUERY getLapMutasi(), laporan mutasi per hari ini");
        return getSession().createSQLQuery("SELECT f.TGL2 as TGL_MT, b.NOBUKTI as NOBUKTI_MT, f.TGL1 AS TGL_OM, d.NOBUKTI as NOBUKTI_OM,"
                + " e.KODE AS KODESTOK_MT, e.NAMA AS NAMASTOK_MT,"
                + " a.TOTALORDER, a.TOTALKIRIMPARSIAL, a.QTYORDER as KIRIMPERMT, a.SISAKIRIMPARSIAL as SISAKIRIM,"
                + " (CASE WHEN a.SISAKIRIMPARSIAL > 0 THEN 'Belum Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL = 0 THEN 'Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL < 0  THEN 'Lebih Kirim' END) as RESULT,"
                + " l.NAMA as TOKO_OM, coalesce(trmket.KET1,'-') AS KET_OM, coalesce(trdket.KET0,'-') AS SUBKET_OM," //toko, ket master & detail
                + " d.NOMOR as NOTRMST_OM, b.NOMOR as NOTRMST_MT, c.NOMOR as NOTRDET_OM, a.NOMOR as NOTRDET_MT, a.NOMORREF_ORDER"
                + " FROM VPUBTRDETSISAPARSIAL a"
                + " INNER JOIN TRMST b ON a.NOMORTRMST = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMORREF_ORDER = c.NOMOR"
                + " INNER JOIN TRMST d ON c.NOMORTRMST = d.NOMOR"
                + " INNER JOIN STOK e ON a.NOMORSTOK = e.NOMOR"
                + " INNER JOIN TRDETKETTGL f ON f.NOMOR = a.NOMOR"
                + " INNER JOIN TRDETLOKASI2 g ON g.NOMOR = a.NOMOR"
                + " INNER JOIN TRANSAKSILOKASI tl ON d.TRANSAKSI = tl.TRANSAKSI"    //tambah utk lokasi toko
                + " INNER JOIN LOKASI l ON tl.LOKASI = l.LOKASI"        //tambah utk nama LOKASI toko
                + " INNER JOIN TRMSTKET trmket ON d.NOMOR = trmket.NOMOR"   //get keterangan master OM
                + " LEFT JOIN TRDETKET trdket ON c.NOMOR = trdket.NOMOR"    //get keterangan detail OM
                + " WHERE b.TRANSAKSI = " + nomorTRANSAKSI
                + " AND CAST(f.TGL2 As Date) = '" + sdf.format(Rdate) + "' ORDER BY a.NOMOR ASC")
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    //Laporan pengiriman mutasi (MT) dengan input tglawal dan tglakhir
    @Override
    public List<String> getTglLapMutasi(String tglawal, String tglakhir) {
        //buat flag untuk trik nomorTransaksi yang lokasi 1 digit atau 2 digit
        int flagInfoLokasi = Integer.parseInt(getInfoLokasi());
        String nomorTRANSAKSI = (flagInfoLokasi > 10) ? ("51" + getInfoLokasi()) : ("510" + getInfoLokasi());
        
        System.out.println("\n ======================== Vpubtrdet08DAOImpl: QUERY getTglLapMutasi(), cek laporan mutasi dengan input tglawal dan tglakhir");
        System.out.println("tglawal (Laporan Pengiriman) =" + tglawal);
        System.out.println("tglakhir (Laporan Pengiriman) =" + tglakhir);
        return getSession().createSQLQuery("SELECT f.TGL2 as TGL_MT, b.NOBUKTI as NOBUKTI_MT, f.TGL1 AS TGL_OM, d.NOBUKTI as NOBUKTI_OM,"
                + " e.KODE AS KODESTOK_MT, e.NAMA AS NAMASTOK_MT,"
                + " a.TOTALORDER, a.TOTALKIRIMPARSIAL, a.QTYORDER as KIRIMPERMT, a.SISAKIRIMPARSIAL as SISAKIRIM,"
                + " (CASE WHEN a.SISAKIRIMPARSIAL > 0 THEN 'Belum Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL = 0 THEN 'Lengkap'"
                + " WHEN a.SISAKIRIMPARSIAL < 0  THEN 'Lebih Kirim' END) as RESULT,"
                + " l.NAMA as TOKO_OM, coalesce(trmket.KET1,'-') AS KET_OM, coalesce(trdket.KET0,'-') AS SUBKET_OM,"
                + " d.NOMOR as NOTRMST_OM, b.NOMOR as NOTRMST_MT, c.NOMOR as NOTRDET_OM, a.NOMOR as NOTRDET_MT, a.NOMORREF_ORDER"
                + " FROM VPUBTRDETSISAPARSIAL a"
                + " INNER JOIN TRMST b ON a.NOMORTRMST = b.NOMOR"
                + " INNER JOIN TRDET c ON a.NOMORREF_ORDER = c.NOMOR"
                + " INNER JOIN TRMST d ON c.NOMORTRMST = d.NOMOR"
                + " INNER JOIN STOK e ON a.NOMORSTOK = e.NOMOR"
                + " INNER JOIN TRDETKETTGL f ON f.NOMOR = a.NOMOR"
                + " INNER JOIN TRDETLOKASI2 g ON g.NOMOR = a.NOMOR"
                + " INNER JOIN TRANSAKSILOKASI tl ON d.TRANSAKSI = tl.TRANSAKSI"
                + " INNER JOIN LOKASI l ON tl.LOKASI = l.LOKASI"
                + " INNER JOIN TRMSTKET trmket ON d.NOMOR = trmket.NOMOR"
                + " LEFT JOIN TRDETKET trdket ON c.NOMOR = trdket.NOMOR"
                + " WHERE b.TRANSAKSI = " + nomorTRANSAKSI
                //eric -> + " AND e.TGL2 between :tglAwal and :tglAkhir"
                + " and cast(f.TGL2 as date) between cast((cast(:tglAwal as timestamp)) as date) and cast((cast(:tglAkhir as timestamp)) as date)"
                + " ORDER BY a.NOMOR ASC")
                .setParameter("tglAwal", tglawal)
                .setParameter("tglAkhir", tglakhir)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }

    //awal mau cetak SJ PDF, query ini jg ada di SJpenjualan.jrxml tapi disana tanpa klausa where. INI UNTUK MAIN REPORT SJ PENJUALAN (BAGIAN ATAS)
    //FAF, nambah field nama driver & no_plat
    @Override
    public JRDataSource getDataSJpenjualan(Integer noTRMSTSJ) {
        //add local variable
        final Query dataPengiriman;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataSJpenjualan(Integer noTRMSTSJ), INI UNTUK MAIN REPORT SJ PENJUALAN");
        dataPengiriman = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT,"
                + " coalesce(a.TELP,'-') AS TELP, a.LOGIN, a.KETERANGAN, b.NAMA AS LOKASI, c.NAMA AS DRIVER, c.PLAT AS NO_PLAT"
                + " FROM VPUBTRMST03 a"
                + " INNER JOIN LOKASI b ON a.LOKASI = b.LOKASI"
                + " INNER JOIN SUPIR c ON a.NOMOR = c.NOMOR"
                + " WHERE a.NOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMSTSJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = dataPengiriman.list();
        
        //menambahkan NOMOR_LAMA (NOMORPROGLAMA) ke dalam List<String> result, getsession yang dibuat di ModelAndView tambahBarang (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("NOMOR_LAMA", session.getAttribute("NOMOR_LAMA"));
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //query ini jg ada di SJpenjualan_subreport.jrxml. 4 inner join, TRMST a utk master SJ, TRMST e untuk detail SO. INI UNTUK SUBREPORT SJ (BAGIAN TENGAH)
    @Override
    public JRDataSource getDataSJpenjualan_sub(Integer noTRMSTSJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataSJpenjualan_sub(Integer noTRMSTSJ), INI UNTUK SUBREPORT SJ");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, b.SATUAN, e.NOBUKTI,"
                + " coalesce(f.KET0,'-') as KET0"   //alias harus sama dgn field di jasper
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " LEFT JOIN TRDETKET f ON d.NOMOR = f.NOMOR"
                + " WHERE a.NOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMSTSJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //FAF, method re-get data untuk REPRINT SJ
    @Override
    public JRDataSource regetDataSJpenjualan(Integer noTRMSTSJ) {
        //add local variable
        final Query dataPengiriman;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataSJpenjualan(Integer noTRMSTSJ), INI UNTUK MAIN REPORT SJ PENJUALAN");
        dataPengiriman = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT,"
                + " coalesce(a.TELP,'-') AS TELP, a.LOGIN, a.KETERANGAN, b.NAMA AS LOKASI, c.NAMA AS DRIVER, c.PLAT AS NO_PLAT"
                + " FROM VPUBTRMST03 a"
                + " LEFT JOIN LOKASI b ON a.LOKASI = b.LOKASI"
                + " LEFT JOIN SUPIR c ON a.NOMOR = c.NOMOR"
                + " WHERE a.NOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMSTSJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = dataPengiriman.list();
        
        //menambahkan NOMOR_LAMA (NOMORPROGLAMA) ke dalam List<String> result, getsession yang dibuat di ModelAndView tambahBarang (ManageController)
//        Iterator iter = result.iterator();
//        Map map = (Map) iter.next();
//        map.put("NOMOR_LAMA", session.getAttribute("NOMOR_LAMA"));
        
        System.out.println("Data Report = " + result);

        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //query ini jg ada di SJpenjualan_subreport.jrxml. 4 inner join, TRMST a utk master SJ, TRMST e untuk detail SO. INI UNTUK SUBREPORT SJ (BAGIAN TENGAH)
    @Override
    public JRDataSource regetDataSJpenjualan_sub(Integer noTRMSTSJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataSJpenjualan_sub(Integer noTRMSTSJ), INI UNTUK SUBREPORT SJ");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, b.SATUAN, e.NOBUKTI,"
                + " coalesce(f.KET0,'-') as KET0"   //alias harus sama dgn field di jasper
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " LEFT JOIN TRDETKET f ON d.NOMOR = f.NOMOR"
                + " WHERE a.NOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMSTSJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //awal mau cetak InvoiceSJ PDF, query ini jg ada di InvoiceSJ.jrxml tapi disana tanpa klausa where. INI UNTUK MAIN REPORT INVOICE SJ (BAGIAN ATAS)
    @Override
    public JRDataSource getDataInvoiceSJ(Integer noTRMSTSJ) {
        //add local variable
        final Query dataInvoiceSJ;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataInvoiceSJ(Integer noTRMSTSJ), INI UNTUK MAIN REPORT");
        
        dataInvoiceSJ = getSession().createSQLQuery("SELECT DISTINCT v1.TGL, v1.NOBUKTI, v1.PHNAMA as NAMA, v1.ALAMAT, v1.TELP," +
            " v1.TRMSTPHKET as KETERANGAN, v1.LOKASI2 AS LOKASI, SUM(v2.TOTALNETTO) AS GRANDTOTAL" +
            " FROM VSETELAHORDER01 v1" +
            " LEFT JOIN VRev01_TrDet15 v2 ON v2.NOMOR = v1.TRDETNOMOR" +
            " WHERE v1.TRMSTNOMOR = :cekBukti" +
            " GROUP BY v1.TGL, v1.NOBUKTI, v1.PHNAMA, v1.ALAMAT, v1.TELP, v1.TRMSTPHKET, v1.LOKASI2")
            .setParameter("cekBukti", noTRMSTSJ)
            .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = dataInvoiceSJ.list();
        
        //menambahkan NOMOR_LAMA (NOMORPROGLAMA) dan SPM ke List<String> result, getsession yang dibuat di ModelAndView tambahBarang (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("NOMOR_LAMA", session.getAttribute("NOMOR_LAMA"));
        map.put("SPM", session.getAttribute("SPM"));
        
        //ambil grandtotal untuk dibuat terbilang
        Double getGRANDTOTAL = (Double) map.get("GRANDTOTAL");
        
        //menambahkan terbilang menggunakan class HSoftTerbilang
        HSoftTerbilang terbilang = new HSoftTerbilang();
        terbilang.setNilai(getGRANDTOTAL.intValue());
        map.put("TERBILANG", terbilang.getHasil());
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //query ini jg ada di InvoiceSJ_subreport.jrxml. 4 inner join, TRMST a utk master SJ, TRMST e untuk detail SO. INI UNTUK SUBREPORT INVOICE SJ (BAGIAN TENGAH)
    @Override
    public JRDataSource getDataInvoiceSJ_sub(Integer noTRMSTSJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataInvoiceSJ_sub(Integer noTRMSTSJ), INI UNTUK SUBREPORT INVOICE");
        query = getSession().createSQLQuery("SELECT v1.KODESTOK, v1.NAMASTOK, v1.TRDETBANYAKNYA as BANYAKNYA, v2.SATUAN, v2.HARGA, v2.TOTALDISC," +
                " v2.TOTALNETTO as TOTAL, v3.TOTALNETTO as GRANDTOTAL" +
                " FROM VSETELAHORDER01 v1" +
                " LEFT JOIN VRev01_TrDet15 v2 ON v2.NOMOR = v1.TRDETNOMOR" +
                " LEFT JOIN VPRVTRMST11 v3 ON v3.NOMOR = v1.TRMSTNOMOR" +
                " WHERE v1.TRMSTNOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMSTSJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //untuk jasper report mutasi. INI UNTUK HEADING SURAT MUTASI (BAGIAN ATAS)
    @Override
    public JRDataSource getDataMutasi(Integer kodeMutasi) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataMutasi(Integer kodeMutasi), INI UNTUK HEADING SURAT JALAN MUTASI");
        query = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.KETKET2 AS MEMO, a.LOGIN, l.NAMA AS LOKASI"
                //+ " a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT, coalesce(a.TELP,'-') AS TELP,"  //ganti dengan memo
                + " FROM VPUBTRMST03 a"
                + " JOIN TRANSAKSILOKASI tl on a.TRANSAKSI = tl.TRANSAKSI"
                + " JOIN LOKASI l ON tl.LOKASI = l.LOKASI"
                + " WHERE a.NOMOR = :kodeMutasi")     //kodeMutasi = NO TRDET MT
                .setParameter("kodeMutasi", kodeMutasi)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = query.list();
        
        //menambahkan Toko OM ke dalam List<String> result, ambil session yang dibuat di ModelAndView prosesMutasi (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("CPM_TOKO_OM", session.getAttribute("CPM_TOKO_OM"));
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //untuk jasper report mutasi. INI UNTUK BODY SURAT MUTASI (BAGIAN TENGAH) 
    @Override
    public JRDataSource getDataMutasi_sub(Integer kodeMutasi) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataMutasi_sub(Integer kodeMutasi), INI UNTUK BODY SURAT JALAN MUTASI");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, e.NOBUKTI, b.SATUAN, t1.KET1, t2.KET0"
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " INNER JOIN TRMSTKET t1 ON e.NOMOR = t1.NOMOR"
                + " INNER JOIN TRDETKET t2 ON b.NOMOR = t2.NOMOR"
                + " WHERE a.NOMOR = :kodeMutasi")
                .setParameter("kodeMutasi", kodeMutasi)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //untuk jasper report terima retur jual. INI UNTUK HEADING SURAT RETUR JUAL (BAGIAN ATAS)
    @Override
    public JRDataSource getDataTRJ(Integer kodeTRJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataTRJ(Integer kodeTRJ), INI UNTUK HEADING SURAT TERIMA RETUR JUAL");
        query = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.KETKET2 AS MEMO, a.LOGIN, d.NAMA AS LOKASI"
                //+ " a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT, coalesce(a.TELP,'-') AS TELP,"  //ganti dengan memo
                + " FROM VPUBTRMST03 a"
                + " INNER JOIN TRDET b ON a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETLOKASI2 c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN LOKASI d ON c.LOKASI2 = d.LOKASI"
                + " WHERE a.NOMOR = :kodeTRJ")     //kodeTRJ = NO TRMST TRJ
                .setParameter("kodeTRJ", kodeTRJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = query.list();
        
        //menambahkan Toko TRJ ke dalam List<String> result, ambil session yang dibuat di ModelAndView prosesTRJ (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("SESS_TOKO_TRJ", session.getAttribute("SESS_TOKO_TRJ"));
        
        System.out.println("Session Attribute (\"SESS_TOKO_TRJ\"): " + session.getAttribute("SESS_TOKO_TRJ"));
        System.out.println("nilai map.get(\"SESS_TOKO_TRJ\"): " + map.get("SESS_TOKO_TRJ"));
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //untuk jasper report terima retur jual. INI UNTUK BODY SURAT RETUR JUAL (BAGIAN TENGAH) 
    @Override
    public JRDataSource getDataTRJ_sub(Integer kodeTRJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataTRJ_sub(Integer kodeTRJ), INI UNTUK BODY SURAT TERIMA RETUR JUAL");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, e.NOBUKTI, b.SATUAN, t1.KET1"
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " INNER JOIN TRMSTKET t1 ON e.NOMOR = t1.NOMOR"
                + " WHERE a.NOMOR = :kodeTRJ")
                .setParameter("kodeTRJ", kodeTRJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //awal mau cetak InvoiceTRJ PDF, query ini jg ada di InvoiceTRJ.jrxml tapi disana tanpa klausa where. INI UNTUK MAIN REPORT INVOICE TRJ (BAGIAN ATAS)
    @Override
    public JRDataSource getDataInvoiceTRJ(Integer noTRMST_TRJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataInvoiceTRJ(Integer noTRMST_TRJ), INI UNTUK MAIN REPORT");
        query = getSession().createSQLQuery("SELECT DISTINCT v1.TGL, v1.NOBUKTI, v1.TRMSTKETKET2 AS MEMO, v1.LOGIN, v1.LOKASI2 AS LOKASI," +
                " SUM(v2.TOTALNETTO) AS GRANDTOTAL" +   //GRANDTOTAL untuk dibuatkan terbilang-nya
                " FROM VSETELAHORDER01 v1" +
                " LEFT JOIN VRev01_TrDet15 v2 ON v2.NOMOR = v1.TRDETNOMOR" +
                " WHERE v1.TRMSTNOMOR = :cekBukti" +
                " GROUP BY v1.TGL, v1.NOBUKTI, v1.TRMSTKETKET2, v1.LOGIN, v1.LOKASI2")
                .setParameter("cekBukti", noTRMST_TRJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = query.list();
        
        //menambahkan Toko TRJ ke dalam List<String> result, ambil session yang dibuat di ModelAndView prosesTRJ (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("SESS_TOKO_TRJ", session.getAttribute("SESS_TOKO_TRJ"));
        
        //ambil grandtotal untuk dibuat terbilang
        Double getGRANDTOTAL = (Double) map.get("GRANDTOTAL");
        
        //menambahkan terbilang menggunakan class HSoftTerbilang
        HSoftTerbilang terbilang = new HSoftTerbilang();
        terbilang.setNilai(getGRANDTOTAL.intValue());
        map.put("TERBILANG", terbilang.getHasil());
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }
    
    //query ini jg ada di InvoiceTRJ_subreport.jrxml. INI UNTUK SUBREPORT INVOICE TRJ (BAGIAN TENGAH)
    @Override
    public JRDataSource getDataInvoiceTRJ_sub(Integer noTRMST_TRJ) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataInvoiceTRJ_sub(Integer noTRMST_TRJ), INI UNTUK SUBREPORT INVOICE TRJ");
        query = getSession().createSQLQuery("SELECT v1.KODESTOK, v1.NAMASTOK, v1.TRDETBANYAKNYA AS BANYAKNYA" +
                ", v2.SATUAN, v2.HARGA, v2.TOTALDISC, v2.TOTALNETTO AS TOTAL" +
                ", v3.TOTALNETTO AS GRANDTOTAL" +
                " FROM VSETELAHORDER01 v1" +
                " LEFT JOIN VRev01_TrDet15 v2 ON v2.NOMOR = v1.TRDETNOMOR" +
                " LEFT JOIN VPRVTRMST11 v3 ON v3.NOMOR = v1.TRMSTNOMOR" +
                " WHERE v1.TRMSTNOMOR = :cekBukti")
                .setParameter("cekBukti", noTRMST_TRJ)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //untuk jasper report terima retur beli. INI UNTUK HEADING SURAT RETUR BELI (BAGIAN ATAS)
    @Override
    public JRDataSource getDataTRB(Integer kodeTRB) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataTRB(Integer kodeTRB), INI UNTUK HEADING SURAT TERIMA RETUR BELI");
        query = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.KETKET2 AS MEMO, a.LOGIN, d.NAMA AS LOKASI,"
                //+ " a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT, coalesce(a.TELP,'-') AS TELP,"  //ganti dengan memo
                + " ph.NAMA AS SUPPLIER_ORB"    //nama supplier
                + " FROM VPUBTRMST03 a"
                + " INNER JOIN TRDET b ON a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETLOKASI2 c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN LOKASI d ON c.LOKASI2 = d.LOKASI"
                + " INNER JOIN PH ph ON a.NOMORPH = ph.NOMOR"
                + " WHERE a.NOMOR = :kodeTRB")     //kodeTRB = NO TRMST TRB
                .setParameter("kodeTRB", kodeTRB)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = query.list();
        
        //menambahkan Toko TRB ke dalam List<String> result, ambil session yang dibuat di ModelAndView prosesTRB (ManageController)
        Iterator iter = result.iterator();
        Map map = (Map) iter.next();
        map.put("SESS_TOKO_TRB", session.getAttribute("SESS_TOKO_TRB"));
        
        System.out.println("Session Attribute (\"SESS_TOKO_TRB\"): " + session.getAttribute("SESS_TOKO_TRB"));
        System.out.println("nilai map.get(\"SESS_TOKO_TRB\"): " + map.get("SESS_TOKO_TRB"));
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }

    //untuk jasper report terima retur beli. INI UNTUK BODY SURAT RETUR BELI (BAGIAN TENGAH) 
    @Override
    public JRDataSource getDataTRB_sub(Integer kodeTRB) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataTRB_sub(Integer kodeTRB), INI UNTUK BODY SURAT TERIMA RETUR BELI");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, e.NOBUKTI, b.SATUAN, t1.KET1, t2.KET0"
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " INNER JOIN TRMSTKET t1 ON e.NOMOR = t1.NOMOR"
                + " INNER JOIN TRDETKET t2 ON b.NOMOR = t2.NOMOR"
                + " WHERE a.NOMOR = :kodeTRB")
                .setParameter("kodeTRB", kodeTRB)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
    //untuk jasper report form LPB. INI UNTUK HEADING SURAT FORM LPB (BAGIAN ATAS)
    @Override
    public JRDataSource getDataLPB(Integer kodeLPB) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataLPB(Integer kodeLPB), INI UNTUK HEADING SURAT FORM LPB");
        query = getSession().createSQLQuery("SELECT DISTINCT a.TGL, a.NOBUKTI, a.NAMA, coalesce(a.ALAMAT,'-') AS ALAMAT,"
                + " coalesce(a.TELP,'-') AS TELP, a.LOGIN, a.KETKET1 as KETERANGAN, a.KETKET2 AS MEMO, b.NAMA AS LOKASI"
                + " FROM VPUBTRMST03 a"
                + " INNER JOIN LOKASI b ON a.LOKASI = b.LOKASI"
                + " WHERE a.NOMOR = :kodeLPB")
                .setParameter("kodeLPB", kodeLPB)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result = query.list();
        
        //menambahkan Toko LPB ke dalam List<String> result, ambil session yang dibuat di ModelAndView prosesLPB (ManageController)
//        Iterator iter = result.iterator();
//        Map map = (Map) iter.next();
//        map.put("SESS_TOKO_LPB", session.getAttribute("SESS_TOKO_LPB"));
        
//        System.out.println("Session Attribute (\"SESS_TOKO_LPB\"): " + session.getAttribute("SESS_TOKO_LPB"));
//        System.out.println("nilai map.get(\"SESS_TOKO_LPB\"): " + map.get("SESS_TOKO_LPB"));
        
        JRDataSource ds = new JRBeanCollectionDataSource(result);
        return ds;
    }
    
    //untuk jasper report form LPB. INI UNTUK BODY SURAT FORM LPB (BAGIAN TENGAH) 
    @Override
    public JRDataSource getDataLPB_sub(Integer kodeLPB) {
        //add local variable
        final Query query;
        System.out.println("\n\n ============ JASPERREPORT ============ Vpubtrdet08DAOImpl: QUERY getDataLPB_sub(Integer kodeLPB), INI UNTUK BODY SURAT FORM LPB");
        query = getSession().createSQLQuery("SELECT b.KODESTOK, b.NAMASTOK, b.BANYAKNYA, b.SATUAN, e.NOBUKTI,"
                + " coalesce(f.KET0,'-') as KET0"   //alias harus sama dgn field di jasper
                + " FROM TRMST a"
                + " INNER JOIN VPUBTRDET08 b on a.NOMOR = b.NOMORTRMST"
                + " INNER JOIN TRDETNOREF c ON b.NOMOR = c.NOMOR"
                + " INNER JOIN TRDET d ON d.NOMOR = c.NOMORREF"
                + " INNER JOIN TRMST e ON d.NOMORTRMST = e.NOMOR"
                + " LEFT JOIN TRDETKET f ON d.NOMOR = f.NOMOR"
                + " WHERE a.NOMOR = :kodeLPB")
                .setParameter("kodeLPB", kodeLPB)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<String> result01 = query.list();
        JRDataSource ds01 = new JRBeanCollectionDataSource(result01);
        return ds01;
    }
    
}
