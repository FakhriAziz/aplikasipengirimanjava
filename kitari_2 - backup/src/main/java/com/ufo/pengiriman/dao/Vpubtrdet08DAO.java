package com.ufo.pengiriman.dao;

import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author PROGRAMER
 */
public interface Vpubtrdet08DAO {
    
    //FAF, tambah parameter
    public int addSOkirim(int jmlDataTambahBarangSO, String[] dataQtyKirimSO, String[] dataTglKirimSO, String[] dataNomorBuktiSO,
        String[] dataNomorTRMSTSO, String[] dataNomorTRDETSO, String dataNamaCustomerSO, String dataAlamatCustomerSO, String dataTelpCustomerSO, String dataNAMA_DRIVER, String dataNO_PLAT);
    
    public int addKirimMutasi(int dataProsesMutasi, String[] dataNomorBuktiOM, String[] dataNomorTRMSTOM, String[] dataNomorTRDETOM,
        String[] dataQtyKirimOM, String[] dataTglOM, String[] dataMemoMutasi);
    
    public int addTerimaRJ(int dataProsesTRJ, String[] dataNomorBuktiTRJ, String[] dataNomorTRMSTTRJ, String[] dataNomorTRDETTRJ,
        String[] dataQtyTRJ, String[] dataTglTRJ, String[] dataMemoTRJ, String[] dataNamaTRMSTPHORJ, String[] dataAlamatTRMSTPHORJ,
        String[] dataTelpTRMSTPHORJ);
    
    public int addTerimaRB(int dataProsesTRB, String[] dataNomorBuktiTRB, String[] dataNomorTRMSTTRB, String[] dataNomorTRDETTRB,
        String[] dataQtyTRB, String[] dataTglTRB, String[] dataMemoTRB, String[] dataSupplierTRB, String[] dataAlamatTRMSTPHORB,
        String[] dataTelpTRMSTPHORB);
    
    public int addTerimaLPB(int dataProsesLPB, String[] dataNomorBuktiLPB, String dataNamaSupplierLPB, String dataAlamatSupplierLPB, 
            String dataTelpSupplierLPB, String[] dataNomorTRMSTLPB, String[] dataNomorTRDETLPB, String[] dataQtyLPB, String[] dataTglLPB,
            String[] dataMemoLPB);

    public List<String> getSuratJalan();
    public List<String> getTglSuratJalan(String tgl1, String tgl2);

    public List<String> getReport();
    public List<String> getTglReport(String tglawal, String tglakhir);
    
    public List<String> getLapMutasi();
    public List<String> getTglLapMutasi(String tglawal, String tglakhir);
    
    //untuk jasper report SJ penjualan
    public JRDataSource getDataSJpenjualan(Integer noTRMSTSJ);
    public JRDataSource getDataSJpenjualan_sub(Integer noTRMSTSJ);
    
    //untuk jasper report SJ penjualan
    public JRDataSource regetDataSJpenjualan(Integer noTRMSTSJ);
    public JRDataSource regetDataSJpenjualan_sub(Integer noTRMSTSJ);
    
    //untuk jasper report invoice SJ
    public JRDataSource getDataInvoiceSJ(Integer noTRMSTSJ);
    public JRDataSource getDataInvoiceSJ_sub(Integer noTRMSTSJ);
    
    //untuk jasper report mutasi 
    public JRDataSource getDataMutasi(Integer kodeMutasi);
    public JRDataSource getDataMutasi_sub(Integer kodeMutasi);
    
    //untuk jasper report terima retur jual 
    public JRDataSource getDataTRJ(Integer kodeTRJ);
    public JRDataSource getDataTRJ_sub(Integer kodeTRJ);
    
    //untuk jasper report invoice SJ
    public JRDataSource getDataInvoiceTRJ(Integer noTRMST_TRJ);
    public JRDataSource getDataInvoiceTRJ_sub(Integer noTRMST_TRJ);
    
    //untuk jasper report terima retur beli 
    public JRDataSource getDataTRB(Integer kodeTRB);
    public JRDataSource getDataTRB_sub(Integer kodeTRB);
    
    //untuk jasper report form LPB 
    public JRDataSource getDataLPB(Integer kodeLPB);
    public JRDataSource getDataLPB_sub(Integer kodeLPB);
}
