/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VPUBTRMST03")
@NamedQueries({
    @NamedQuery(name = "Vpubtrmst03.findAll", query = "SELECT v FROM Vpubtrmst03 v"),
    @NamedQuery(name = "Vpubtrmst03.findByNomor", query = "SELECT v FROM Vpubtrmst03 v WHERE v.nomor = :nomor"),
    @NamedQuery(name = "Vpubtrmst03.findByTransaksi", query = "SELECT v FROM Vpubtrmst03 v WHERE v.transaksi = :transaksi"),
    @NamedQuery(name = "Vpubtrmst03.findByNobukti", query = "SELECT v FROM Vpubtrmst03 v WHERE v.nobukti = :nobukti"),
    @NamedQuery(name = "Vpubtrmst03.findByTgl", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tgl = :tgl"),
    @NamedQuery(name = "Vpubtrmst03.findByTglisi", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tglisi = :tglisi"),
    @NamedQuery(name = "Vpubtrmst03.findByTglposting", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tglposting = :tglposting"),
    @NamedQuery(name = "Vpubtrmst03.findByLogin", query = "SELECT v FROM Vpubtrmst03 v WHERE v.login = :login"),
    @NamedQuery(name = "Vpubtrmst03.findByNomorph", query = "SELECT v FROM Vpubtrmst03 v WHERE v.nomorph = :nomorph"),
    @NamedQuery(name = "Vpubtrmst03.findByDiscpersen0", query = "SELECT v FROM Vpubtrmst03 v WHERE v.discpersen0 = :discpersen0"),
    @NamedQuery(name = "Vpubtrmst03.findByDiscrp0", query = "SELECT v FROM Vpubtrmst03 v WHERE v.discrp0 = :discrp0"),
    @NamedQuery(name = "Vpubtrmst03.findByDiscpersen1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.discpersen1 = :discpersen1"),
    @NamedQuery(name = "Vpubtrmst03.findByDiscrp1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.discrp1 = :discrp1"),
    @NamedQuery(name = "Vpubtrmst03.findByPpnpersen", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ppnpersen = :ppnpersen"),
    @NamedQuery(name = "Vpubtrmst03.findByPpnrp", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ppnrp = :ppnrp"),
    @NamedQuery(name = "Vpubtrmst03.findByBiayarp0", query = "SELECT v FROM Vpubtrmst03 v WHERE v.biayarp0 = :biayarp0"),
    @NamedQuery(name = "Vpubtrmst03.findByBiayarp1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.biayarp1 = :biayarp1"),
    @NamedQuery(name = "Vpubtrmst03.findByTotalhpp", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totalhpp = :totalhpp"),
    @NamedQuery(name = "Vpubtrmst03.findByTotalbruto", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totalbruto = :totalbruto"),
    @NamedQuery(name = "Vpubtrmst03.findByTotaldisc0", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totaldisc0 = :totaldisc0"),
    @NamedQuery(name = "Vpubtrmst03.findByTotal0", query = "SELECT v FROM Vpubtrmst03 v WHERE v.total0 = :total0"),
    @NamedQuery(name = "Vpubtrmst03.findByTotaldisc1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totaldisc1 = :totaldisc1"),
    @NamedQuery(name = "Vpubtrmst03.findByTotal1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.total1 = :total1"),
    @NamedQuery(name = "Vpubtrmst03.findByTotaldisc", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totaldisc = :totaldisc"),
    @NamedQuery(name = "Vpubtrmst03.findByTotalppn", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totalppn = :totalppn"),
    @NamedQuery(name = "Vpubtrmst03.findByTotal2", query = "SELECT v FROM Vpubtrmst03 v WHERE v.total2 = :total2"),
    @NamedQuery(name = "Vpubtrmst03.findByTotalnetto", query = "SELECT v FROM Vpubtrmst03 v WHERE v.totalnetto = :totalnetto"),
    @NamedQuery(name = "Vpubtrmst03.findByNama", query = "SELECT v FROM Vpubtrmst03 v WHERE v.nama = :nama"),
    @NamedQuery(name = "Vpubtrmst03.findByAlamat", query = "SELECT v FROM Vpubtrmst03 v WHERE v.alamat = :alamat"),
    @NamedQuery(name = "Vpubtrmst03.findByTelp", query = "SELECT v FROM Vpubtrmst03 v WHERE v.telp = :telp"),
    @NamedQuery(name = "Vpubtrmst03.findByKeterangan", query = "SELECT v FROM Vpubtrmst03 v WHERE v.keterangan = :keterangan"),
    @NamedQuery(name = "Vpubtrmst03.findByTglkirim", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tglkirim = :tglkirim"),
    @NamedQuery(name = "Vpubtrmst03.findByTgljatuhtempo", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tgljatuhtempo = :tgljatuhtempo"),
    @NamedQuery(name = "Vpubtrmst03.findByTglinvoice", query = "SELECT v FROM Vpubtrmst03 v WHERE v.tglinvoice = :tglinvoice"),
    @NamedQuery(name = "Vpubtrmst03.findByKetnosuratjalan", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ketnosuratjalan = :ketnosuratjalan"),
    @NamedQuery(name = "Vpubtrmst03.findByKetnobukti", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ketnobukti = :ketnobukti"),
    @NamedQuery(name = "Vpubtrmst03.findByKetket1", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ketket1 = :ketket1"),
    @NamedQuery(name = "Vpubtrmst03.findByKetket2", query = "SELECT v FROM Vpubtrmst03 v WHERE v.ketket2 = :ketket2"),
    @NamedQuery(name = "Vpubtrmst03.findByLokasi", query = "SELECT v FROM Vpubtrmst03 v WHERE v.lokasi = :lokasi")})
public class Vpubtrmst03 implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    @Id
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSI")
    private short transaksi;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOBUKTI")
    private String nobukti;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLPOSTING")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglposting;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMORPH")
    private Integer nomorph;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DISCPERSEN0")
    private Double discpersen0;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DISCRP0")
    private Double discrp0;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DISCPERSEN1")
    private Double discpersen1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DISCRP1")
    private Double discrp1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "PPNPERSEN")
    private Double ppnpersen;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "PPNRP")
    private Double ppnrp;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "BIAYARP0")
    private Double biayarp0;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "BIAYARP1")
    private Double biayarp1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALHPP")
    private Double totalhpp;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALBRUTO")
    private Double totalbruto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALDISC0")
    private Double totaldisc0;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTAL0")
    private Double total0;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALDISC1")
    private Double totaldisc1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTAL1")
    private Double total1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALDISC")
    private Double totaldisc;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALPPN")
    private Double totalppn;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTAL2")
    private Double total2;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALNETTO")
    private Double totalnetto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NAMA")
    private String nama;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "ALAMAT")
    private String alamat;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TELP")
    private String telp;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "KETERANGAN")
    private String keterangan;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TGLKIRIM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglkirim;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TGLJATUHTEMPO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgljatuhtempo;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TGLINVOICE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglinvoice;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "KETNOSURATJALAN")
    private String ketnosuratjalan;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "KETNOBUKTI")
    private String ketnobukti;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "KETKET1")
    private String ketket1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "KETKET2")
    private String ketket2;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOKASI")
    private short lokasi;

    public Vpubtrmst03() {
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public short getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(short transaksi) {
        this.transaksi = transaksi;
    }

    public String getNobukti() {
        return nobukti;
    }

    public void setNobukti(String nobukti) {
        this.nobukti = nobukti;
    }

    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public Date getTglposting() {
        return tglposting;
    }

    public void setTglposting(Date tglposting) {
        this.tglposting = tglposting;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getNomorph() {
        return nomorph;
    }

    public void setNomorph(Integer nomorph) {
        this.nomorph = nomorph;
    }

    public Double getDiscpersen0() {
        return discpersen0;
    }

    public void setDiscpersen0(Double discpersen0) {
        this.discpersen0 = discpersen0;
    }

    public Double getDiscrp0() {
        return discrp0;
    }

    public void setDiscrp0(Double discrp0) {
        this.discrp0 = discrp0;
    }

    public Double getDiscpersen1() {
        return discpersen1;
    }

    public void setDiscpersen1(Double discpersen1) {
        this.discpersen1 = discpersen1;
    }

    public Double getDiscrp1() {
        return discrp1;
    }

    public void setDiscrp1(Double discrp1) {
        this.discrp1 = discrp1;
    }

    public Double getPpnpersen() {
        return ppnpersen;
    }

    public void setPpnpersen(Double ppnpersen) {
        this.ppnpersen = ppnpersen;
    }

    public Double getPpnrp() {
        return ppnrp;
    }

    public void setPpnrp(Double ppnrp) {
        this.ppnrp = ppnrp;
    }

    public Double getBiayarp0() {
        return biayarp0;
    }

    public void setBiayarp0(Double biayarp0) {
        this.biayarp0 = biayarp0;
    }

    public Double getBiayarp1() {
        return biayarp1;
    }

    public void setBiayarp1(Double biayarp1) {
        this.biayarp1 = biayarp1;
    }

    public Double getTotalhpp() {
        return totalhpp;
    }

    public void setTotalhpp(Double totalhpp) {
        this.totalhpp = totalhpp;
    }

    public Double getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(Double totalbruto) {
        this.totalbruto = totalbruto;
    }

    public Double getTotaldisc0() {
        return totaldisc0;
    }

    public void setTotaldisc0(Double totaldisc0) {
        this.totaldisc0 = totaldisc0;
    }

    public Double getTotal0() {
        return total0;
    }

    public void setTotal0(Double total0) {
        this.total0 = total0;
    }

    public Double getTotaldisc1() {
        return totaldisc1;
    }

    public void setTotaldisc1(Double totaldisc1) {
        this.totaldisc1 = totaldisc1;
    }

    public Double getTotal1() {
        return total1;
    }

    public void setTotal1(Double total1) {
        this.total1 = total1;
    }

    public Double getTotaldisc() {
        return totaldisc;
    }

    public void setTotaldisc(Double totaldisc) {
        this.totaldisc = totaldisc;
    }

    public Double getTotalppn() {
        return totalppn;
    }

    public void setTotalppn(Double totalppn) {
        this.totalppn = totalppn;
    }

    public Double getTotal2() {
        return total2;
    }

    public void setTotal2(Double total2) {
        this.total2 = total2;
    }

    public Double getTotalnetto() {
        return totalnetto;
    }

    public void setTotalnetto(Double totalnetto) {
        this.totalnetto = totalnetto;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTglkirim() {
        return tglkirim;
    }

    public void setTglkirim(Date tglkirim) {
        this.tglkirim = tglkirim;
    }

    public Date getTgljatuhtempo() {
        return tgljatuhtempo;
    }

    public void setTgljatuhtempo(Date tgljatuhtempo) {
        this.tgljatuhtempo = tgljatuhtempo;
    }

    public Date getTglinvoice() {
        return tglinvoice;
    }

    public void setTglinvoice(Date tglinvoice) {
        this.tglinvoice = tglinvoice;
    }

    public String getKetnosuratjalan() {
        return ketnosuratjalan;
    }

    public void setKetnosuratjalan(String ketnosuratjalan) {
        this.ketnosuratjalan = ketnosuratjalan;
    }

    public String getKetnobukti() {
        return ketnobukti;
    }

    public void setKetnobukti(String ketnobukti) {
        this.ketnobukti = ketnobukti;
    }

    public String getKetket1() {
        return ketket1;
    }

    public void setKetket1(String ketket1) {
        this.ketket1 = ketket1;
    }

    public String getKetket2() {
        return ketket2;
    }

    public void setKetket2(String ketket2) {
        this.ketket2 = ketket2;
    }

    public short getLokasi() {
        return lokasi;
    }

    public void setLokasi(short lokasi) {
        this.lokasi = lokasi;
    }
    
}
