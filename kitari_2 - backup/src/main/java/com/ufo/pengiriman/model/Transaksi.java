/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "TRANSAKSI")
@NamedQueries({
    @NamedQuery(name = "Transaksi.findAll", query = "SELECT t FROM Transaksi t"),
    @NamedQuery(name = "Transaksi.findByTransaksi", query = "SELECT t FROM Transaksi t WHERE t.transaksi = :transaksi"),
    @NamedQuery(name = "Transaksi.findByNama", query = "SELECT t FROM Transaksi t WHERE t.nama = :nama")})
public class Transaksi implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSI")
    private Short transaksi;
    
    @Column(name = "NAMA")
    private String nama;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transaksi1", fetch = FetchType.LAZY)
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transaksi1")
    private Transaksilokasi transaksilokasi;

    public Transaksi() {
    }

    public Transaksi(Short transaksi) {
        this.transaksi = transaksi;
    }

    public Short getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(Short transaksi) {
        this.transaksi = transaksi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Transaksilokasi getTransaksilokasi() {
        return transaksilokasi;
    }

    public void setTransaksilokasi(Transaksilokasi transaksilokasi) {
        this.transaksilokasi = transaksilokasi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transaksi != null ? transaksi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaksi)) {
            return false;
        }
        Transaksi other = (Transaksi) object;
        if ((this.transaksi == null && other.transaksi != null) || (this.transaksi != null && !this.transaksi.equals(other.transaksi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Transaksi[ transaksi=" + transaksi + " ]";
    }
    
}
