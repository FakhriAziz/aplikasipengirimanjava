/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "STOK")
@NamedQueries({
    @NamedQuery(name = "Stok.findAll", query = "SELECT s FROM Stok s"),
    @NamedQuery(name = "Stok.findByNomor", query = "SELECT s FROM Stok s WHERE s.nomor = :nomor"),
    @NamedQuery(name = "Stok.findByKode", query = "SELECT s FROM Stok s WHERE s.kode = :kode"),
    @NamedQuery(name = "Stok.findByNama", query = "SELECT s FROM Stok s WHERE s.nama = :nama"),
    @NamedQuery(name = "Stok.findBySatuan0", query = "SELECT s FROM Stok s WHERE s.satuan0 = :satuan0"),
    @NamedQuery(name = "Stok.findBySatuan1", query = "SELECT s FROM Stok s WHERE s.satuan1 = :satuan1"),
    @NamedQuery(name = "Stok.findByFaktor", query = "SELECT s FROM Stok s WHERE s.faktor = :faktor"),
    @NamedQuery(name = "Stok.findByTampil", query = "SELECT s FROM Stok s WHERE s.tampil = :tampil"),
    @NamedQuery(name = "Stok.findByLogin", query = "SELECT s FROM Stok s WHERE s.login = :login"),
    @NamedQuery(name = "Stok.findByTglisi", query = "SELECT s FROM Stok s WHERE s.tglisi = :tglisi")})
public class Stok implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "KODE")
    private String kode;
    
    @Column(name = "NAMA")
    private String nama;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN0")
    private String satuan0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN1")
    private String satuan1;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "FAKTOR")
    private double faktor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TAMPIL")
    private short tampil;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorstok", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorstok")
    private Collection<Trdet> trdetCollection;
    
    @JoinColumn(name = "NOMORDISCSTOK", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Discstok nomordiscstok;
    
    @JoinColumn(name = "NOMORJENISSTOK", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Jenisstok nomorjenisstok;
    
    @JoinColumn(name = "NOMORLIMITSTOK", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Limitstok nomorlimitstok;

    public Stok() {
    }

    public Stok(Integer nomor) {
        this.nomor = nomor;
    }

    public Stok(Integer nomor, String kode, String satuan0, String satuan1, double faktor, short tampil, String login, Date tglisi) {
        this.nomor = nomor;
        this.kode = kode;
        this.satuan0 = satuan0;
        this.satuan1 = satuan1;
        this.faktor = faktor;
        this.tampil = tampil;
        this.login = login;
        this.tglisi = tglisi;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getSatuan0() {
        return satuan0;
    }

    public void setSatuan0(String satuan0) {
        this.satuan0 = satuan0;
    }

    public String getSatuan1() {
        return satuan1;
    }

    public void setSatuan1(String satuan1) {
        this.satuan1 = satuan1;
    }

    public double getFaktor() {
        return faktor;
    }

    public void setFaktor(double faktor) {
        this.faktor = faktor;
    }

    public short getTampil() {
        return tampil;
    }

    public void setTampil(short tampil) {
        this.tampil = tampil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public Collection<Trdet> getTrdetCollection() {
        return trdetCollection;
    }

    public void setTrdetCollection(Collection<Trdet> trdetCollection) {
        this.trdetCollection = trdetCollection;
    }

    public Discstok getNomordiscstok() {
        return nomordiscstok;
    }

    public void setNomordiscstok(Discstok nomordiscstok) {
        this.nomordiscstok = nomordiscstok;
    }

    public Jenisstok getNomorjenisstok() {
        return nomorjenisstok;
    }

    public void setNomorjenisstok(Jenisstok nomorjenisstok) {
        this.nomorjenisstok = nomorjenisstok;
    }

    public Limitstok getNomorlimitstok() {
        return nomorlimitstok;
    }

    public void setNomorlimitstok(Limitstok nomorlimitstok) {
        this.nomorlimitstok = nomorlimitstok;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stok)) {
            return false;
        }
        Stok other = (Stok) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Stok[ nomor=" + nomor + " ]";
    }
    
}
