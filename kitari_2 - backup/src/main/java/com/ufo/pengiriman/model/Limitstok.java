/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "LIMITSTOK")
@NamedQueries({
    @NamedQuery(name = "Limitstok.findAll", query = "SELECT l FROM Limitstok l"),
    @NamedQuery(name = "Limitstok.findByNomor", query = "SELECT l FROM Limitstok l WHERE l.nomor = :nomor"),
    @NamedQuery(name = "Limitstok.findByLimitbawah", query = "SELECT l FROM Limitstok l WHERE l.limitbawah = :limitbawah"),
    @NamedQuery(name = "Limitstok.findByLimitatas", query = "SELECT l FROM Limitstok l WHERE l.limitatas = :limitatas")})
public class Limitstok implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LIMITBAWAH")
    private double limitbawah;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LIMITATAS")
    private double limitatas;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorlimitstok", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorlimitstok")
    private Collection<Stok> stokCollection;

    public Limitstok() {
    }

    public Limitstok(Integer nomor) {
        this.nomor = nomor;
    }

    public Limitstok(Integer nomor, double limitbawah, double limitatas) {
        this.nomor = nomor;
        this.limitbawah = limitbawah;
        this.limitatas = limitatas;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public double getLimitbawah() {
        return limitbawah;
    }

    public void setLimitbawah(double limitbawah) {
        this.limitbawah = limitbawah;
    }

    public double getLimitatas() {
        return limitatas;
    }

    public void setLimitatas(double limitatas) {
        this.limitatas = limitatas;
    }

    public Collection<Stok> getStokCollection() {
        return stokCollection;
    }

    public void setStokCollection(Collection<Stok> stokCollection) {
        this.stokCollection = stokCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Limitstok)) {
            return false;
        }
        Limitstok other = (Limitstok) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Limitstok[ nomor=" + nomor + " ]";
    }
    
}
