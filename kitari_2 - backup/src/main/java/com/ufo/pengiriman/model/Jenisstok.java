/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "JENISSTOK")
@NamedQueries({
    @NamedQuery(name = "Jenisstok.findAll", query = "SELECT j FROM Jenisstok j"),
    @NamedQuery(name = "Jenisstok.findByNomor", query = "SELECT j FROM Jenisstok j WHERE j.nomor = :nomor")})
public class Jenisstok implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    @JoinColumn(name = "NOMORJENIS0", referencedColumnName = "NOMOR")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne(optional = false)
    private Jenis0 nomorjenis0;
    
    @JoinColumn(name = "NOMORJENIS1", referencedColumnName = "NOMOR")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne(optional = false)
    private Jenis1 nomorjenis1;
    
    @JoinColumn(name = "NOMORJENIS2", referencedColumnName = "NOMOR")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ManyToOne(optional = false)
    private Jenis2 nomorjenis2;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorjenisstok", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorjenisstok")
    private Collection<Stok> stokCollection;

    public Jenisstok() {
    }

    public Jenisstok(Integer nomor) {
        this.nomor = nomor;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public Jenis0 getNomorjenis0() {
        return nomorjenis0;
    }

    public void setNomorjenis0(Jenis0 nomorjenis0) {
        this.nomorjenis0 = nomorjenis0;
    }

    public Jenis1 getNomorjenis1() {
        return nomorjenis1;
    }

    public void setNomorjenis1(Jenis1 nomorjenis1) {
        this.nomorjenis1 = nomorjenis1;
    }

    public Jenis2 getNomorjenis2() {
        return nomorjenis2;
    }

    public void setNomorjenis2(Jenis2 nomorjenis2) {
        this.nomorjenis2 = nomorjenis2;
    }

    public Collection<Stok> getStokCollection() {
        return stokCollection;
    }

    public void setStokCollection(Collection<Stok> stokCollection) {
        this.stokCollection = stokCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jenisstok)) {
            return false;
        }
        Jenisstok other = (Jenisstok) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Jenisstok[ nomor=" + nomor + " ]";
    }
    
}
