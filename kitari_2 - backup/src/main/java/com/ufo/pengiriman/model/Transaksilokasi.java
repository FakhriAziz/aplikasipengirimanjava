/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "TRANSAKSILOKASI")
@NamedQueries({
    @NamedQuery(name = "Transaksilokasi.findAll", query = "SELECT t FROM Transaksilokasi t"),
    @NamedQuery(name = "Transaksilokasi.findByTransaksi", query = "SELECT t FROM Transaksilokasi t WHERE t.transaksi = :transaksi")})
public class Transaksilokasi implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSI")
    private Short transaksi;
    
    @JoinColumn(name = "LOKASI", referencedColumnName = "LOKASI")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Lokasi lokasi;
    
    @JoinColumn(name = "TRANSAKSI", referencedColumnName = "TRANSAKSI", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
//    @OneToOne(optional = false)
    private Transaksi transaksi1;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "transaksi", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transaksi")
    private Collection<Trmst> trmstCollection;

    public Transaksilokasi() {
    }

    public Transaksilokasi(Short transaksi) {
        this.transaksi = transaksi;
    }

    public Short getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(Short transaksi) {
        this.transaksi = transaksi;
    }

    public Lokasi getLokasi() {
        return lokasi;
    }

    public void setLokasi(Lokasi lokasi) {
        this.lokasi = lokasi;
    }

    public Transaksi getTransaksi1() {
        return transaksi1;
    }

    public void setTransaksi1(Transaksi transaksi1) {
        this.transaksi1 = transaksi1;
    }

    public Collection<Trmst> getTrmstCollection() {
        return trmstCollection;
    }

    public void setTrmstCollection(Collection<Trmst> trmstCollection) {
        this.trmstCollection = trmstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transaksi != null ? transaksi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaksilokasi)) {
            return false;
        }
        Transaksilokasi other = (Transaksilokasi) object;
        if ((this.transaksi == null && other.transaksi != null) || (this.transaksi != null && !this.transaksi.equals(other.transaksi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Transaksilokasi[ transaksi=" + transaksi + " ]";
    }
    
}
