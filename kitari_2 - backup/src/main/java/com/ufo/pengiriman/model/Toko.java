/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "TOKO")
@NamedQueries({
    @NamedQuery(name = "Toko.findAll", query = "SELECT t FROM Toko t"),
    @NamedQuery(name = "Toko.findByToko", query = "SELECT t FROM Toko t WHERE t.toko = :toko"),
    @NamedQuery(name = "Toko.findByNama", query = "SELECT t FROM Toko t WHERE t.nama = :nama")})
public class Toko implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TOKO")
    private Short toko;
    
    @Column(name = "NAMA")
    private String nama;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "toko", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toko")
    private Collection<Lokasi> lokasiCollection;

    public Toko() {
    }

    public Toko(Short toko) {
        this.toko = toko;
    }

    public Short getToko() {
        return toko;
    }

    public void setToko(Short toko) {
        this.toko = toko;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Collection<Lokasi> getLokasiCollection() {
        return lokasiCollection;
    }

    public void setLokasiCollection(Collection<Lokasi> lokasiCollection) {
        this.lokasiCollection = lokasiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (toko != null ? toko.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Toko)) {
            return false;
        }
        Toko other = (Toko) object;
        if ((this.toko == null && other.toko != null) || (this.toko != null && !this.toko.equals(other.toko))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Toko[ toko=" + toko + " ]";
    }
    
}
