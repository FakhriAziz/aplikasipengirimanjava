/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VPUBTRDETSISA")
@NamedQueries({
    @NamedQuery(name = "Vpubtrdetsisa.findAll", query = "SELECT v FROM Vpubtrdetsisa v"),
    @NamedQuery(name = "Vpubtrdetsisa.findByNomor", query = "SELECT v FROM Vpubtrdetsisa v WHERE v.nomor = :nomor"),
    @NamedQuery(name = "Vpubtrdetsisa.findByNomortrmst", query = "SELECT v FROM Vpubtrdetsisa v WHERE v.nomortrmst = :nomortrmst"),
    @NamedQuery(name = "Vpubtrdetsisa.findByNomorstok", query = "SELECT v FROM Vpubtrdetsisa v WHERE v.nomorstok = :nomorstok"),
    @NamedQuery(name = "Vpubtrdetsisa.findByQtyorder", query = "SELECT v FROM Vpubtrdetsisa v WHERE v.qtyorder = :qtyorder"),
    @NamedQuery(name = "Vpubtrdetsisa.findByQtykirim", query = "SELECT v FROM Vpubtrdetsisa v WHERE v.qtykirim = :qtykirim"),
    @NamedQuery(name = "Vpubtrdetsisa.findByQtysisa", query = "SELECT v.qtysisa FROM Vpubtrdetsisa v WHERE v.nomor = :nomor")})
public class Vpubtrdetsisa implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    @Id
    private Integer nomor;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMORTRMST")
    private Integer nomortrmst;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMORSTOK")
    private Integer nomorstok;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "QTYORDER")
    private Double qtyorder;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "QTYKIRIM")
    private Double qtykirim;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "QTYSISA")
    private Double qtysisa;

    public Vpubtrdetsisa() {
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public Integer getNomortrmst() {
        return nomortrmst;
    }

    public void setNomortrmst(Integer nomortrmst) {
        this.nomortrmst = nomortrmst;
    }

    public Integer getNomorstok() {
        return nomorstok;
    }

    public void setNomorstok(Integer nomorstok) {
        this.nomorstok = nomorstok;
    }

    public Double getQtyorder() {
        return qtyorder;
    }

    public void setQtyorder(Double qtyorder) {
        this.qtyorder = qtyorder;
    }

    public Double getQtykirim() {
        return qtykirim;
    }

    public void setQtykirim(Double qtykirim) {
        this.qtykirim = qtykirim;
    }

    public Double getQtysisa() {
        return qtysisa;
    }

    public void setQtysisa(Double qtysisa) {
        this.qtysisa = qtysisa;
    }
    
}
