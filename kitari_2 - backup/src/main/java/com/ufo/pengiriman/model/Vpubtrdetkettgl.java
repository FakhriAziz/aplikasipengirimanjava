package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VPUBTRDETKETTGL")
@NamedQueries({
    @NamedQuery(name = "Vpubtrdetkettgl.findAll", query = "SELECT v FROM Vpubtrdetkettgl v"),
    @NamedQuery(name = "Vpubtrdetkettgl.findByNomor", query = "SELECT v FROM Vpubtrdetkettgl v WHERE v.nomor = :nomor"),
    @NamedQuery(name = "Vpubtrdetkettgl.findByTgl1", query = "SELECT v FROM Vpubtrdetkettgl v WHERE v.tgl1 = :tgl1"),
    @NamedQuery(name = "Vpubtrdetkettgl.findByTgl2", query = "SELECT v FROM Vpubtrdetkettgl v WHERE v.tgl2 = :tgl2")})
public class Vpubtrdetkettgl implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    @Id
    private Integer nomor;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TGL1")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl1;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TGL2")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl2;

    public Vpubtrdetkettgl() {
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public Date getTgl1() {
        return tgl1;
    }

    public void setTgl1(Date tgl1) {
        this.tgl1 = tgl1;
    }

    public Date getTgl2() {
        return tgl2;
    }

    public void setTgl2(Date tgl2) {
        this.tgl2 = tgl2;
    }
    
}
