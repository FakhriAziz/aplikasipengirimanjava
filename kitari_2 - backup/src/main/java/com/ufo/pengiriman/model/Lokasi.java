/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "LOKASI")
@NamedQueries({
    @NamedQuery(name = "Lokasi.findAll", query = "SELECT l FROM Lokasi l"),
    @NamedQuery(name = "Lokasi.findByLokasi", query = "SELECT l FROM Lokasi l WHERE l.lokasi = :lokasi"),
    @NamedQuery(name = "Lokasi.findByNama", query = "SELECT l FROM Lokasi l WHERE l.nama = :nama")})
public class Lokasi implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOKASI")
    private Short lokasi;
    
    @Column(name = "NAMA")
    private String nama;
    
    @JoinColumn(name = "TOKO", referencedColumnName = "TOKO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Toko toko;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "lokasi", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lokasi")
    private Collection<Transaksilokasi> transaksilokasiCollection;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "lokasi", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lokasi")
    private Collection<Userid> useridCollection;

    public Lokasi() {
    }

    public Lokasi(Short lokasi) {
        this.lokasi = lokasi;
    }

    public Short getLokasi() {
        return lokasi;
    }

    public void setLokasi(Short lokasi) {
        this.lokasi = lokasi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Toko getToko() {
        return toko;
    }

    public void setToko(Toko toko) {
        this.toko = toko;
    }

    public Collection<Transaksilokasi> getTransaksilokasiCollection() {
        return transaksilokasiCollection;
    }

    public void setTransaksilokasiCollection(Collection<Transaksilokasi> transaksilokasiCollection) {
        this.transaksilokasiCollection = transaksilokasiCollection;
    }

    public Collection<Userid> getUseridCollection() {
        return useridCollection;
    }

    public void setUseridCollection(Collection<Userid> useridCollection) {
        this.useridCollection = useridCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lokasi != null ? lokasi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lokasi)) {
            return false;
        }
        Lokasi other = (Lokasi) object;
        if ((this.lokasi == null && other.lokasi != null) || (this.lokasi != null && !this.lokasi.equals(other.lokasi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Lokasi[ lokasi=" + lokasi + " ]";
    }
    
}
