/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "TRMST")
@NamedQueries({
    @NamedQuery(name = "Trmst.findAll", query = "SELECT t FROM Trmst t"),
    @NamedQuery(name = "Trmst.findByNomor", query = "SELECT t.nobukti FROM Trmst t WHERE t.nomor = :nomor"),
    @NamedQuery(name = "Trmst.findByNobukti", query = "SELECT t FROM Trmst t WHERE t.nobukti = :nobukti"),
    @NamedQuery(name = "Trmst.findByTgl", query = "SELECT t FROM Trmst t WHERE t.tgl = :tgl"),
    @NamedQuery(name = "Trmst.findByTglisi", query = "SELECT t FROM Trmst t WHERE t.tglisi = :tglisi"),
    @NamedQuery(name = "Trmst.findByTglposting", query = "SELECT t FROM Trmst t WHERE t.tglposting = :tglposting"),
    @NamedQuery(name = "Trmst.findByLogin", query = "SELECT t.login FROM Trmst t WHERE t.nobukti = :nomor"),
    @NamedQuery(name = "Trmst.findByDivisi", query = "SELECT t FROM Trmst t WHERE t.divisi = :divisi")})
public class Trmst implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOBUKTI")
    private String nobukti;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLPOSTING")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglposting;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    
    @Column(name = "DIVISI")
    private Integer divisi;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomortrmst", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomortrmst")
    private Collection<Trdet> trdetCollection;
    
    @JoinColumn(name = "NOMORPH", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Ph nomorph;
    
    @JoinColumn(name = "TRANSAKSI", referencedColumnName = "TRANSAKSI")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Transaksilokasi transaksi;

    public Trmst() {
    }

    public Trmst(Integer nomor) {
        this.nomor = nomor;
    }

    public Trmst(Integer nomor, String nobukti, Date tgl, Date tglisi, Date tglposting, String login) {
        this.nomor = nomor;
        this.nobukti = nobukti;
        this.tgl = tgl;
        this.tglisi = tglisi;
        this.tglposting = tglposting;
        this.login = login;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public String getNobukti() {
        return nobukti;
    }

    public void setNobukti(String nobukti) {
        this.nobukti = nobukti;
    }

    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public Date getTglposting() {
        return tglposting;
    }

    public void setTglposting(Date tglposting) {
        this.tglposting = tglposting;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getDivisi() {
        return divisi;
    }

    public void setDivisi(Integer divisi) {
        this.divisi = divisi;
    }

    public Collection<Trdet> getTrdetCollection() {
        return trdetCollection;
    }

    public void setTrdetCollection(Collection<Trdet> trdetCollection) {
        this.trdetCollection = trdetCollection;
    }

    public Ph getNomorph() {
        return nomorph;
    }

    public void setNomorph(Ph nomorph) {
        this.nomorph = nomorph;
    }

    public Transaksilokasi getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(Transaksilokasi transaksi) {
        this.transaksi = transaksi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trmst)) {
            return false;
        }
        Trmst other = (Trmst) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Trmst[ nomor=" + nomor + " ]";
    }
    
}
