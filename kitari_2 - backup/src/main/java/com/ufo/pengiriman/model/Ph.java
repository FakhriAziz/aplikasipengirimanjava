/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "PH")
@NamedQueries({
    @NamedQuery(name = "Ph.findAll", query = "SELECT p FROM Ph p"),
    @NamedQuery(name = "Ph.findByNomor", query = "SELECT p FROM Ph p WHERE p.nomor = :nomor"),
    @NamedQuery(name = "Ph.findByKode", query = "SELECT p FROM Ph p WHERE p.kode = :kode"),
    @NamedQuery(name = "Ph.findByJenis", query = "SELECT p FROM Ph p WHERE p.jenis = :jenis"),
    @NamedQuery(name = "Ph.findByNama", query = "SELECT p FROM Ph p WHERE p.nama = :nama"),
    @NamedQuery(name = "Ph.findByTampil", query = "SELECT p FROM Ph p WHERE p.tampil = :tampil"),
    @NamedQuery(name = "Ph.findByLogin", query = "SELECT p FROM Ph p WHERE p.login = :login"),
    @NamedQuery(name = "Ph.findByTglisi", query = "SELECT p FROM Ph p WHERE p.tglisi = :tglisi"),
    @NamedQuery(name = "Ph.findByKunci", query = "SELECT p FROM Ph p WHERE p.kunci = :kunci")})
public class Ph implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "KODE")
    private String kode;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "JENIS")
    private Character jenis;
    
    @Column(name = "NAMA")
    private String nama;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TAMPIL")
    private short tampil;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "KUNCI")
    private short kunci;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorph", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorph")
    private Collection<Trmst> trmstCollection;

    public Ph() {
    }

    public Ph(Integer nomor) {
        this.nomor = nomor;
    }

    public Ph(Integer nomor, String kode, Character jenis, short tampil, String login, Date tglisi, short kunci) {
        this.nomor = nomor;
        this.kode = kode;
        this.jenis = jenis;
        this.tampil = tampil;
        this.login = login;
        this.tglisi = tglisi;
        this.kunci = kunci;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Character getJenis() {
        return jenis;
    }

    public void setJenis(Character jenis) {
        this.jenis = jenis;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public short getTampil() {
        return tampil;
    }

    public void setTampil(short tampil) {
        this.tampil = tampil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public short getKunci() {
        return kunci;
    }

    public void setKunci(short kunci) {
        this.kunci = kunci;
    }

    public Collection<Trmst> getTrmstCollection() {
        return trmstCollection;
    }

    public void setTrmstCollection(Collection<Trmst> trmstCollection) {
        this.trmstCollection = trmstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ph)) {
            return false;
        }
        Ph other = (Ph) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Ph[ nomor=" + nomor + " ]";
    }

}
