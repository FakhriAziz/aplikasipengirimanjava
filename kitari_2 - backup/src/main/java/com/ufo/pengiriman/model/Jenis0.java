/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "JENIS0")
@NamedQueries({
    @NamedQuery(name = "Jenis0.findAll", query = "SELECT j FROM Jenis0 j"),
    @NamedQuery(name = "Jenis0.findByNomor", query = "SELECT j FROM Jenis0 j WHERE j.nomor = :nomor"),
    @NamedQuery(name = "Jenis0.findByJenis0", query = "SELECT j FROM Jenis0 j WHERE j.jenis0 = :jenis0"),
    @NamedQuery(name = "Jenis0.findByLogin", query = "SELECT j FROM Jenis0 j WHERE j.login = :login"),
    @NamedQuery(name = "Jenis0.findByTglisi", query = "SELECT j FROM Jenis0 j WHERE j.tglisi = :tglisi")})
public class Jenis0 implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "JENIS0")
    private String jenis0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorjenis0", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomorjenis0")
    private Collection<Jenisstok> jenisstokCollection;

    public Jenis0() {
    }

    public Jenis0(Integer nomor) {
        this.nomor = nomor;
    }

    public Jenis0(Integer nomor, String jenis0, String login, Date tglisi) {
        this.nomor = nomor;
        this.jenis0 = jenis0;
        this.login = login;
        this.tglisi = tglisi;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public String getJenis0() {
        return jenis0;
    }

    public void setJenis0(String jenis0) {
        this.jenis0 = jenis0;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public Collection<Jenisstok> getJenisstokCollection() {
        return jenisstokCollection;
    }

    public void setJenisstokCollection(Collection<Jenisstok> jenisstokCollection) {
        this.jenisstokCollection = jenisstokCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jenis0)) {
            return false;
        }
        Jenis0 other = (Jenis0) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Jenis0[ nomor=" + nomor + " ]";
    }
    
}
