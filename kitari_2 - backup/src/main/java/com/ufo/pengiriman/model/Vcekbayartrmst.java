/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VCEKBAYARTRMST")
@NamedQueries({
    @NamedQuery(name = "Vcekbayartrmst.findAll", query = "SELECT v FROM Vcekbayartrmst v"),
    @NamedQuery(name = "Vcekbayartrmst.findByNomor", query = "SELECT v FROM Vcekbayartrmst v WHERE v.nomor = :nomor"),
    @NamedQuery(name = "Vcekbayartrmst.findByTransaksi", query = "SELECT v FROM Vcekbayartrmst v WHERE v.transaksi = :transaksi"),
    @NamedQuery(name = "Vcekbayartrmst.findByTgl", query = "SELECT v FROM Vcekbayartrmst v WHERE v.tgl = :tgl"),
    @NamedQuery(name = "Vcekbayartrmst.findByNobukti", query = "SELECT v FROM Vcekbayartrmst v WHERE v.nobukti = :nobukti"),
    @NamedQuery(name = "Vcekbayartrmst.findByNama", query = "SELECT v FROM Vcekbayartrmst v WHERE v.nama = :nama"),
    @NamedQuery(name = "Vcekbayartrmst.findByTotalnetto", query = "SELECT v FROM Vcekbayartrmst v WHERE v.totalnetto = :totalnetto"),
    @NamedQuery(name = "Vcekbayartrmst.findByTotalbayar", query = "SELECT v FROM Vcekbayartrmst v WHERE v.totalbayar = :totalbayar")})
public class Vcekbayartrmst implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    @Id
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSI")
    private short transaksi;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOBUKTI")
    private String nobukti;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NAMA")
    private String nama;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALNETTO")
    private Double totalnetto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALBAYAR")
    private Double totalbayar;

    public Vcekbayartrmst() {
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public short getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(short transaksi) {
        this.transaksi = transaksi;
    }

    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public String getNobukti() {
        return nobukti;
    }

    public void setNobukti(String nobukti) {
        this.nobukti = nobukti;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Double getTotalnetto() {
        return totalnetto;
    }

    public void setTotalnetto(Double totalnetto) {
        this.totalnetto = totalnetto;
    }

    public Double getTotalbayar() {
        return totalbayar;
    }

    public void setTotalbayar(Double totalbayar) {
        this.totalbayar = totalbayar;
    }
    
}
