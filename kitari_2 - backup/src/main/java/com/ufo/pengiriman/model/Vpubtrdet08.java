package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VPUBTRDET08")
@NamedQueries({
    @NamedQuery(name = "Vpubtrdet08.findAll", query = "SELECT v FROM Vpubtrdet08 v"),
    @NamedQuery(name = "Vpubtrdet08.findByNomor", query = "SELECT v FROM Vpubtrdet08 v WHERE v.nomor = :nomor"),
    @NamedQuery(name = "Vpubtrdet08.findByNomortrmst", query = "SELECT v FROM Vpubtrdet08 v WHERE v.nomortrmst = :nomortrmst"),
    @NamedQuery(name = "Vpubtrdet08.findByNomorstok", query = "SELECT v FROM Vpubtrdet08 v WHERE v.nomorstok = :nomorstok"),
    @NamedQuery(name = "Vpubtrdet08.findByKodestok", query = "SELECT v FROM Vpubtrdet08 v WHERE v.kodestok = :kodestok"),
    @NamedQuery(name = "Vpubtrdet08.findByNamastok", query = "SELECT v FROM Vpubtrdet08 v WHERE v.namastok = :namastok"),
    @NamedQuery(name = "Vpubtrdet08.findByNomorjenisstok", query = "SELECT v FROM Vpubtrdet08 v WHERE v.nomorjenisstok = :nomorjenisstok"),
    @NamedQuery(name = "Vpubtrdet08.findByJenis0", query = "SELECT v FROM Vpubtrdet08 v WHERE v.jenis0 = :jenis0"),
    @NamedQuery(name = "Vpubtrdet08.findByJenis1", query = "SELECT v FROM Vpubtrdet08 v WHERE v.jenis1 = :jenis1"),
    @NamedQuery(name = "Vpubtrdet08.findByJenis2", query = "SELECT v FROM Vpubtrdet08 v WHERE v.jenis2 = :jenis2"),
    @NamedQuery(name = "Vpubtrdet08.findByBanyaknya", query = "SELECT v FROM Vpubtrdet08 v WHERE v.banyaknya = :banyaknya"),
    @NamedQuery(name = "Vpubtrdet08.findBySatuan", query = "SELECT v FROM Vpubtrdet08 v WHERE v.satuan = :satuan"),
    @NamedQuery(name = "Vpubtrdet08.findByHarga", query = "SELECT v FROM Vpubtrdet08 v WHERE v.harga = :harga"),
    @NamedQuery(name = "Vpubtrdet08.findByBanyaknya0", query = "SELECT v FROM Vpubtrdet08 v WHERE v.banyaknya0 = :banyaknya0"),
    @NamedQuery(name = "Vpubtrdet08.findBySatuan0", query = "SELECT v FROM Vpubtrdet08 v WHERE v.satuan0 = :satuan0"),
    @NamedQuery(name = "Vpubtrdet08.findBySatuan1", query = "SELECT v FROM Vpubtrdet08 v WHERE v.satuan1 = :satuan1"),
    @NamedQuery(name = "Vpubtrdet08.findByFaktor", query = "SELECT v FROM Vpubtrdet08 v WHERE v.faktor = :faktor"),
    @NamedQuery(name = "Vpubtrdet08.findByHpp", query = "SELECT v FROM Vpubtrdet08 v WHERE v.hpp = :hpp"),
    @NamedQuery(name = "Vpubtrdet08.findByTotalhpp", query = "SELECT v FROM Vpubtrdet08 v WHERE v.totalhpp = :totalhpp"),
    @NamedQuery(name = "Vpubtrdet08.findByTotalbruto", query = "SELECT v FROM Vpubtrdet08 v WHERE v.totalbruto = :totalbruto"),
    @NamedQuery(name = "Vpubtrdet08.findByTotalnetto", query = "SELECT v FROM Vpubtrdet08 v WHERE v.totalnetto = :totalnetto"),
    @NamedQuery(name = "Vpubtrdet08.findByHarganetto", query = "SELECT v FROM Vpubtrdet08 v WHERE v.harganetto = :harganetto"),
    @NamedQuery(name = "Vpubtrdet08.findByHargasatuannetto", query = "SELECT v FROM Vpubtrdet08 v WHERE v.hargasatuannetto = :hargasatuannetto"),
    @NamedQuery(name = "Vpubtrdet08.findByLokasi2", query = "SELECT v FROM Vpubtrdet08 v WHERE v.lokasi2 = :lokasi2"),
    @NamedQuery(name = "Vpubtrdet08.findByNamalokasi2", query = "SELECT v FROM Vpubtrdet08 v WHERE v.namalokasi2 = :namalokasi2"),
    @NamedQuery(name = "Vpubtrdet08.findByTgl", query = "SELECT v FROM Vpubtrdet08 v WHERE v.tgl = :tgl"),
    @NamedQuery(name = "Vpubtrdet08.findByTransaksi", query = "SELECT v FROM Vpubtrdet08 v WHERE v.transaksi = :transaksi"),
    @NamedQuery(name = "Vpubtrdet08.findByTransaksinama", query = "SELECT v FROM Vpubtrdet08 v WHERE v.transaksinama = :transaksinama"),
    @NamedQuery(name = "Vpubtrdet08.findByLokasi", query = "SELECT v FROM Vpubtrdet08 v WHERE v.lokasi = :lokasi"),
    @NamedQuery(name = "Vpubtrdet08.findByNamalokasi", query = "SELECT v FROM Vpubtrdet08 v WHERE v.namalokasi = :namalokasi"),
    @NamedQuery(name = "Vpubtrdet08.findByJenisinout", query = "SELECT v FROM Vpubtrdet08 v WHERE v.jenisinout = :jenisinout"),
    @NamedQuery(name = "Vpubtrdet08.findByJenisph", query = "SELECT v FROM Vpubtrdet08 v WHERE v.jenisph = :jenisph")})
public class Vpubtrdet08 implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    @Id
    private Integer nomor;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMORTRMST")
    private Integer nomortrmst;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NOMORSTOK")
    private Integer nomorstok;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "KODESTOK")
    private String kodestok;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NAMASTOK")
    private String namastok;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMORJENISSTOK")
    private int nomorjenisstok;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "JENIS0")
    private String jenis0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "JENIS1")
    private String jenis1;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "JENIS2")
    private String jenis2;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "BANYAKNYA")
    private double banyaknya;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN")
    private String satuan;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "HARGA")
    private double harga;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "BANYAKNYA0")
    private Double banyaknya0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN0")
    private String satuan0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN1")
    private String satuan1;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "FAKTOR")
    private double faktor;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "HPP")
    private Double hpp;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALHPP")
    private Double totalhpp;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALBRUTO")
    private Double totalbruto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TOTALNETTO")
    private Double totalnetto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "HARGANETTO")
    private Double harganetto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "HARGASATUANNETTO")
    private Double hargasatuannetto;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "LOKASI2")
    private Integer lokasi2;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NAMALOKASI2")
    private String namalokasi2;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSI")
    private short transaksi;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "TRANSAKSINAMA")
    private String transaksinama;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOKASI")
    private short lokasi;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "NAMALOKASI")
    private String namalokasi;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "JENISINOUT")
    private Integer jenisinout;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "JENISPH")
    private Character jenisph;

    public Vpubtrdet08() {
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public Integer getNomortrmst() {
        return nomortrmst;
    }

    public void setNomortrmst(Integer nomortrmst) {
        this.nomortrmst = nomortrmst;
    }

    public Integer getNomorstok() {
        return nomorstok;
    }

    public void setNomorstok(Integer nomorstok) {
        this.nomorstok = nomorstok;
    }

    public String getKodestok() {
        return kodestok;
    }

    public void setKodestok(String kodestok) {
        this.kodestok = kodestok;
    }

    public String getNamastok() {
        return namastok;
    }

    public void setNamastok(String namastok) {
        this.namastok = namastok;
    }

    public int getNomorjenisstok() {
        return nomorjenisstok;
    }

    public void setNomorjenisstok(int nomorjenisstok) {
        this.nomorjenisstok = nomorjenisstok;
    }

    public String getJenis0() {
        return jenis0;
    }

    public void setJenis0(String jenis0) {
        this.jenis0 = jenis0;
    }

    public String getJenis1() {
        return jenis1;
    }

    public void setJenis1(String jenis1) {
        this.jenis1 = jenis1;
    }

    public String getJenis2() {
        return jenis2;
    }

    public void setJenis2(String jenis2) {
        this.jenis2 = jenis2;
    }

    public double getBanyaknya() {
        return banyaknya;
    }

    public void setBanyaknya(double banyaknya) {
        this.banyaknya = banyaknya;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public Double getBanyaknya0() {
        return banyaknya0;
    }

    public void setBanyaknya0(Double banyaknya0) {
        this.banyaknya0 = banyaknya0;
    }

    public String getSatuan0() {
        return satuan0;
    }

    public void setSatuan0(String satuan0) {
        this.satuan0 = satuan0;
    }

    public String getSatuan1() {
        return satuan1;
    }

    public void setSatuan1(String satuan1) {
        this.satuan1 = satuan1;
    }

    public double getFaktor() {
        return faktor;
    }

    public void setFaktor(double faktor) {
        this.faktor = faktor;
    }

    public Double getHpp() {
        return hpp;
    }

    public void setHpp(Double hpp) {
        this.hpp = hpp;
    }

    public Double getTotalhpp() {
        return totalhpp;
    }

    public void setTotalhpp(Double totalhpp) {
        this.totalhpp = totalhpp;
    }

    public Double getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(Double totalbruto) {
        this.totalbruto = totalbruto;
    }

    public Double getTotalnetto() {
        return totalnetto;
    }

    public void setTotalnetto(Double totalnetto) {
        this.totalnetto = totalnetto;
    }

    public Double getHarganetto() {
        return harganetto;
    }

    public void setHarganetto(Double harganetto) {
        this.harganetto = harganetto;
    }

    public Double getHargasatuannetto() {
        return hargasatuannetto;
    }

    public void setHargasatuannetto(Double hargasatuannetto) {
        this.hargasatuannetto = hargasatuannetto;
    }

    public Integer getLokasi2() {
        return lokasi2;
    }

    public void setLokasi2(Integer lokasi2) {
        this.lokasi2 = lokasi2;
    }

    public String getNamalokasi2() {
        return namalokasi2;
    }

    public void setNamalokasi2(String namalokasi2) {
        this.namalokasi2 = namalokasi2;
    }

    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public short getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(short transaksi) {
        this.transaksi = transaksi;
    }

    public String getTransaksinama() {
        return transaksinama;
    }

    public void setTransaksinama(String transaksinama) {
        this.transaksinama = transaksinama;
    }

    public short getLokasi() {
        return lokasi;
    }

    public void setLokasi(short lokasi) {
        this.lokasi = lokasi;
    }

    public String getNamalokasi() {
        return namalokasi;
    }

    public void setNamalokasi(String namalokasi) {
        this.namalokasi = namalokasi;
    }

    public Integer getJenisinout() {
        return jenisinout;
    }

    public void setJenisinout(Integer jenisinout) {
        this.jenisinout = jenisinout;
    }

    public Character getJenisph() {
        return jenisph;
    }

    public void setJenisph(Character jenisph) {
        this.jenisph = jenisph;
    }
    
}
