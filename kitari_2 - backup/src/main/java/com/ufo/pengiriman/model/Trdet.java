package com.ufo.pengiriman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "TRDET")
@NamedQueries({
    @NamedQuery(name = "Trdet.findAll", query = "SELECT t FROM Trdet t"),
    @NamedQuery(name = "Trdet.findByNomor", query = "SELECT t FROM Trdet t WHERE t.nomor = :nomor"),
    @NamedQuery(name = "Trdet.findByBanyaknya", query = "SELECT t FROM Trdet t WHERE t.banyaknya = :banyaknya"),
    @NamedQuery(name = "Trdet.findBySatuan", query = "SELECT t FROM Trdet t WHERE t.satuan = :satuan"),
    @NamedQuery(name = "Trdet.findByHarga", query = "SELECT t FROM Trdet t WHERE t.harga = :harga"),
    @NamedQuery(name = "Trdet.findBySatuan0", query = "SELECT t FROM Trdet t WHERE t.satuan0 = :satuan0"),
    @NamedQuery(name = "Trdet.findBySatuan1", query = "SELECT t FROM Trdet t WHERE t.satuan1 = :satuan1"),
    @NamedQuery(name = "Trdet.findByFaktor", query = "SELECT t FROM Trdet t WHERE t.faktor = :faktor")})
public class Trdet implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "BANYAKNYA")
    private double banyaknya;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN")
    private String satuan;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "HARGA")
    private double harga;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN0")
    private String satuan0;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "SATUAN1")
    private String satuan1;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "FAKTOR")
    private double faktor;
    
    @JoinColumn(name = "NOMORSTOK", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Stok nomorstok;
    
    @JoinColumn(name = "NOMORTRMST", referencedColumnName = "NOMOR")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Trmst nomortrmst;

    public Trdet() {
    }

    public Trdet(Integer nomor) {
        this.nomor = nomor;
    }

    public Trdet(Integer nomor, double banyaknya, String satuan, double harga, String satuan0, String satuan1, double faktor) {
        this.nomor = nomor;
        this.banyaknya = banyaknya;
        this.satuan = satuan;
        this.harga = harga;
        this.satuan0 = satuan0;
        this.satuan1 = satuan1;
        this.faktor = faktor;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public double getBanyaknya() {
        return banyaknya;
    }

    public void setBanyaknya(double banyaknya) {
        this.banyaknya = banyaknya;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public String getSatuan0() {
        return satuan0;
    }

    public void setSatuan0(String satuan0) {
        this.satuan0 = satuan0;
    }

    public String getSatuan1() {
        return satuan1;
    }

    public void setSatuan1(String satuan1) {
        this.satuan1 = satuan1;
    }

    public double getFaktor() {
        return faktor;
    }

    public void setFaktor(double faktor) {
        this.faktor = faktor;
    }

    public Stok getNomorstok() {
        return nomorstok;
    }

    public void setNomorstok(Stok nomorstok) {
        this.nomorstok = nomorstok;
    }

    public Trmst getNomortrmst() {
        return nomortrmst;
    }

    public void setNomortrmst(Trmst nomortrmst) {
        this.nomortrmst = nomortrmst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trdet)) {
            return false;
        }
        Trdet other = (Trdet) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Trdet[ nomor=" + nomor + " ]";
    }
    
}
