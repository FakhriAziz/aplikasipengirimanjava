/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "USERID")
@NamedQueries({
    @NamedQuery(name = "Userid.findAll", query = "SELECT u FROM Userid u"),
    @NamedQuery(name = "Userid.findByNomor", query = "SELECT u FROM Userid u WHERE u.nomor = :nomor"),
    @NamedQuery(name = "Userid.findByKode", query = "SELECT u FROM Userid u WHERE u.kode = :kode"),
    @NamedQuery(name = "Userid.findByNama", query = "SELECT u FROM Userid u WHERE u.nama = :nama"),
    @NamedQuery(name = "Userid.findByPsw", query = "SELECT u FROM Userid u WHERE u.psw = :psw")})
public class Userid implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "KODE")
    private String kode;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NAMA")
    private String nama;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "PSW")
    private String psw;
    
    @JoinColumn(name = "LOKASI", referencedColumnName = "LOKASI")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    @ManyToOne(optional = false)
    private Lokasi lokasi;

    public Userid() {
    }

    public Userid(Integer nomor) {
        this.nomor = nomor;
    }

    public Userid(Integer nomor, String kode, String nama, String psw) {
        this.nomor = nomor;
        this.kode = kode;
        this.nama = nama;
        this.psw = psw;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Lokasi getLokasi() {
        return lokasi;
    }

    public void setLokasi(Lokasi lokasi) {
        this.lokasi = lokasi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userid)) {
            return false;
        }
        Userid other = (Userid) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Userid[ nomor=" + nomor + " ]";
    }
    
}
