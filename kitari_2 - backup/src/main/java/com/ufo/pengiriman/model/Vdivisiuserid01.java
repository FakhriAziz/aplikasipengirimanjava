/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "VDIVISIUSERID01")
@NamedQueries({
    @NamedQuery(name = "Vdivisiuserid01.findAll", query = "SELECT v FROM Vdivisiuserid01 v"),
    @NamedQuery(name = "Vdivisiuserid01.findByDivisinomor", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.divisinomor = :divisinomor"),
    @NamedQuery(name = "Vdivisiuserid01.findByDivisikode", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.divisikode = :divisikode"),
    @NamedQuery(name = "Vdivisiuserid01.findByDivisiketerangan", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.divisiketerangan = :divisiketerangan"),
    @NamedQuery(name = "Vdivisiuserid01.findByUseridnomor", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.useridnomor = :useridnomor"),
    @NamedQuery(name = "Vdivisiuserid01.findByUseridkode", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.useridkode = :useridkode"),
    @NamedQuery(name = "Vdivisiuserid01.findByUseridnama", query = "SELECT v FROM Vdivisiuserid01 v WHERE v.useridnama = :useridnama")})
public class Vdivisiuserid01 implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DIVISINOMOR")
    @Id
    private Integer divisinomor;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DIVISIKODE")
    private String divisikode;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "DIVISIKETERANGAN")
    private String divisiketerangan;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "USERIDNOMOR")
    private Integer useridnomor;
    
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "USERIDKODE")
    private String useridkode;
    
    //@Basic(fetch = FetchType.LAZY)
    @Column(name = "USERIDNAMA")
    private String useridnama;

    public Vdivisiuserid01() {
    }

    public Integer getDivisinomor() {
        return divisinomor;
    }

    public void setDivisinomor(Integer divisinomor) {
        this.divisinomor = divisinomor;
    }

    public String getDivisikode() {
        return divisikode;
    }

    public void setDivisikode(String divisikode) {
        this.divisikode = divisikode;
    }

    public String getDivisiketerangan() {
        return divisiketerangan;
    }

    public void setDivisiketerangan(String divisiketerangan) {
        this.divisiketerangan = divisiketerangan;
    }

    public Integer getUseridnomor() {
        return useridnomor;
    }

    public void setUseridnomor(Integer useridnomor) {
        this.useridnomor = useridnomor;
    }

    public String getUseridkode() {
        return useridkode;
    }

    public void setUseridkode(String useridkode) {
        this.useridkode = useridkode;
    }

    public String getUseridnama() {
        return useridnama;
    }

    public void setUseridnama(String useridnama) {
        this.useridnama = useridnama;
    }

}
