/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufo.pengiriman.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PROGRAMER
 */
@Entity
@Table(name = "DISCSTOK")
@NamedQueries({
    @NamedQuery(name = "Discstok.findAll", query = "SELECT d FROM Discstok d"),
    @NamedQuery(name = "Discstok.findByNomor", query = "SELECT d FROM Discstok d WHERE d.nomor = :nomor"),
    @NamedQuery(name = "Discstok.findByDiscrp0", query = "SELECT d FROM Discstok d WHERE d.discrp0 = :discrp0"),
    @NamedQuery(name = "Discstok.findByDiscpersen0", query = "SELECT d FROM Discstok d WHERE d.discpersen0 = :discpersen0"),
    @NamedQuery(name = "Discstok.findByDiscrp1", query = "SELECT d FROM Discstok d WHERE d.discrp1 = :discrp1"),
    @NamedQuery(name = "Discstok.findByDiscpersen1", query = "SELECT d FROM Discstok d WHERE d.discpersen1 = :discpersen1"),
    @NamedQuery(name = "Discstok.findByUprp0", query = "SELECT d FROM Discstok d WHERE d.uprp0 = :uprp0"),
    @NamedQuery(name = "Discstok.findByUppersen0", query = "SELECT d FROM Discstok d WHERE d.uppersen0 = :uppersen0"),
    @NamedQuery(name = "Discstok.findByUprp1", query = "SELECT d FROM Discstok d WHERE d.uprp1 = :uprp1"),
    @NamedQuery(name = "Discstok.findByUppersen1", query = "SELECT d FROM Discstok d WHERE d.uppersen1 = :uppersen1"),
    @NamedQuery(name = "Discstok.findByLogin", query = "SELECT d FROM Discstok d WHERE d.login = :login"),
    @NamedQuery(name = "Discstok.findByTglisi", query = "SELECT d FROM Discstok d WHERE d.tglisi = :tglisi")})
public class Discstok implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "NOMOR")
    private Integer nomor;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "DISCRP0")
    private double discrp0;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "DISCPERSEN0")
    private double discpersen0;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "DISCRP1")
    private double discrp1;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "DISCPERSEN1")
    private double discpersen1;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "UPRP0")
    private double uprp0;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "UPPERSEN0")
    private double uppersen0;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "UPRP1")
    private double uprp1;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "UPPERSEN1")
    private double uppersen1;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "LOGIN")
    private String login;
    //@Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "TGLISI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglisi;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "nomordiscstok", fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nomordiscstok")
    private Collection<Stok> stokCollection;

    public Discstok() {
    }

    public Discstok(Integer nomor) {
        this.nomor = nomor;
    }

    public Discstok(Integer nomor, double discrp0, double discpersen0, double discrp1, double discpersen1, double uprp0, double uppersen0, double uprp1, double uppersen1, String login, Date tglisi) {
        this.nomor = nomor;
        this.discrp0 = discrp0;
        this.discpersen0 = discpersen0;
        this.discrp1 = discrp1;
        this.discpersen1 = discpersen1;
        this.uprp0 = uprp0;
        this.uppersen0 = uppersen0;
        this.uprp1 = uprp1;
        this.uppersen1 = uppersen1;
        this.login = login;
        this.tglisi = tglisi;
    }

    public Integer getNomor() {
        return nomor;
    }

    public void setNomor(Integer nomor) {
        this.nomor = nomor;
    }

    public double getDiscrp0() {
        return discrp0;
    }

    public void setDiscrp0(double discrp0) {
        this.discrp0 = discrp0;
    }

    public double getDiscpersen0() {
        return discpersen0;
    }

    public void setDiscpersen0(double discpersen0) {
        this.discpersen0 = discpersen0;
    }

    public double getDiscrp1() {
        return discrp1;
    }

    public void setDiscrp1(double discrp1) {
        this.discrp1 = discrp1;
    }

    public double getDiscpersen1() {
        return discpersen1;
    }

    public void setDiscpersen1(double discpersen1) {
        this.discpersen1 = discpersen1;
    }

    public double getUprp0() {
        return uprp0;
    }

    public void setUprp0(double uprp0) {
        this.uprp0 = uprp0;
    }

    public double getUppersen0() {
        return uppersen0;
    }

    public void setUppersen0(double uppersen0) {
        this.uppersen0 = uppersen0;
    }

    public double getUprp1() {
        return uprp1;
    }

    public void setUprp1(double uprp1) {
        this.uprp1 = uprp1;
    }

    public double getUppersen1() {
        return uppersen1;
    }

    public void setUppersen1(double uppersen1) {
        this.uppersen1 = uppersen1;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getTglisi() {
        return tglisi;
    }

    public void setTglisi(Date tglisi) {
        this.tglisi = tglisi;
    }

    public Collection<Stok> getStokCollection() {
        return stokCollection;
    }

    public void setStokCollection(Collection<Stok> stokCollection) {
        this.stokCollection = stokCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nomor != null ? nomor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Discstok)) {
            return false;
        }
        Discstok other = (Discstok) object;
        if ((this.nomor == null && other.nomor != null) || (this.nomor != null && !this.nomor.equals(other.nomor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ufo.pengiriman.model.Discstok[ nomor=" + nomor + " ]";
    }
    
}
