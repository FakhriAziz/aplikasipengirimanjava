package com.ufo.pengiriman.hibernate;

import com.ufo.pengiriman.dao.AbstractDao;
import com.ufo.pengiriman.model.Vpubtrdet08;
import java.util.Base64;
import java.util.List;
import org.hibernate.Query;
import org.springframework.stereotype.Component;

/**
 *
 * @author PROGRAMER
 */
@Component
public class HibernateUtil extends AbstractDao<Integer, Vpubtrdet08> {
    
    public final String getNomorTRMST() {
        //Query Id = getSession().createSQLQuery("SELECT MAX(NOMOR) + 1 FROM TRMST");   //  baca dari generator saja
        Query Id = getSession().createSQLQuery("SELECT GEN_ID(GENNOMORTRMST, 1) FROM RDB$DATABASE");
        List<Integer> resultId = Id.list();
        return String.valueOf(resultId.get(0));
    }

    public final String setNomorPH(String kodeBukti) {
        Query PHTrmst = getSession().createSQLQuery("SELECT a.NOMORPH FROM TRMST a WHERE a.NOBUKTI = :cekPH").setParameter("cekPH", kodeBukti);
        List<String> kodePH = PHTrmst.list();
        return String.valueOf(kodePH.get(0));
    }
    
    public final Object[] setTRDET(String kodeBukti, String nomorTRDET, String nomorTRMST, boolean isMutasi) {
        System.out.println("\n======================== HibernateUtil: QUERY setTRDET(String kodeBukti, String nomorTRDET, String nomorTRMST, boolean isMutasi)");
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append("a.NOMORSTOK, ");     //#1, cekRowTrdet[0], pNomorStok
        sb.append("a.SATUAN, ");        //#2, cekRowTrdet[1], pSatuan
        sb.append("a.HARGA, ");         //#3, cekRowTrdet[2], pHarga
        sb.append("a.SATUAN0, ");       //#4, cekRowTrdet[3], pSatuan0
        sb.append("a.SATUAN1, ");       //#5, cekRowTrdet[4], pSatuan1
        sb.append("a.FAKTOR, ");        //#6, cekRowTrdet[5], pFaktor
        if(!isMutasi){
            sb.append("a.LOKASI2, ");   //#7, cekRowTrdet[6], pLokasi2 selain mutasi cukup copy lokasi2 dari order saja           
        } else {
            sb.append("b.LOKASI, ");    //#7, cekRowTrdet[6], pLokasi2 ambil nilai lokasi dari yg bikin order, jangan copy lokasi2 saja (a.LOKASI2)
        }
        sb.append("a.DISCPERSEN0, ");   //#8, cekRowTrdet[7], pDiscPersen0
        sb.append("a.DISCRP0, ");       //#9, cekRowTrdet[8], pDiscRp0
        sb.append("a.DISCPERSEN1, ");   //#10, cekRowTrdet[9], pDiscPersen1
        sb.append("a.DISCRP1, ");       //#11, cekRowTrdet[10], pDiscRp1
        sb.append("a.PPNPERSEN, ");     //#12, cekRowTrdet[11], pPpnPersen
        sb.append("a.PPNRP, ");         //#13, cekRowTrdet[12], pPpnRp
        sb.append("a.BIAYARP0, ");      //#14, cekRowTrdet[13], pBiayaRp0
        sb.append("a.BIAYARP1, ");      //#15, cekRowTrdet[14], pBiayaRp1
        sb.append("coalesce(c.KET0,'-'), ");    //#16, cekRowTrdet[15], pKet0
        sb.append("d.NAMA, ");          //#17, cekRowTrdet[16], pNamaStok
        sb.append("a.NOMOR ");          //#18, cekRowTrdet[17], pNomorRef
        sb.append("FROM VPUBTRDET04 a ");
        sb.append("INNER JOIN VPUBTRMST03 b on a.NOMORTRMST = b.NOMOR ");
        sb.append("LEFT JOIN TRDETKET c on a.NOMOR = c.NOMOR ");
        sb.append("INNER JOIN STOK d on a.NOMORSTOK = d.NOMOR ");
        sb.append("WHERE b.NOBUKTI = :kode ");          //nomor SO
        sb.append("AND a.NOMOR = :nomorTRDET ");        //nomor trdet SO
        sb.append("AND a.NOMORTRMST = :nomorTRMST");   //nomor trmst SO
        Query idUser = getSession().createSQLQuery(sb.toString())
                .setParameter("kode", kodeBukti)
                .setParameter("nomorTRDET", nomorTRDET)
                .setParameter("nomorTRMST", nomorTRMST);
        List<Object[]> result = (List<Object[]>) idUser.list();
        Object[] rowTrdet = (Object[]) result.get(0);
        return rowTrdet;
    }

    public final Integer setDecBukti(String noBuktiEnc) {  //dipanggil di public ModelAndView deleteBarang (ManageController.java)
        System.out.println("\n======================== HibernateUtil: method Integer setDecBukti(String noBuktiEnc)");
        System.out.println("\n parameter noBuktiEnc: " + noBuktiEnc);
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] ArrayBukti = decoder.decode(noBuktiEnc);
        String decBukti = new String(ArrayBukti);
        Integer tempBukti = Integer.parseInt(decBukti);
        System.out.println("\n return tempBukti: " + tempBukti);
        return tempBukti;
    }

    public Object[] getIdUser(String username) {
        System.out.println("\n======================== HibernateUtil: QUERY getIdUser(String username)");
        Query idUser = getSession().createSQLQuery("SELECT v.DIVISINOMOR, u.LOKASI FROM VDIVISIUSERID01 v"
                + " INNER JOIN USERID u on u.KODE = v.USERIDKODE"
                + " WHERE v.USERIDKODE = :cekUser")
                .setParameter("cekUser", username);
        List<Object[]> result = (List<Object[]>) idUser.list();
        Object[] rowUser = (Object[]) result.get(0);
        return rowUser;
    }

    public Double cekStatus(Integer nomor) {
        System.out.println("\n======================== HibernateUtil: QUERY cekStatus(Integer nomor)");
        return (Double) getSession().getNamedQuery("Vpubtrdetsisa.findByQtysisa").setParameter("nomor", nomor).uniqueResult();
    }

    public String getKetSO(String noBukti) {
        System.out.println("\n======================== HibernateUtil: QUERY getKetSO(String noBukti)");
        Query idKet = getSession().createSQLQuery("SELECT coalesce(p.KETERANGAN,'-') FROM TRMST t"
                + " INNER JOIN TRMSTPH p ON t.NOMOR = p.NOMOR"
                //eric: + " INNER JOIN TRMSTKET r ON t.NOMOR = r.NOMOR"   //off krn ga ngefek ngapain join lagi ke trmstket
                + " WHERE t.NOBUKTI = :cekBukti")
                .setParameter("cekBukti", noBukti);
        List<String> resultKet = idKet.list();
        String rowKet = resultKet.get(0);
        return rowKet;
    }
    
    public String getKetMutasi(String noBukti) {
        System.out.println("\n======================== HibernateUtil: QUERY getKetMutasi(String noBukti)");
        Query idKet = getSession().createSQLQuery("SELECT coalesce(tk.KET1,'-') FROM TRMST t"
                + " INNER JOIN TRMSTKET tk ON t.NOMOR = tk.NOMOR WHERE t.NOBUKTI = :cekBukti")
                .setParameter("cekBukti", noBukti);
        List<String> resultKet = idKet.list();
        String rowKet = resultKet.get(0);
        return rowKet;
    }
    
    public String getKetReturJual(String noBukti) {
        System.out.println("\n======================== HibernateUtil: QUERY getKetReturJual(String noBukti)");
        Query idKet = getSession().createSQLQuery("SELECT coalesce(tk.KET1,'-') FROM TRMST t"
                + " LEFT JOIN TRMSTKET tk ON t.NOMOR = tk.NOMOR WHERE t.NOBUKTI = :cekBukti")   //LEFT JOIN DULU
                .setParameter("cekBukti", noBukti);
        List<String> resultKet = idKet.list();
        String rowKet = resultKet.get(0);
        return rowKet;
    }
    
    public String getKetReturBeli(String noBukti) {
        System.out.println("\n======================== HibernateUtil: QUERY getKetReturBeli(String noBukti)");
        Query idKet = getSession().createSQLQuery("SELECT coalesce(tk.KET1,'-') FROM TRMST t"
                + " LEFT JOIN TRMSTKET tk ON t.NOMOR = tk.NOMOR WHERE t.NOBUKTI = :cekBukti")   //LEFT JOIN DULU
                .setParameter("cekBukti", noBukti);
        List<String> resultKet = idKet.list();
        String rowKet = resultKet.get(0);
        return rowKet;
    }
    
//    public String getKetPO(String noBukti) {
//        System.out.println("\n======================== HibernateUtil: QUERY getKetPO(String noBukti)");
//        Query idKet = getSession().createSQLQuery("SELECT coalesce(tk.KET1,'-') FROM TRMST t"
//                + " LEFT JOIN TRMSTKET tk ON t.NOMOR = tk.NOMOR WHERE t.NOBUKTI = :cekBukti")   //LEFT JOIN DULU
//                .setParameter("cekBukti", noBukti);
//        List<String> resultKet = idKet.list();
//        String rowKet = resultKet.get(0);
//        return rowKet;
//    }
    //KHUSUS PO, KETERANGAN AMBIL DARI TRMSTPH.KETERANGAN, BUKAN DARI TRMSTKET.KET1
    public String getKetPO(String noBukti) {
        System.out.println("\n======================== HibernateUtil: QUERY getKetPO(String noBukti)");
        Query idKet = getSession().createSQLQuery("SELECT coalesce(p.KETERANGAN,'-') FROM TRMST t"
                + " INNER JOIN TRMSTPH p ON t.NOMOR = p.NOMOR"
                //eric: + " INNER JOIN TRMSTKET r ON t.NOMOR = r.NOMOR"   //off krn ga ngefek ngapain join lagi ke trmstket
                + " WHERE t.NOBUKTI = :cekBukti")
                .setParameter("cekBukti", noBukti);
        List<String> resultKet = idKet.list();
        String rowKet = resultKet.get(0);
        return rowKet;
    }
}
