<nav class="navbar navbar-default header navbar-fixed-top">
    <div class="col-md-12 nav-wrapper">
        <div class="navbar-header" style="width:100%;">
            <div class="navbar-brand">
                Lokasi : <%=request.getAttribute("Lokasi")%> (<%=request.getAttribute("NamaLokasi")%>)   
            </div>
            
            <!--sonny, update 16/04/19 set off-->
<!--            <div class="navbar-brand">
                Qty terkirim hari ini : <%=request.getAttribute("cekTotalBatasKirim")%>
            </div>-->
            
            <%-- hidden toko karena sdh ada di list2/daftar2
            <div class="navbar-brand">
                Toko : <%=request.getAttribute("Toko")%>
            </div>
            --%>
            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><%= request.getAttribute("Nama")%></span></li>
                <li class="dropdown avatar-dropdown">
                    <img src="${contextPath}/resources/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                    <ul class="dropdown-menu user-dropdown">
                        <c:url value="j_spring_security_logout" var="logoutUrl" />
                        <form action="${logoutUrl}" method="post" id="logoutForm">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                        <script>
                            function formSubmit() {
                                document.getElementById("logoutForm").submit();
                            }
                        </script>
                        <li>
                            <a href="javascript:formSubmit()">
                                <span>Log Out</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>