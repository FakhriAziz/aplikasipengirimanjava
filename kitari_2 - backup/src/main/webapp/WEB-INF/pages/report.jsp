<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper2">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content">
                <div class="panel col-md-12 top-20 padding-0">
                    <div style="padding-left: 13px"><h3>Laporan SJ Penjualan</h3></div>
                    <c:if test="${not empty errtgl}">
                        <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
                            <center><strong>Peringatan!</strong> ${errtgl}</center>
                        </div>
                    </c:if>
                    <!--panel body atas, input pilih tanggal sj untuk menampilkan list laporan SJ penjualan-->
                    <div class="panel-body">
                        <form method="post" action="listReport">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="form-group">
                                <div class="left">
                                    <label>Tanggal SJ :</label>
                                    <input type="text" class="datepicker" name="tglLaporanAwal"/>
                                </div>
                                <div class="left">
                                    <label>&nbsp;s.d.&nbsp;</label>
                                    <input type="text" class="datepicker" name="tglLaporanAkhir"/>
                                </div>
                                <div class="left spacer">
                                    <!--<button type="submit" name="btnReport" class="btn btn-3d btn-default no_double_click">-->
                                    <button type="submit" name="btnReport" class="btn btn-3d btn-primary no_double_click">
                                        <span>Proses Tanggal</span>
                                    </button>   
                                </div>
                                <div class="left">
                                    <button id="btnExport" class="btn btn-3d btn-success" type="button">
                                        <span>Cetak</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--panel body bawah, list laporan SJ penjualan-->
                    <div class="panel-body">
                        <div class="responsive-table">
                            <table id="datatable" class="table table-striped table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th align="center">No.</th>
                                        <th align="center">Tanggal SJ</th>
                                        <th align="center">No. SJ</th>
                                        <th align="center">Tanggal SO</th>
                                        <th align="center">No. SO</th>
                                        <th align="center">Toko</th>
                                        <th align="center">Kode Barang</th>
                                        <th align="center">Nama Barang</th>
                                        <th align="center">Keterangan SO</th>
                                        <th align="center">Qty Kirim</th>
                                        <th align="center">Total Qty Order</th>
                                        <th align="center">Total Qty Kirim</th>
                                        <th align="center">Sisa Qty Kirim</th>
                                        <th align="center">Status Kirim</th>
                                        <!--  hidden display data nomor, tgl kirim so-->
<!--                                        <th>No. TRMST SO</th>
                                        <th>No. TRMST SJ</th>
                                        <th>No. TRDET SO</th>
                                        <th>No. TRDET SJ</th>
                                        <th>NOMOR REF SO</th>
                                        <th align="center">Tanggal Kirim SO</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--  varStatus untuk membantu penomoran baris data-->
                                    <!--from: MainController -> IF[daftarReport, listLaporan] backto: MainController goto: end-->
                                    <c:forEach var="data" items="${datalaporan}" varStatus="loopCounter">
                                        <tr>
                                            <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                            <td>${loopCounter.count}.</td>
                                            <td><fmt:formatDate pattern="dd/MM/YYYY" value="${data.TGL_SJ}"/></td>
                                            <td>${data.NOBUKTI_SJ}</td>
                                            <td><fmt:formatDate pattern="dd/MM/YYYY" value="${data.TGL_SO}"/></td>
                                            <td>${data.NOBUKTI_SO}</td>
                                            <!--utk toko di laporan, source query-nya beda dengan list kirim-->
                                            <td>${data.TOKO_SO}</td>
                                            <td>${data.KODESTOK_SJ}</td>
                                            <td>${data.NAMASTOK_SJ}</td>
                                            <td>
                                                <b>${data.KET_SO}</b><br/>
                                                <b>${data.SUBKET_SO}</b>    
                                            </td>
                                            <td align="right">${data.KIRIMPERSJ}</td>
                                            <td align="right">${data.TOTALORDER}</td>
                                            <td align="right">${data.TOTALKIRIMPARSIAL}</td>
                                            <td align="right">${data.SISAKIRIM}</td>
                                            <td>${data.RESULT}</td>
                                            <!--  hidden display data nomor, tgl kirim so-->
                                            <%--<td align="center">${data.NOTRMST_SO}</td>
                                            <td align="center">${data.NOTRMST_SJ}</td>
                                            <td align="center">${data.NOTRDET_SO}</td>
                                            <td align="center">${data.NOTRDET_SJ}</td>
                                            <td align="center">${data.NOMORREF_ORDER}</td>
                                            <td><fmt:formatDate pattern="dd/MM/YYYY" value="${data.TGLKIRIM}"/></td>
                                            --%>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script src="${contextPath}/resources/js/appfile.js"></script>
        <script src="${contextPath}/resources/js/jquery.base64.js"></script>
        <script type="text/javascript">
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                'date-uk-pre': function ( a ) {
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                'date-uk-asc': function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                'date-uk-desc': function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            
            $(function(){
                //  prevent multi click (no_double_click)
                var hitungKlik = 1;
                $('.no_double_click').click(function(event) {
                    hitungKlik++;
                    if (hitungKlik > 2) {
                        $(this).attr('disabled', 'disabled');
                        event.preventDefault();
                    }
                });
                
                //  datepicker
                $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset;}});
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy",
                    monthNames: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
                    dayNamesMin: [ "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" ]
                });
                
                //  datatables
                $("#datatable").DataTable({
                    aaSorting: [[0,"asc"]],
                    dom: "<'left'f>",
                    paging: false,
                    info: false,
                    aoColumns: [null,{sType: "date-uk"},null,{sType: "date-uk"},null,{sType: "date-uk"},null,null,null,null,null,null,null,null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sEmptyTable: "Silahkan input \"Tanggal SJ\" diatas untuk menampilkan laporan SJ Penjualan.",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan."
                    }
                });
                
                $("#btnExport").click(function(){
                    $("#tblExport").btechco_excelexport({
                        containerid: "datatable",
                        datatype: $datatype.Table,
                        filename: 'Laporan SJ Penjualan'
                    });
                });
            });
        </script>
    </body>
</html>