<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <!--off by sonny-->
                <!--<div class="page-404 animated flipInX">-->
                <div class="page-404">
                    <img src="${contextPath}/resources/img/404.png" class="img-responsive" style="margin-left: -50px; max-width: 35%;"/>
                    <div class="col-md-12" style="margin-top: -50px; font-size: 15px; color: red;">
                        <strong>
                            <c:choose>
                                <c:when test ="${errAlamat == true}">${errList} </c:when>
                                <c:when test ="${errNama == true}">${errList}</c:when>
                                <%--<c:when test ="${errReport == true}">${errList}</c:when> off karena "errReport" tidak digunakan--%>
                                <c:when test ="${errSave == true}">${errList}</c:when>
                                <c:when test ="${errUser == true}">${errList}</c:when>
                                <c:when test ="${errNomorBukti == true}">${errList}</c:when>
                                <c:otherwise>${errList}</c:otherwise>
                            </c:choose>
                        </strong>
                    </div>
                    <div class="col-md-12">
                        <c:choose>
                            <%--<c:when test ="${errReport == true || errUser == true}"> off karena "errReport" tidak digunakan--%> 
                            <c:when test ="${errUser == true}"> 
                                <button class="btn btn-3d btn-danger" type="button" OnClick="urlToList()">
                                    <span>KEMBALI</span>
                                </button>
                            </c:when> 
                            <c:otherwise>
                                <button class="btn btn-3d btn-danger" type="button" onClick="exitAndReload()">
                                    <span>KEMBALI</span>
                                </button>
                            </c:otherwise>
                        </c:choose>
                    </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script>
            function urlToList() {
                window.opener.location.href = "list";
                self.close();
            }
            //exit dan reload tanpa konfirm
            function exitAndReload() {
                window.close();
                window.opener.location.reload(true); 
            }
        </script>
    </body>
</html>
