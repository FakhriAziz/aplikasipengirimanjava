<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
  <style>
    * { box-sizing: border-box }
    body { background-color:#2E363F }
    .log-form {
      width: 40%;
      min-width: 320px;
      max-width: 475px;
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
      -moz-transform: translate(-50%, -50%);
      -o-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
    }
    .log-form form {
      background: #fff none repeat scroll 0 0;
      display: block;
      width: 100%;
      padding: 2em;
    }
    .log-form h2 {
      color: white;
      font-size: 1.35em;
      display: block;
      background-color: #3b5998;
      padding: .75em 1em .75em 1em;
      margin: 0;
      font-weight: 200;
    }
    .log-form input {
      display: block;
      margin: auto auto;
      width: 100%;
      margin-bottom: 2em;
      padding: .5em 0;
      border: none;
      border-bottom: 1px solid #eaeaea;
      padding-bottom: 1.25em;
      color: #757575;
    }
    .log-form input:focus {
      outline: none;
    }
    .log-form .btn {
      display: inline-block;
      background-color: #3b5998;
      border: 1px solid #1ba0a9;
      padding: .5em 2em;
      color: white;
      margin-right: .5em;
      box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
    }
    .log-form .btn:hover {
      background-color: #2293ED;
    }
  </style>
</head>

<body>
    <div class="log-form">
        <h2>KiTaRI (Kirim-Mutasi-Retur-Invoice)</h2>
        <c:if test="${not empty error}">
            <div style="background-color:silver; color:red;">
                <strong>Peringatan!</strong> ${error}
            </div>
        </c:if>
        <form action="j_spring_security_check" method="POST">
            <input type="text" title="username" name="j_kodeuser" placeholder="username" />
            <input type="password" title="password" name="j_passworduser" placeholder="password" />
            <button type="submit" class="btn">OK</button>
            <button type="reset" class="btn">Cancel</button>
        </form>
    </div>
</body>
</html>

<%-- skrip eric
<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
      <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard form-signin-wrapper">
        <div class="container">
            <form class="form-signin" action="<c:url value='j_spring_security_check' />" method='POST'>
                <div class="panel periodic-login">
                    <div class="panel-body text-center">
                        <h1 class="atomic-symbol">LOGIN</h1>
                        <c:if test="${not empty error}">
                            <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
                                <strong>Peringatan!</strong> ${error}
                            </div>
                        </c:if>
                        <c:if test="${not empty msg}">
                            <div class="alert alert-success alert-raised alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
                                <strong>Berhasil!</strong> ${msg}
                            </div>
                        </c:if>
                        <i class="icons icon-arrow-down"></i>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="text" class="form-text" name="j_kodeuser" required>
                            <span class="bar"></span>
                            <label>Username</label>
                        </div>
                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="password" class="form-text" name="j_passworduser" required>
                            <span class="bar"></span>
                            <label>Password</label>
                        </div>
                        <input type="submit" class="col-md-12 btn ripple btn-3d btn-default" value="Sign In"/>
                    </div>
                </div>
            </form>-->
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
    </body>
</html>
--%>