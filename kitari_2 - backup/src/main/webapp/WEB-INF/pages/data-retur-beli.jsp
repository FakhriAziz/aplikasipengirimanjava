<%-- 
    Document   : data-retur-beli
    Created on : Oct 19, 2018, 3:40:15 PM
    Author     : PROGRAMER
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <div class="container-fluid mimin-wrapper2">
            <div class="panel col-md-12 top-20 padding-0">
                <div style="padding-left: 13px"><h3>Daftar Terima Retur Beli</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <form id="formDataTRB" method="post" action="prosesTRB">
                            <!--panel body atas, input memo-->
                            <table class="table table-striped table-bordered" style="width:550px;" cellspacing="0">
                                <tr>
                                    <!--#1 to request-->
                                    <th align="center">Memo Terima Retur Beli (Max. 255 karakter)</th>
                                    <td>:</td>
                                    <td><textarea name="dataMemoTRB" cols="54" rows="3" class="memo"></textarea></td>
                                </tr>
                            </table>
                            <%  
                                SimpleDateFormat formater = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
                                SimpleDateFormat AppDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            %>
                            <table id="dataTRB" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">No.</th>
                                        <th align="center">Tanggal Order Retur Beli</th>
                                        <th align="center">Nomor Order Retur Beli</th>
                                        <th align="center">Toko</th>
                                        <th align="center">Kode Barang</th>
                                        <th align="center">Nama Barang</th>
                                        <th align="center">Qty</th>
                                        <th align="center">Qty<br>(Maks.)</th>
                                        <!--informasi harga beli dan supplier-->
                                        <!--<th align="center">Harga Beli</th>-->
                                        <th align="center">Supplier</th>
                                        <th align="center">Keterangan Order Retur Beli</th>
                                        <!--hidden display data nomor-->
<!--                                        <th>Nomor TRMST</th>
                                        <th>Nomor TRDET</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--varStatus untuk membantu penomoran baris data-->
                                    <!--from: MainController->model.addObject("daftarTerimaRB", list_terimaRB); goto: ManageController-->
                                    <c:forEach var="daftar" items="${daftarTerimaRB}" varStatus="loopCounter"> 
                                        <tr>
                                            <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                            <td>${loopCounter.count}.</td
                                            
                                            <!--#2 to request-->
                                            <c:set var="tanggal" value="${daftar.mapTglTRB}"/>
                                            <%
                                                String dateStr = String.valueOf(pageContext.getAttribute("tanggal"));
                                                Date result = formater.parse(dateStr);
                                            %>
                                            <td>
                                                <fmt:formatDate pattern="dd/MM/YYYY" value="${daftar.mapTglTRB}"/>
                                                <input type="hidden" name="dataTglTRB" value="<%=AppDateFormat.format(result)%>">
                                            </td>
                                            
                                            <!--#3 to request-->
                                            <td>${daftar.mapNomorBuktiTRB}
                                                <input type="hidden" name="dataNomorBuktiTRB" value="${daftar.mapNomorBuktiTRB}">
                                            </td>
                                            
                                            <!--#4 to request-->
                                            <td>${daftar.mapTokoTRB}
                                                <input type="hidden" name="dataTokoTRB" value="${daftar.mapTokoTRB}">
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan TERIMA RETUR BELI via sql query-->
                                            <td>${daftar.mapKodeStokTRB}</td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan TERIMA RETUR BELI via sql query-->
                                            <td>${daftar.mapNamaStokTRB}</td>
                                            
                                            <!--#5 to request-->
                                            <td align="right">${daftar.mapQtyTRB}
                                                <input type="hidden" name="dataQtyTRB" value="${daftar.mapQtyTRB}">
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, QTY ORB tidak ditampilkan di jasper-->
                                            <td align="right">${daftar.mapQtyORB}</td>
                                            
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan TERIMA RETUR BELI via sql query-->
                                            <!--<td align="right">Rp. ${daftar.mapHargaBeliTRB}</td>-->
                                            <!--<td>${daftar.mapSupplierTRB}</td>-->
                                            
                                            <!--UPDATE 2019-12-09: #6 to request, mapSupplierTRB sebelumnya TIDAK KE REQUEST-->
                                            <td>${daftar.mapSupplierTRB}
                                                <input type="hidden" name="dataSupplierTRB" value="${daftar.mapSupplierTRB}">
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan TERIMA RETUR BELI via sql query-->
                                            <td>${daftar.mapKetTRB}<br/>${daftar.mapSubKetTRB}</td>
                                            
                                            <!--#7 to request-->
                                            <input type="hidden" name="dataNomorTRMSTTRB" value="${daftar.mapNomorTRMSTTRB}">
                                            
                                            <!--#8 to request-->
                                            <input type="hidden" name="dataNomorTRDETTRB" value="${daftar.mapNomorTRDETTRB}">
                                            
                                            <!--UPDATE 2019-12-09: #9 to request-->
                                            <input type="hidden" name="dataAlamatTRMSTPHORB" value="${daftar.mapAlamatTRMSTPHORB}">
                                            
                                            <!--UPDATE 2019-12-09: #10 to request-->
                                            <input type="hidden" name="dataTelpTRMSTPHORB" value="${daftar.mapTelpTRMSTPHORB}">
                                            
                                            <!--hidden display data nomor-->
<!--                                            <td>${daftar.mapNomorTRMSTTRB}</td>
                                            <td>${daftar.mapNomorTRDETTRB}</td>-->
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                            <c:set var="setData" value="${daftarTerimaRB}"/>
                            <c:choose>
                                <c:when test="${not empty setData}">
                                    <div class="col-sm-2" style="text-align: right">
                                        <button class="btn btn-3d btn-primary" type="submit">
                                            <span>Simpan Data</span>
                                        </button>
                                    </div>
                                    <div class="left">
                                        <button class="btn btn-3d btn-danger" type="button" onClick="exitAndReload()">
                                            <span>Batal Data</span>
                                        </button> 
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="col-md-12" style="text-align:center">
                                        <button class="btn btn-3d btn-primary" type="button" onClick="window.close()">
                                            <span>Kembali ke Daftar Order Retur Beli</span>
                                        </button> 
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            //untuk form target list-retur-beli.jsp
            window.name = "myTRB";
                
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $(function(){ 
                //options induk datatables
                var options = {
                    aaSorting: [[0,"asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-2'i>",
                    aoColumns: [null,{sType: "date-uk"},null,null,null,null,null,null,null,null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Tidak ada Daftar Terima Retur Beli.",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    },
                    //set fixed width utk bbrp kolom
                    columnDefs: [
                        {'targets': [0,6], 'width': 25},
                        {'targets': [1,2], 'width': 75},
                        {'targets': 7, 'width': 35},
                        {'targets': 8, 'width': 155}
                    ]
                };
                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5,20,50,-1], ["Per 5 data", "Per 20 data", "Per 50 data", "Semua data"]]
                });
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });
                
                //eksekusi datatables dengan opsi1
                var oTable = $("#dataTRB").DataTable(opt1);
                
                //submission function
                $("#formDataTRB").on("submit", function(){
                    //destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                    oTable.destroy();
                    $("#dataTRB").DataTable(opt2);
                });
            });
            
            //exit dan reload dengan konfirmasi
            function exitAndReload() {
                var x = confirm('Anda yakin untuk membatalkan?');
                if (x) { 
                    window.close();
                    window.opener.location.reload(true); 
                }
            }
        </script>
    </body>
</html>
