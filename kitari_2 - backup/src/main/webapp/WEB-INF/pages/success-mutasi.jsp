<%-- 
    Document   : success-mutasi
    Created on : Apr 3, 2018, 6:50:59 AM
    Author     : SONNY
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <div class="page-404">
                    <form method="post" action="psmt" target="win_psmt" onsubmit="window.open('SJ MUTASI', 'win_psmt', 'resizeable=no');">
                        <img src="${contextPath}/resources/img/success.png" class="img-responsive"/><br/>
                        <!--ambil variabel dari list prosesMutasi di ManageController-->
                        <%--
                        <c:forEach var="lpm" items="${list_prosesMutasi}">
                            <!--#1 to request-->
                            <span>${lpm.dataMemoMutasi}</span>
                            <input type="hidden" name="rc_MemoMutasi" value="${lpm.dataMemoMutasi}">
                            <br/>
                            
                            <!--#2 to request-->
                            <span>${lpm.dataTglOM}</span>
                            <input type="hidden" name="rc_TglOM" value="${lpm.dataTglOM}">
                            <br/>
                            
                            <!--#3 to request-->
                            <span>${lpm.dataNomorBuktiOM}</span>
                            <input type="hidden" name="rc_NomorBuktiOM" value="${lpm.dataNomorBuktiOM}">
                            <br/>
                            
                            <!--#4 to request-->
                            <span>${lpm.dataTokoOM}</span>
                            <input type="hidden" name="rc_TokoOM" value="${lpm.dataTokoOM}">
                            <br/>
                            
                            <!--#5 to request-->
                            <span>${lpm.dataQtyKirimOM}</span>
                            <input type="hidden" name="rc_QtyKirimOM" value="${lpm.dataQtyKirimOM}">
                            <br/>
                            
                            <!--#6 to request-->
                            <span>${lpm.dataNomorTRMSTOM}</span>
                            <input type="hidden" name="rc_NomorTRMSTOM" value="${lpm.dataNomorTRMSTOM}">
                            <br/>
                            
                            <!--#7 to request-->
                            <span>${lpm.dataNomorTRDETOM}</span>
                            <input type="hidden" name="rc_NomorTRDETOM" value="${lpm.dataNomorTRDETOM}">
                            <br/>
                            <hr/>
                        </c:forEach>
                        --%>
                        <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                        <div class="col-md-12">
                            <button class="btn btn-3d btn-success" type="submit">
                                <span>Cetak Surat Jalan Mutasi</span>
                            </button>
                            <span class="spacer"></span>
                            <button class="btn btn-3d btn-danger" type="button" OnClick="confirmAndExit()">
                                <span>Tutup - Close</span>
                            </button> 
                        </div>
                    </form>
                </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script language="Javascript">
            $(function(){
                //disable back navigation
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });
                
                //disable F5/Ctrl+R, tp msh bisa reload jika klik kanan dan klik reload
                $(document).keydown(function(e){
                    var c = e.which || e.keyCode;
                    if (c===82 || c===116 || c===17) {
                        e.preventDefault();
                    }
                    
                }); 
            });
            
            function refreshParent(){ window.opener.location.reload(true); }
            window.onunload = refreshParent;
            
            //konfirmasi sebelum exit
            function confirmAndExit() {
                var x = confirm('Anda yakin untuk menutup form ini?');
                if (x) { window.close(); }
            }
        </script>
    </body>
</html>