<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <!--off by sonny-->
                <!--<div class="page-404 animated flipInX">-->
                <div class="page-404">
                    <img src="${contextPath}/resources/img/session.png" class="img-responsive" style="margin-bottom:-10px;margin-left:100px;"/>
                    <c:url value="j_spring_security_logout" var="logoutUrl" />
                    <form action="${logoutUrl}" method="post" id="logoutForm">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                    <script>
                        function formSubmit() {
                            document.getElementById("logoutForm").submit();
                        }
                    </script>
                    <a href="javascript:formSubmit()" style="color:rgb(33, 150, 243);"> <b>Kembali Ke Login</b>
                    </br>
                    <span class="icons icon-arrow-down" style="color:rgb(33, 150, 243);"></span>
                    </a>
                </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
    </body>
</html>
