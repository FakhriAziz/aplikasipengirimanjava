<%-- 
    Document   : success-pembelian
    Created on : Jul 23, 2019, 4:03:13 PM
    Author     : PROGRAMER
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <div class="page-404">
                    <form method="post" action="plpb" target="win_plpb" onsubmit="window.open('TERIMA LPB', 'win_plpb', 'resizeable=no');">
                        <img src="${contextPath}/resources/img/success.png" class="img-responsive"/><br/>
                        <div class="col-md-12">
                            <button class="btn btn-3d btn-success" type="submit">
                                <span>Cetak Form LPB</span>
                            </button>
                            <span class="spacer"></span>
                            <button class="btn btn-3d btn-danger" type="button" OnClick="confirmAndExit()">
                                <span>Tutup - Close</span>
                            </button> 
                        </div>
                    </form>
                </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script language="Javascript">
            $(function(){
                //disable back navigation
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });
                
                //disable F5/Ctrl+R, tp msh bisa reload jika klik kanan dan klik reload
                $(document).keydown(function(e){
                    var c = e.which || e.keyCode;
                    if (c===82 || c===116 || c===17) {
                        e.preventDefault();
                    }
                    
                }); 
            });
            
            function refreshParent(){ window.opener.location.reload(true); }
            window.onunload = refreshParent;
            
            //konfirmasi sebelum exit
            function confirmAndExit() {
                var x = confirm('Anda yakin untuk menutup form ini?');
                if (x) { window.close(); }
            }
        </script>
    </body>
</html>

