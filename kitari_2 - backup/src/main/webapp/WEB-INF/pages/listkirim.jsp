<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper2">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content">
                <div class="panel col-md-12 top-20 padding-0">
                    <div style="padding-left: 13px"><h3>Daftar SO</h3></div>
                    <c:if test="${not empty errtgl}">
                        <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">�</span>
                            </button>
                            <center><strong>Peringatan!</strong>${errtgl}</center>
                        </div>
                    </c:if>
                    <!--panel body atas, input pilih tanggal so untuk menampilkan list pengiriman dan cetak ke excel-->
                    <div class="panel-body">
                        <form method="post" action="list">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="form-group">
                                <div class="left">
                                    <label>Tanggal Transaksi :</label>
                                    <input type="text" class="datepicker" name="tglAwal"/>
                                </div>
                                <div class="left">
                                    <label>&nbsp;s.d.&nbsp;</label>
                                    <input type="text" class="datepicker" name="tglAkhir"/>
                                </div>
                                <div class="left spacer">
                                    <!--<button type="submit" name="btnKirim" class="btn btn-3d btn-default no_double_click">-->
                                    <button type="submit" name="btnKirim" class="btn btn-3d btn-primary no_double_click">
                                        <span>Proses Tanggal</span>
                                    </button>
                                </div>
                                <div class="left">
                                    <button id="btnExport" class="btn btn-3d btn-success" type="button">
                                        <span>Cetak</span>
                                    </button>
                                </div>
                                <div class="left spacer">
                                    <a target="_new" href="listSurat" style="text-decoration: none">
                                    <button id="btnExport" class="btn btn-3d btn-primary" type="button">
                                        <span>List Surat Jalan</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body"> <!-- FAF,Tambahan -->
<!--                        <div class="left">
                            <select id="sel" name="sel" style="padding-top: 10px; padding-bottom: 10px;" onChange="myNewFunction();">
                                <option value='0'>Pilih Kolom Search</option>
                                <option value='1'>Kode Barang</option>
                                <option value='2'>Nama Barang</option>
                            </select>
                        </div>-->
                        <div class="left">
                            <label>Cari berdasarkan tanggal kirim :</label>
                            <input type="text" size="25" name="display" id="display" />
                        </div>
<!--                        <div class="left spacer">
                            <input type="button" id="showVal" value="Value Property" class="btn btn-3d btn-success" />
                        </div>-->
                    </div>
                    <!--panel body bawah, list pengiriman-->
                    <div class="panel-body">
                        <div class="responsive-table">
                            <form id="formListKirim" method="post" target="myWindow" action="kirim"
                                  onsubmit="window.open('kirim', 'myWindow', 'resizeable=no');">
                                <!--<form id="formListKirim" method="get" target="iframe_listkirim" action="kirim">-->
                                <table id="tables" class="table table-striped table-bordered" style="width: 1127px">
                                    <thead>
                                        <tr>
                                            <th align="center">No.</th>
                                            <th align="center">Tanggal SO</th>
                                            <th align="center">No. SO</th>
                                            <!--sonny, add tanggal bayar dan nobukti pembayaran-->
                                            <th align="center">Tanggal Bayar</th>
                                            <th align="center">No. Pembayaran</th>
                                            <th align="center">Toko</th>
                                            <th align="center">Kode Barang</th>
                                            <th align="center">Nama Barang</th>
                                            <th align="center">Qty Kirim</th>
                                            <th align="center">Qty SO</th>
                                            <th align="center">Keterangan SO</th>
                                            <!--JENIS BARANG DIHAPUS-->
                                            <th align="center">Nama</th>
                                            <th align="center">Alamat</th>
                                            <th align="center">No. Telp/No. HP</th>
                                            <th align="center">No. Program Lama</th>
                                            <th align="center">SPM</th>
                                            <th align="center">Nama Driver</th> <!--FAF, Tambahan-->
                                            <th align="center">Nomor Plat</th>
                                            <!--  hidden display data gudang, nomor2, tgl kirim so-->
                                            <!--                                            <th>Gudang</th>
                                                                                        <th>Nomor TRMST</th>
                                                                                        <th>Nomor TRDET</th>
                                                                                        <th align="center">Tanggal Kirim SO</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--varStatus=loopCounter untuk membantu penomoran baris data (row numbers)-->
                                        <!--from: MainController->IF[daftarSO, listSO] backto: MainController goto: datapengirman-->
                                        <c:forEach var="setdata" items="${datakirim}" varStatus="loopCounter">
                                            <tr>
                                                <!--method loopCounter.index dimulai dari 0. loopCounter.count dimulai dari 1.-->
                                                <td>${loopCounter.count}.</td>

                                                <!--#1 to request, ganti mekanisme date dari semula java object menjadi format jstl-->
                                                <td>
                                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${setdata.TGL_SO}" var="tanggalSO" />
                                                    <fmt:formatDate value="${tanggalSO}" pattern="dd/MM/yyyy" var="displayTSO" />
                                                    <c:out value="${displayTSO}"/>
                                                    <input type="hidden" name="getTglSO" value="${displayTSO}"/>
                                                </td>

                                                <!--#2 to request-->
                                                <td>${setdata.NOMORBUKTI_SO}
                                                    <input type="hidden" name="getNomorBuktiSO" value="${setdata.NOMORBUKTI_SO}"/>
                                                </td>

                                                <!--sonny, add tanggal bayar dan nobukti pembayaran-->
                                                <!--#3 to request, UPDATE ADD TGL PEMBAYARAN (02 MEI 2019)-->
                                                <td>
                                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${setdata.TGL_IR}" var="tanggalIR"/>
                                                    <fmt:formatDate value="${tanggalIR}" pattern="dd/MM/yyyy" var="displayTIR"/>
                                                    <!--utk displayTIR, karena tdk bisa coalesce date ke '-', add ternary filter-->
                                                    <c:out value="${empty displayTIR ? '-' : displayTIR}"/>
                                                    <input type="hidden" name="getTglIR" value="${displayTIR}"/>
                                                </td>

                                                <!--#4 to request, UPDATE ADD NOBUKTI PEMBAYARAN (02 MEI 2019)-->
                                                <td>${setdata.NOMORBUKTI_IR}
                                                    <input type="hidden" name="getNomorBuktiIR" value="${setdata.NOMORBUKTI_IR}"/>
                                                </td>

                                                <!--#5 to request-->
                                                <td><b>${setdata.TOKO_SO}</b>
                                                    <input type="hidden" name="getTokoSO" value="${setdata.TOKO_SO}"/>
                                                </td>

                                                <!--#6 to request-->
                                                <td>${setdata.KODESTOK_SO}
                                                    <input type="hidden" name="getKodeStokSO" value="${setdata.KODESTOK_SO}"/>
                                                </td>

                                                <!--#7 to request-->
                                                <td>${setdata.NAMASTOK_SO}
                                                    <input type="hidden" name="getNamaStokSO" value="${setdata.NAMASTOK_SO}"/>
                                                </td>

                                                <!--#8 to request, ubah input type number ke text, add oninput utk float number (pecahan 2 digit di belakang koma)-->
                                                <td align="right">
                                                    <input type="text" class="cekjml" name="getQtyKirimSO" value="0" min="0" max="${setdata.QTY_SO}"
                                                           style="width: 65px; font-size: 13px;" onblur="this.value = this.value * 1;" required
                                                           oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                                </td>

                                                <!--#9 to request-->
                                                <td align="right">${setdata.QTY_SO}
                                                    <input type="hidden" name="getQtySO" value="${setdata.QTY_SO}"/>
                                                </td>

                                                <!--#10 & #11 to request-->
                                                <td><b>${setdata.KET_SO}</b><br/><b>${setdata.SUBKET_SO}</b>
                                                    <input type="hidden" name="getKetSO" value="${setdata.KET_SO}"/>
                                                    <input type="hidden" name="getSubKetSO" value="${setdata.SUBKET_SO}"/>
                                                </td>

                                                <!--JENIS BARANG DIHAPUS-->

                                                <!--#12 to request-->
                                                <td>${setdata.NAMACUSTOMER_SO}
                                                    <input type="hidden" name="getNamaCustomerSO" value="${setdata.NAMACUSTOMER_SO}"/>
                                                </td>

                                                <!--#13 to request, ganti ke textarea untuk alamat-->
                                                <td> <!--FAF, Text area dinonaktifkan-->
                                                    <input type="hidden" name="getAlamatCustomerSO" value="${setdata.ALAMATCUSTOMER_SO}"/>
                                                    <!--<textarea name="getAlamatCustomerSO" cols="30" rows="3">${setdata.ALAMATCUSTOMER_SO}</textarea>-->
                                                </td>

                                                <!--#14 to request, ganti ke textarea untuk telp-->
                                                <td> <!--FAF, Text area dinonaktifkan-->
                                                    <input type="hidden" name="getTelpCustomerSO" value="${setdata.TELPCUSTOMER_SO}"/>
                                                    <!--<textarea name="getTelpCustomerSO" cols="21" rows="3">${setdata.TELPCUSTOMER_SO}</textarea>-->
                                                </td>

                                                <!--#15 to request-->
                                                <td>${setdata.NOMORPROGLAMA}
                                                    <input type="hidden" name="getNomorProgLama" value="${setdata.NOMORPROGLAMA}"/>
                                                </td>

                                                <!--#16 to request-->
                                        <input type="hidden" name="getGudangSJ" value="${setdata.GUDANG_SJ}"/>

                                        <!--#17 to request-->
                                        <input type="hidden" name="getNomorTRMSTSO" value="${setdata.NOMORTRMST_SO}"/>

                                        <!--#18 to request-->
                                        <input type="hidden" name="getNomorTRDETSO" value="${setdata.NOMORTRDET_SO}"/>

                                        <!--#19 to request-->
                                        <fmt:parseDate pattern="yyyy-MM-dd" value="${setdata.TGLKIRIM_SO}" var="tanggalKSO"/>
                                        <fmt:formatDate value="${tanggalKSO}" pattern="dd/MM/yyyy" var="displayTKSO" />
                                        <input type="hidden" name="getTglKirimSO" value="${displayTKSO}"/>

                                        <!--#20 to request-->
                                        <td>${setdata.SPM}
                                            <input type="hidden" name="getSPM" value="${setdata.SPM}"/>
                                        </td>
                                        <td><!--FAF, Tambahan-->
                                            <input type="text" name="getNAMA_DRIVER" cols="21" rows="3" value="0" min="0" max="${setdata.NAMA_DRIVER}"/>
                                        </td>
                                        <td>
                                            <input type="text" name="getNO_PLAT" cols="21" rows="3" value="0" min="0" max="${setdata.NO_PLAT}"/>
                                        </td>

                                        <!--  hidden display data gudang, nomor2, tgl kirim so-->
<!--                                                <td align="center">${setdata.GUDANG_SJ}</td>
                                        <td align="center">${setdata.NOMORTRMST_SO}</td>
                                        <td align="center">${setdata.NOMORTRDET_SO}</td>
                                        <td align="center">${setdata.TGLKIRIM_SO}</td>
                                        -->
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                                <c:set var="setDaftar" value="${datakirim}"/>
                                <c:choose>
                                    <c:when test="${not empty setDaftar}">
                                        <div class="col-md-12" style="text-align: center">
                                            <button class="btn btn-3d btn-primary" type="submit">
                                                <span>Lihat Data</span>
                                            </button>
                                        </div>
                                    </c:when>
                                </c:choose>
                                <!--<iframe id="iframe_listkirim" style="width:0; height:0; border:0; border:none"></iframe>-->
                            </form>
                        </div>
                    </div>
                    <!--COBA TAMPILKAN JSON-->
                    <!--                    <div>
                                            <strong>${datakirimJson}</strong>
                                        </div>-->
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script src="${contextPath}/resources/js/appexcel.js"></script>
        <script type="text/javascript">

//            function myNewFunction(element) {
//                var text = element.options[element.selectedIndex].text;
//                document.getElementById("test").innerHTML = text;
//            }

            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split("/");
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $(function () {
                //  unblock saat load/reload halaman
                setTimeout($.unblockUI, 2000);

                //  prevent multi click (no_double_click)
                var hitungKlik = 1;
                $(".no_double_click").click(function (event) {
                    hitungKlik++;
                    if (hitungKlik > 2) {
                        $(this).attr("disabled", "disabled");
                        event.preventDefault();
                    }
                });

                //  datepicker
                $.extend($.datepicker, {_checkOffset: function (inst, offset, isFixed) {
                        return offset;
                    }});
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy",
                    monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                    dayNamesMin: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
                });

                //  options induk datatables
                var options = {
                    aaSorting: [[0, "asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-6'i>",
                    aoColumns: [null, {sType: "date-uk"}, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Silahkan input \"Tanggal Transaksi\" diatas untuk menampilkan \"Daftar SO\".",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    }
                    //'dom': '<"left"f>',
                    //'dom': '<"left"f><"clear"l>rtip',
                    //'paging': false,
                    //'info': false,
                    //'columnDefs': [{'targets': '_all', 'width': 'auto'}],
                };

//                var el = document.getElementById('display');
//                
//                function getSelectedOption(sel) {
//                    var opt;
//                    for ( var i = 0, len = sel.options.length; i < len; i++ ) {
//                        opt = sel.options[i];
//                        if ( opt.selected === true ) {
//                            break;
//                        }
//                    }
//                    return opt;
//                }
                
//                document.getElementById('showVal').onclick = function () {
//                    el.value = sel.value;    
//                }

                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5, 20, 50, -1], ["Per 5 data", "Per 20 data", "Per 50 data", "Semua data"]]
                });
                
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });

                //  eksekusi datatables dengan opsi1
                var oTable = $("#tables").DataTable(opt1);
//                var colSearch;
//                var sel = document.getElementById("sel");
//
//                console.log("param = " + sel.value);

//                if (sel === 1){
//                    colSearch = "1";
//                } else if (sel === 2) {
//                    colSearch = "2";
//                }

                $('#display').on( 'keyup', function () { //FAF, Tambah search 1 kolom
                    oTable
                        .columns(1)
                        .search( this.value )
                        .draw();
                });

//                var table = $('#example').DataTable();

//                oTable.columns('.select-filter').every(function () {
//                    var that = this;
//
//                    // Create the select list and search operation
//                    var select = $('<select />')
//                            .appendTo(
//                                    this.footer()
//                                    )
//                            .on('change', function () {
//                                that
//                                        .search($(this).val())
//                                        .draw();
//                            });

                    // Get the search data for the first column and add to the select list
//                    this
//                            .cache('search')
//                            .sort()
//                            .unique()
//                            .each(function (d) {
//                                select.append($('<option value="' + d + '">' + d + '</option>'));
//                            });
//                });


//                $('#tables thead th').each(function () { //FAF, Tambahan
//                    var title = $(this).text();
//                    $(this).html(title+' <input type="text" class="col-search-input" placeholder="Search ' + title + '" />');
//                });
//                
//                table.columns().every(function () { //FAF, Tambahan
//                    var table = this;
//                    $('input', this.header()).on('keyup change', function () {
//                        if (table.search() !== this.value) {
//                               table.search(this.value).draw();
//                        }
//                    });
//                });



                /* solusi utk validasi input yg loop/iterasi dengan support pagination datatable */

                var allCekJml = oTable.$(":input.cekjml", {"page": "all"});

                //  BEGIN formListKirim submission function
                $("#formListKirim").on("submit", function (evt) {
                    //cegah submit dulu agar bisa run blockUI
                    evt.preventDefault();

                    //  block halaman saat submission dan jika ada isian qty kirim lebih dari 0
                    var cekQty = $("input#cekjml");
                    $.each(cekQty, function () {
                        if ($(this).val() > '0') {
                            $.blockUI({
                                css: {
                                    border: "none",
                                    padding: "15px 0",
                                    backgroundColor: "#000",
                                    "-webkit-border-radius": "10px",
                                    "-moz-border-radius": "10px",
                                    opacity: .5,
                                    color: "#fff"
                                },
                                message: "<h4>..harap tunggu, masih proses entry pengiriman..</h4>"
                            });
                        }//end if
                    });
                    //  destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                    oTable.destroy();
                    $("#tables").DataTable(opt2);

                    //bind ulang untuk submit form
                    $(this).unbind('submit').submit();

                }); //END formListKirim submission function

                //cek input jumlah min-max
//                $("#cekjml").change(function () { //jquery .change dgn id ($) ada bug, ganti by class
                allCekJml.change(function () {
                    var max = parseFloat($(this).attr("max"));  //sonny, ubah parseInt ke parseFloat
                    var min = parseFloat($(this).attr("min"));
                    if ($(this).val() > max) {
                        $(this).val(max);
                    }
                });

                //cetak ke excel
                $("#btnExport").click(function () {
                    $("#tables").exportExcel({
                        name: "Pengiriman",
                        filename: "Daftar Order Penjualan (SO)",
                        fileext: ".xls"
                    });
                });
            });
        </script>
    </body>
</html>