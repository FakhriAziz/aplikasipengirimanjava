<%@page import="java.nio.charset.StandardCharsets"%>
<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content">
                <div class="panel col-md-12 top-20 padding-0">
                    <div style="padding-left: 13px"><h3>Daftar Surat Jalan (Penjualan &AMP; Mutasi)</h3></div>
                    <c:if test="${not empty errtgl}">
                        <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
                            <center><strong>Peringatan!</strong> ${errtgl}</center>
                        </div>
                    </c:if>
                    <c:if test="${not empty msg}">
                        <div class="alert alert-success alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">�</span>
                            </button>
                            <center>
                                <strong>Berhasil!</strong>${msg}
                            </center>
                        </div>
                    </c:if>
                    <!--panel body atas, input pilih tanggal untuk menampilkan list surat jalan-->
                    <div class="panel-body">
                        <form method="post" action="listSurat">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="form-group">
                                <div class="left">
                                    <label>Tanggal Kirim :</label>
                                    <input type="text" class="datepicker" name="tglSuratAwal"/>
                                </div>
                                <div class="left">
                                    <label>&nbsp;s.d.&nbsp;</label>
                                    <input type="text" class="datepicker" name="tglSuratAkhir"/>
                                </div>
                                
                                <div class="left spacer">
                                    <!--<button type="submit" name="btnSurat" class="btn btn-3d btn-default no_double_click">-->
                                    <button type="submit" name="btnSurat" class="btn btn-3d btn-primary no_double_click">
                                        <span>Proses Tanggal</span>
                                    </button>   
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--panel body bawah, list surat jalan-->
                    <div class="panel-body">
                        <div class="responsive-table">
                            <form id="formListKirim" method="post" target="myWindow" action="re-psj"
                                  onsubmit="window.open('kirim', 'myWindow', 'resizeable=no');">
                                <table id="tables" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th align="center">No.</th>
                                            <th align="center">No Transaksi</th>
                                            <th align="center">Tanggal Kirim</th>
                                            <th align="center">Transaksi</th>
                                            <th align="center">No. Surat Jalan</th>
                                            <!--  untuk Nama, Alamat, No. Telp dipindah ke detail.jsp-->
                                            <!--<th>Memo<br/>(untuk mutasi)</th>-->
                                            <th align="center" style="width: 300px;">Keterangan Order</th>
                                            <th align="center">Aksi</th>
                                            <!--  hidden display data nomor-->
                                            <!--<th>Nomor TRMST</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--  varStatus untuk membantu penomoran baris data-->
                                        <c:forEach var="list" items="${listsurat}" varStatus="loopCounter">
                                            <!--var encNomor untuk enkpNomor yg ditangkap method ModelAndView deleteBarang di ManageController-->
                                            <c:set var="encNomor" value="${list.NOMORTRMST_SJ_MT}"/>
                                            <tr>
                                                <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                                <td>${loopCounter.count}.</td>
                                                <td>${list.NOMORTRMST_SJ_MT}
                                                    <input type="hidden" name="getNomorTRMST" value="${list.NOMORTRMST_SJ_MT}"/>
                                                </td>
                                                <td><fmt:formatDate pattern="dd/MM/YYYY" value="${list.TGL_SJ_MT}"/></td>
                                                <!--  filter jenis transaksi apakah penjualan atau mutasi-->
                                                <c:choose>
                                                    <c:when test="${list.TRANSAKSI < 6100}">
                                                        <td>Mutasi</td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td>Penjualan</td>
                                                    </c:otherwise>
                                                </c:choose>
                                                <!--teknik lain dari eric untuk memanggil jendela baru dibantu jquery-->
                                                <td><a target="_new" href="findBarang?idBukti=${list.NOMORBUKTI_SJ_MT}">${list.NOMORBUKTI_SJ_MT}</a></td>
                                                <!--  muat memo disini-->
                                                <!--<td align="center">MEMOOOO</td>-->
                                                <!--  untuk Nama, Alamat, No. Telp dipindah ke detail.jsp-->
                                                <!--  filter keterangan untuk tampil ket mutasi atau ket SO-->
                                                <c:choose>
                                                    <c:when test="${list.TRANSAKSI < 6100}">
                                                        <td>${list.KET_OM}</td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td>${list.KET_SO}</td>
                                                    </c:otherwise>
                                                </c:choose>
                                                <!--  MULAI BAGIAN AKSI-->
                                                <%  
                                                    String Nomor = String.valueOf(pageContext.getAttribute("encNomor"));
                                                    String enkpNomor = enk.encodeToString(Nomor.getBytes(StandardCharsets.UTF_8));

                                                    String tSAw = request.getParameter("tglSuratAwal");
                                                    String tSAk = request.getParameter("tglSuratAkhir");
                                                %>
                                                <!--  assign nilai parameter request dalam var jstl-->
                                                <c:set var="tSAw" value="<%=tSAw%>"/>
                                                <c:set var="tSAk" value="<%=tSAk%>"/>
                                                <td>
                                                    <!--  tambah button detail-->
                                                    <a target="_new" href="findBarang?idBukti=${list.NOMORBUKTI_SJ_MT}" style="text-decoration: none">
                                                        <button class="btn btn-3d btn-primary" type="button">
                                                            <span>Detail</span>
                                                        </button>
                                                    </a>
                                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <%--HANYA MANAGER YG BISA KLIK HAPUS SJ--%>
                                                    <!--<span>NOMOR DIVISI: ${NomorDivisi}</span>-->

                                                    <c:choose>
                                                        <%--eric pake DIVISIKODE ~ DIVISI, sonny pake nomor DIVISINOMOR ~ NomorDivisi--%>
                                                        <c:when test="${NomorDivisi=='5'}">
                                                            <!--  seleksi jika ada tSAw dan tSAk maka tambahkan ke URL dan lempar ke method deleteBarang di ManageController.java-->
                                                            <a method="post" action="re-psj" target="win_psj" onsubmit="window.open('SURAT JALAN PENJUALAN', 'win_psj', 'resizeable=no');">   
                                                                <button class="btn ripple btn-3d btn-success" type="submit">
                                                                    <span class="fa fa-trash">Re-Print</span>
                                                                </button>
                                                            </a>    
                                                        </c:when>
                                                        <c:otherwise>
                                                            <button class="btn ripple btn-3d btn-success" type="button" disabled>
                                                                <span class="fa fa-trash">Re-Print</span>
                                                            </button>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                                    <c:choose>
                                                        <%--eric pake DIVISIKODE ~ DIVISI, sonny pake nomor DIVISINOMOR ~ NomorDivisi--%>
                                                        <c:when test="${NomorDivisi=='5'}">
                                                            <!--  seleksi jika ada tSAw dan tSAk maka tambahkan ke URL dan lempar ke method deleteBarang di ManageController.java-->
                                                            <c:choose>
                                                                <c:when test="${empty tSAw and empty tSAk}">
                                                                    <a href="<%=enkpNomor%>" onClick="return confirm('Anda yakin untuk menghapus data ini?')">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="<%=enkpNomor%>?tSAw=<%=tSAw%>&tSAk=<%=tSAk%>" onClick="return confirm('Anda yakin untuk menghapus data ini?')">
                                                                </c:otherwise>
                                                            </c:choose>            
                                                                <button class="btn ripple btn-3d btn-danger" type="button">
                                                                    <span class="fa fa-trash">Hapus</span>
                                                                </button>
                                                            </a>    
                                                        </c:when>
                                                        <c:otherwise>
                                                            <button class="btn ripple btn-3d btn-danger" type="button" disabled>
                                                                <span class="fa fa-trash">Hapus</span>
                                                            </button>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <!--  cek value tSAw dan tSAk-->
                                                    <!--<span><%=tSAw%></span>-->
                                                    <!--<span><%=tSAk%></span>-->
                                                </td>
                                                <!--  hidden display data nomor-->
                                                <!--<td align="center">${list.NOMORTRMST_SJ_MT}</td>-->
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                'date-uk-pre': function (a) {
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                'date-uk-asc': function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                'date-uk-desc': function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $(function () {
                //  prevent multi click (no_double_click)
                var hitungKlik = 1;
                $('.no_double_click').click(function (event) {
                    hitungKlik++;
                    if (hitungKlik > 2) {
                        $(this).attr('disabled', 'disabled');
                        event.preventDefault();
                    }
                });
                
                //  datepicker
                $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset;}});
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy",
                    monthNames: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
                    dayNamesMin: [ "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" ]
                });
                
                //  datatables
                $("#tables").DataTable({
                    aaSorting: [[0, "asc"]],
                    dom: "<'left'f>",
                    paging: false,
                    info: false,
                    aoColumns: [null,{sType: "date-uk"},null,null,null,{bSortable: false}],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sEmptyTable: "Silahkan input \"Tanggal Kirim\" diatas untuk menampilkan daftar surat jalan.",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan."
                    }
                });
                
                //eric, teknik lain dlm jquery.
                /* tentang ^= (caret equals).
                 * [attr^=val] a CSS selector that means:
                 * Element that has an attribute named 'attr' with a value that STARTS WITH 'val'.
                 * It's similar to [attr$=val], which does the opposite, looking for an attribute ENDS WITH val
                 * cek jg => http://api.jquery.com/attribute-starts-with-selector/
                 */
                jQuery("a[target^=_new]").click(function () {
                    return openWindow(this.href);   //panggil fungsi openWindow(url) di bawah
                });
                
                //eric
                function openWindow(url) {
                    var width = 850;
                    var height = 550;
                    window.open(
                            url,
                            'newwindow',
                            'width=' + width + ','
                            + 'height=' + height + ','
                            + 'top=' + ((window.innerHeight - height) / 2) + ','
                            + 'left=' + ((window.innerWidth - width) / 2));
                    return false;
                }
            });
        </script>
    </body>
</html>