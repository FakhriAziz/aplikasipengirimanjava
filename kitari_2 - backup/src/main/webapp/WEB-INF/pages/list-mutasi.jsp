<%-- 
    Document   : list-mutasi
    Created on : Mar 20, 2018, 4:00:18 PM
    Author     : PROGRAMER
--%>
<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper2">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content">
                <div class="panel col-md-12 top-20 padding-0">
                    <div style="padding-left: 13px"><h3>Daftar Order Mutasi</h3></div>
                    <c:if test="${not empty errtgl}">
                        <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">�</span>
                            </button>
                            <center><strong>Peringatan!</strong>${errtgl}</center>
                        </div>
                    </c:if>
                    <!--panel body atas, input pilih tanggal untuk menampilkan list mutasi dan cetak ke excel-->
                    <div class="panel-body">
                        <form method="post" action="listMutasi">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="form-group">
                                <div class="left">
                                    <label>Tanggal Order Mutasi :</label>
                                    <input type="text" class="datepicker" name="tglAwal"/>
                                </div>
                                <div class="left">
                                    <label>&nbsp;s.d.&nbsp;</label>
                                    <input type="text" class="datepicker" name="tglAkhir"/>
                                </div>
                                <div class="left spacer">
                                    <!--<button type="submit" name="btnMutasi" class="btn btn-3d btn-default no_double_click">-->
                                    <button type="submit" name="btnMutasi" class="btn btn-3d btn-primary no_double_click">
                                        <span>Proses Tanggal</span>
                                    </button>
                                </div>
                                <div class="left">
                                    <button id="btnExport" class="btn btn-3d btn-success" type="button">
                                        <span>Cetak</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--panel body bawah, list mutasi-->
                    <div class="panel-body">
                        <div class="responsive-table">
                            <form id="formListMutasi" method="post" action="kirimMutasi" target="myMutasi">
                                <table id="tables" class="table table-striped table-bordered" style="width: 1127px">
                                    <thead>
                                        <tr>
                                            <th align="center">No.</th>
                                            <th align="center">Tanggal Order</th>
                                            <th align="center">Nomor Order</th>
                                            <th align="center">Toko</th>
                                            <th align="center">Kode Barang</th>
                                            <th align="center">Nama Barang</th>
                                            <th align="center">Qty Kirim</th>
                                            <th align="center">Qty Order</th>
                                            <th align="center">Keterangan Order</th>
                                            <!--  hidden display data nomor-->
<!--                                            <th>Nomor TRMST</th>
                                            <th>Nomor TRDET</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--  varStatus untuk membantu penomoran baris data-->
                                        <!--from: MainController->IF[daftarMUTASI, listMutasi] backto: MainController goto: data-mutasi-->
                                        <c:forEach var="setdata" items="${datamutasi}" varStatus="loopCounter">
                                            <tr>
                                                <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                                <td>${loopCounter.count}.</td>
                                                
                                                <!--#1 to request, ganti mekanisme date dari semula java object menjadi format jstl-->
                                                <td>
                                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${setdata.TGL_OM}" var="tanggalOM" />
                                                    <fmt:formatDate value="${tanggalOM}" pattern="dd/MM/yyyy" var="displayTOM" />
                                                    <c:out value="${displayTOM}" />
                                                    <input type="hidden" name="getTglOM" value="${displayTOM}"/>
                                                </td>
                                                
                                                <!--#2 to request-->
                                                <td>${setdata.NOMORBUKTI_OM}
                                                    <input type="hidden" name="getNomorBuktiOM" value="${setdata.NOMORBUKTI_OM}"/>
                                                </td>
                                                
                                                <!--#3 to request-->
                                                <td>${setdata.TOKO_OM}
                                                    <input type="hidden" class="cekGOM" name="getTokoOM" value="${setdata.TOKO_OM}"/>
                                                </td>
                                                
                                                <!--#4 to request-->
                                                <td>${setdata.KODESTOK_OM}
                                                    <input type="hidden" name="getKodeStokOM" value="${setdata.KODESTOK_OM}"/>
                                                </td>
                                                
                                                <!--#5 to request-->
                                                <td>${setdata.NAMASTOK_OM}
                                                    <input type="hidden" name="getNamaStokOM" value="${setdata.NAMASTOK_OM}"/>
                                                </td>
                                                
                                                <!--#6 to request-->
                                                <td align="right">
                                                    <input type="number" class="cekjml" name="getQtyKirimOM" min="0" max="${setdata.QTY_OM}" style="width: 45px" value="0"/>
                                                </td>
                                                
                                                <!--#7 to request-->
                                                <td align="right">${setdata.QTY_OM}
                                                    <input type="hidden" name="getQtyOM" value="${setdata.QTY_OM}"/>
                                                </td>
                                                
                                                <!--#8 & #9 to request-->
                                                <td>${setdata.KET_OM}<br/>${setdata.SUBKET_OM}
                                                    <input type="hidden" name="getKetOM" value="${setdata.KET_OM}"/>
                                                    <input type="hidden" name="getSubKetOM" value="${setdata.SUBKET_OM}"/>
                                                </td>
                                                
                                                <!--#10 to request-->
                                                <input type="hidden" name="getNomorTRMSTOM" value="${setdata.NOMORTRMST_OM}"/>
                                                
                                                <!--#11 to request-->
                                                <input type="hidden" name="getNomorTRDETOM" value="${setdata.NOMORTRDET_OM}"/>
                                                
                                                <!--  hidden display data nomor-->
<!--                                                <td align="center">${setdata.NOMORTRMST_OM}</td>
                                                <td align="center">${setdata.NOMORTRDET_OM}</td>-->
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                                <c:set var="setDaftar" value="${datamutasi}"/>
                                <c:choose>
                                    <c:when test="${not empty setDaftar}">
                                        <div class="col-md-12" style="text-align:center">
                                            <button id="pm" class="btn btn-3d btn-primary" type="submit">
                                                <span>Proses Mutasi</span>
                                            </button>
                                        </div>
                                    </c:when>
                                </c:choose>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script src="${contextPath}/resources/js/appexcel.js"></script>
        <script type="text/javascript">
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function ( a ) {
                    var ukDatea = a.split("/");
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            
            $(function(){
                //  unblock saat load/reload halaman
                setTimeout($.unblockUI, 2000);
                                  
                //  prevent multi click (no_double_click)
                var hitungKlik = 1;
                $(".no_double_click").click(function(event){
                    hitungKlik++;
                    if(hitungKlik > 2){
                        $(this).attr("disabled", "disabled");
                        event.preventDefault();
                    }
                });
                
                //  datepicker
                $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset;}});
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy",
                    monthNames: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
                    dayNamesMin: ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]
                });
                
                //  options induk datatables
                var options = {
                    aaSorting: [[0,"asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-6'i>",
                    aoColumns: [null,{sType: "date-uk"},null,null,null,null,null,null,null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Silahkan input \"Tanggal Order Mutasi\" diatas untuk menampilkan \"Daftar Order Mutasi\".",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    }
                };
                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5,20,50,-1], ["Per 5 data","Per 20 data","Per 50 data","Semua data"]]
                });
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });
                
                //  eksekusi datatables dengan opsi1
                var oTable = $("#tables").DataTable(opt1);
                
                //  solusi utk validasi input yg loop/iterasi dengan support pagination datatable
                var allCekJml = oTable.$(":input.cekjml", {"page": "all"});
                var allCekGOM = oTable.$(":input.cekGOM", {"page": "all"});
                
                //  formListMutasi submission function
                $("#formListMutasi").on("submit", function(evt){
                    //cegah submit dulu agar bisa run validasi dan blockUI
                    evt.preventDefault();
                    
                    //dummy variabel utk validasi dan sbg penanda proses lanjut/tidak.
                    var dummy;
                   
                   //block halaman saat akan submission dan setelah pengecekan isian qty mutasi ada yg lebih dari 0, toko OM sama, dan dummy <> 0.
                    $.each(allCekJml, function(ind, el){
                        if($(this).val() > "0") {
                            if(typeof dummy === "undefined"){
                                dummy = $(allCekGOM[ind]).val();
                                console.log("if #1 => [ind]: " + ind + ", dummy: " + dummy + ", allCekGOM: " + $(allCekGOM[ind]).val());
                            }
                            else if(dummy !== $(allCekGOM[ind]).val()){
                                alert("Pilihan Beda Toko: " + $(allCekGOM[ind]).val());
                                console.log("if #2 => [ind]: " + ind + ", dummy: " + dummy + ", allCekGOM: " + $(allCekGOM[ind]).val());
                                dummy = 0;
                                return false;
                            }
                        }
                    }); //end each
                    
                    if(dummy !== 0){
                        console.log("dummy setelah pengecekan: " + dummy);
                        $.blockUI({
                           css: {
                               border: "none", 
                               padding: "15px 0",
                               backgroundColor: "#000",
                               "-webkit-border-radius": "10px",
                               "-moz-border-radius": "10px",
                               opacity: .5,
                               color: "#fff"
                           },
                           message: "<h4>..harap tunggu, masih proses entry mutasi..</h4>"
                        });
                       
                        //  destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                        oTable.destroy();
                        $("#tables").DataTable(opt2);

                        //bind ulang untuk submit form
                        $(this).unbind("submit").submit();
                        
                        //buka jendela myMutasi setelah submit form diatas
                        window.open("kirimMutasi", "myMutasi", "resizeable=no");
                    }
                    
                }); //END formListMutasi submission function
                
                //cek input jumlah min-max
                allCekJml.change(function(){
                    var max = parseInt($(this).attr("max"));
                    var min = parseInt($(this).attr("min"));
                    if ($(this).val() > max){ $(this).val(max); }
                });
                
                //cetak ke excel
                $("#btnExport").click(function(){
                    $("#tables").exportExcel({
                        name: "Mutasi",
                        filename: "Daftar Order Mutasi (OM)",
                        fileext: ".xls"
                    });
                });
            });
        </script>
    </body>
</html>