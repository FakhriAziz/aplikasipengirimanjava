<%-- 
    Document   : data-pembelian
    Created on : Jul 23, 2019, 3:05:15 PM
    Author     : PROGRAMER
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <div class="container-fluid mimin-wrapper2">
            <div class="panel col-md-12 top-20 padding-0">
                <div style="padding-left: 13px"><h3>Daftar LPB</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <form id="formDataLPB" method="post" action="prosesLPB">
                            <!--panel body atas, input memo-->
                            <table class="table table-striped table-bordered" style="width:550px;" cellspacing="0">
                                <tr>
                                    <!--#1 to request-->
                                    <th align="center">Memo LPB (Max. 255 karakter)</th>
                                    <td>:</td>
                                    <td><textarea name="dataMemoLPB" cols="54" rows="3" class="memo"></textarea></td>
                                </tr>
                            </table>
                            <%  
                                SimpleDateFormat formater = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
                                SimpleDateFormat AppDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            %>
                            <table id="dataLPB" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">No.</th>
                                        <th align="center">Tanggal PO</th>
                                        <th align="center">Nomor PO</th>
                                        <th align="center">Toko</th>
                                        <th align="center">Kode Barang</th>
                                        <th align="center">Nama Barang</th>
                                        <th align="center">Qty</th>
                                        <th align="center">Qty<br>(Maks.)</th>
                                        <!--informasi harga beli dan supplier-->
                                        <!--<th align="center">Harga Beli</th>-->
                                        <th align="center">Supplier</th>
                                        <th align="center">Keterangan PO</th>
                                        <!--hidden display data nomor-->
<!--                                        <th>Nomor TRMST</th>
                                        <th>Nomor TRDET</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--varStatus untuk membantu penomoran baris data-->
                                    <!--from: MainController->model.addObject("daftarTerimaLPB", list_terimaLPB); goto: ManageController-->
                                    <c:forEach var="daftar" items="${daftarTerimaLPB}" varStatus="loopCounter"> 
                                        <tr>
                                            <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                            <td>${loopCounter.count}.</td
                                            
                                            <!--#2 to request-->
                                            <c:set var="tanggal" value="${daftar.mapTglLPB}"/>
                                            <%
                                                String dateStr = String.valueOf(pageContext.getAttribute("tanggal"));
                                                Date result = formater.parse(dateStr);
                                            %>
                                            <td>
                                                <fmt:formatDate pattern="dd/MM/YYYY" value="${daftar.mapTglLPB}"/>
                                                <input type="hidden" name="dataTglLPB" value="<%=AppDateFormat.format(result)%>">
                                            </td>
                                            
                                            <!--#3 to request-->
                                            <td>${daftar.mapNomorBuktiLPB}
                                                <input type="hidden" name="dataNomorBuktiLPB" value="${daftar.mapNomorBuktiLPB}">
                                            </td>
                                            
                                            <!--KHUSUS PO, TIDAK KE REQUEST, nanti tampil di laporan LPB via sql query-->
                                            <td>${daftar.mapTokoLPB}
                                                <!--<input type="hidden" name="dataTokoLPB" value="${daftar.mapTokoLPB}">-->
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan LPB via sql query-->
                                            <td>${daftar.mapKodeStokLPB}</td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan LPB via sql query-->
                                            <td>${daftar.mapNamaStokLPB}</td>
                                            
                                            <!--#4 to request-->
                                            <td align="right">${daftar.mapQtyLPB}
                                                <input type="hidden" name="dataQtyLPB" value="${daftar.mapQtyLPB}">
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, QTY PO tidak ditampilkan di jasper-->
                                            <td align="right">${daftar.mapQtyPO}</td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan LPB via sql query-->
                                            <!--<td align="right">Rp. ${daftar.mapHargaBeliLPB}</td>-->
                                            
                                            <!--#5 to request-->
                                            <td>${daftar.mapNamaSupplierLPB}
                                                <input type="hidden" name="dataNamaSupplierLPB" value="${daftar.mapNamaSupplierLPB}">
                                            </td>
                                            
                                            <!--TIDAK KE REQUEST, nanti tampil di laporan LPB via sql query-->
                                            <td>${daftar.mapKetLPB}<br/>${daftar.mapSubKetLPB}</td>
                                            
                                            <!--#6 to request-->
                                            <input type="hidden" name="dataAlamatSupplierLPB" value="${daftar.mapAlamatSupplierLPB}">
                                            
                                            <!--#7 to request-->
                                            <input type="hidden" name="dataTelpSupplierLPB" value="${daftar.mapTelpSupplierLPB}">
                                            
                                            <!--#8 to request-->
                                            <input type="hidden" name="dataNomorTRMSTLPB" value="${daftar.mapNomorTRMSTLPB}">
                                            
                                            <!--#9 to request-->
                                            <input type="hidden" name="dataNomorTRDETLPB" value="${daftar.mapNomorTRDETLPB}">
                                            
                                            <!--hidden display data nomor-->
<!--                                            <td>${daftar.mapNomorTRMSTLPB}</td>
                                            <td>${daftar.mapNomorTRDETLPB}</td>-->
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                            <c:set var="setData" value="${daftarTerimaLPB}"/>
                            <c:choose>
                                <c:when test="${not empty setData}">
                                    <div class="col-sm-2" style="text-align: right">
                                        <button class="btn btn-3d btn-primary" type="submit">
                                            <span>Simpan Data</span>
                                        </button>
                                    </div>
                                    <div class="left">
                                        <button class="btn btn-3d btn-danger" type="button" onClick="exitAndReload()">
                                            <span>Batal Data</span>
                                        </button> 
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="col-md-12" style="text-align:center">
                                        <button class="btn btn-3d btn-primary" type="button" onClick="window.close()">
                                            <span>Kembali ke Daftar Purchase Order (PO)</span>
                                        </button> 
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            //untuk form target list-pembelian.jsp
            window.name = "myLPB";
                
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $(function(){ 
                //options induk datatables
                var options = {
                    aaSorting: [[0,"asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-2'i>",
                    aoColumns: [null,{sType: "date-uk"},null,null,null,null,null,null,null,null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Tidak ada Daftar LPB.",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    },
                    //set fixed width utk bbrp kolom
                    columnDefs: [
                        {'targets': [0,6], 'width': 25},
                        {'targets': [1,2], 'width': 75},
                        {'targets': 7, 'width': 35},
                        {'targets': 8, 'width': 155}
                    ]
                };
                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5,20,50,-1], ["Per 5 data", "Per 20 data", "Per 50 data", "Semua data"]]
                });
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });
                
                //eksekusi datatables dengan opsi1
                var oTable = $("#dataLPB").DataTable(opt1);
                
                //submission function
                $("#formDataLPB").on("submit", function(){
                    //destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                    oTable.destroy();
                    $("#dataLPB").DataTable(opt2);
                });
            });
            
            //exit dan reload dengan konfirmasi
            function exitAndReload() {
                var x = confirm('Anda yakin untuk membatalkan?');
                if (x) { 
                    window.close();
                    window.opener.location.reload(true); 
                }
            }
        </script>
    </body>
</html>
