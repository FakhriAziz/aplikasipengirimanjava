<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <div class="container-fluid mimin-wrapper2">
            <div class="panel col-md-12 top-20 padding-0">
                <div style="padding-left: 13px"><h3>Proses Pengiriman SO</h3></div>           
                <div class="panel-body">
                    <div class="responsive-table">
                        <form id="formDataKirim" method="post" action="tambahKirim">
                            <%                                SimpleDateFormat formater = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
                                SimpleDateFormat AppDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            %>
                            <table id="dataPengiriman" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">No.</th>
                                        <th align="center">Tanggal SO</th>
                                        <th align="center">No. SO</th>
                                        <!--sonny, add tanggal bayar dan nobukti pembayaran-->
                                        <th align="center">Tanggal Bayar</th>
                                        <th align="center">No. Pembayaran</th>
                                        <th align="center">Toko</th>
                                        <th align="center">Kode Barang</th>
                                        <th align="center">Nama Barang</th>
                                        <th align="center">Qty</th>
                                        <th align="center">Qty SO</th>
                                        <th align="center">Keterangan SO</th>
                                        <!--JENIS BARANG DIHAPUS-->
                                        <th align="center">Nama</th>
                                        <th align="center">Alamat</th>
                                        <th align="center">No. Telp/No. HP</th>
                                        <th align="center">No. Program Lama</th>
                                        <th align="center">SPM</th>
                                        <th align="center">Nama Driver</th><!--FAF, Tambahan-->
                                        <th align="center">Nomor Plat</th>
                                        <!--  hidden display data gudang, nomor2, tgl kirim so-->
                                        <!--                                        <th>Gudang</th>
                                                                                <th>Nomor TRMST</th>
                                                                                <th>Nomor TRDET</th>
                                                                                <th align="center">Tanggal Kirim SO</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--  varStatus untuk membantu penomoran baris data-->
                                    <!--from: MainController->model.addObject("daftarkirim", listData); goto: ManageController-->
                                    <c:forEach var="daftar" items="${daftarkirim}" varStatus="loopCounter">
                                        <tr>
                                            <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                            <td>${loopCounter.count}.</td>

                                            <!--TIDAK KE REQUEST/TIDAK PERLU INPUT HIDDEN, nanti tampil di laporan SJ Penjualan via sql query-->
                                            <td><fmt:formatDate pattern="dd/MM/YYYY" value="${daftar.mapTglSO}" /></td>

                                            <!--#1 to request-->
                                            <td>${daftar.mapNomorBuktiSO}
                                                <input type="hidden" name="dataNomorBuktiSO" value="${daftar.mapNomorBuktiSO}">
                                            </td>

                                            <!--sonny, add tanggal bayar dan nobukti pembayaran-->
                                            <!--TIDAK KE REQUEST, Tgl IR tidak ditampilkan di jasper, UPDATE ADD CEK TGL PEMBAYARAN (02 MEI 2019)-->
                                            <td>
                                                <fmt:formatDate pattern="dd/MM/YYYY" value="${daftar.mapTglIR}" var="displayTIR"/>
                                                <!--utk displayTIR, karena tdk bisa coalesce date ke '-', add ternary filter-->
                                                <c:out value="${empty displayTIR ? '-' : displayTIR}"/>
                                            </td>

                                            <!--TIDAK KE REQUEST, Nomor Bukti IR tidak ditampilkan di jasper, UPDATE ADD NOBUKTI PEMBAYARAN (02 MEI 2019)-->
                                            <td>${daftar.mapNomorBuktiIR}</td>

                                            <!--TIDAK KE REQUEST, Toko SO tidak ditampilkan di jasper-->
                                            <td><b>${daftar.mapTokoSO}</b></td>

                                            <!--TIDAK KE REQUEST, nanti tampil di laporan SJ Penjualan via sql query-->
                                            <td>${daftar.mapKodeStokSO}</td>

                                            <!--TIDAK KE REQUEST, nanti tampil di laporan SJ Penjualan via sql query-->
                                            <td>${daftar.mapNamaStokSO}</td>

                                            <!--#2 to request-->
                                            <td align="right">${daftar.mapQtyKirimSO}
                                                <input type="hidden" name="dataQtyKirimSO" value="${daftar.mapQtyKirimSO}">      
                                            </td>

                                            <!--TIDAK KE REQUEST, QTY SO tidak ditampilkan di jasper-->
                                            <td align="right">${daftar.mapQtySO}</td>

                                            <!--TIDAK KE REQUEST, nanti tampil di laporan SJ Penjualan via sql query-->
                                            <td><b>${daftar.mapKetSO}</b><br/><b>${daftar.mapSubKetSO}</b></td>

                                            <!--#3 to request-->
                                            <td>${daftar.mapNamaCustomerSO}
                                                <input type="hidden" name="dataNamaCustomerSO" value="${daftar.mapNamaCustomerSO}">
                                            </td>

                                            <!--#4 to request-->
                                            <td>${daftar.mapAlamatCustomerSO}
                                                <input type="hidden" name="dataAlamatCustomerSO" value="${daftar.mapAlamatCustomerSO}">
                                            </td>

                                            <!--#5 to request-->
                                            <td>${daftar.mapTelpCustomerSO}
                                                <input type="hidden" name="dataTelpCustomerSO" value="${daftar.mapTelpCustomerSO}">
                                            </td>

                                            <!--#6 to request-->
                                            <td>${daftar.mapNomorProgLama}
                                                <input type="hidden" name="dataNomorProgLama" value="${daftar.mapNomorProgLama}">
                                            </td>

                                            <!--#7 to request-->
                                    <input type="hidden" name="dataNomorTRMSTSO" value="${daftar.mapNomorTRMSTSO}">

                                    <!--#8 to request-->
                                    <input type="hidden" name="dataNomorTRDETSO" value="${daftar.mapNomorTRDETSO}">

                                    <!--#9 to request, alternatif pake java object (servlet) bukan jstl-->
                                    <c:set var="tanggal" value="${daftar.mapTglKirimSO}"/>
                                    <%
                                        String dateStr = String.valueOf(pageContext.getAttribute("tanggal"));
                                        Date result = formater.parse(dateStr);
                                    %>
                                    <input type="hidden" name="dataTglKirimSO" value="<%=AppDateFormat.format(result)%>">

                                    <!--#10 to request-->
                                    <td>${daftar.mapSPM}
                                        <input type="hidden" name="dataSPM" value="${daftar.mapSPM}">
                                    </td>
                                    <td>${daftar.mapNAMA_DRIVER}<!--FAF, Tambahan-->
                                        <input type="hidden" name="dataNAMA_DRIVER" cols="21" rows="3" value="${daftar.mapNAMA_DRIVER}"/>
                                    </td>
                                    <td>${daftar.mapNO_PLAT}
                                        <input type="hidden" name="dataNO_PLAT" cols="21" rows="3" value="${daftar.mapNO_PLAT}"/>
                                    </td>

                                    <!--  hidden display data gudang, nomor2, tgl kirim so-->
                                    <%--<td>
                                        <fmt:formatDate pattern="dd/MM/YYYY" value="${daftar.getTglKirimSO}"/>
                                        <input type="hidden" name="dt_tglkirim_so[]" value="<%=AppDateFormat.format(result)%>"> 
                                    </td>
                                    <td>${daftar.mapGudangSJ}</td>
                                    <td>${daftar.mapNomorTRMSTSO}</td>
                                    <td>${daftar.mapNomorTRDETSO}</td>
                                    --%>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                            <c:set var="setData" value="${daftarkirim}"/>
                            <c:choose>
                                <c:when test="${not empty setData}">
                                    <div class="left spacer">
                                        <button class="btn btn-3d btn-primary" type="submit">
                                            <span>Simpan Data</span>
                                        </button>
                                    </div>
                                    <div class="left">
                                        <button class="btn btn-3d btn-danger" type="button" onClick="exitAndReload()">
                                            <span>Batal Data</span>
                                        </button> 
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="col-md-12" style="text-align:center">
                                        <button class="btn btn-3d btn-primary" type="button" onClick="window.close()">
                                            <span>Kembali ke Daftar SO</span>
                                        </button> 
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function (a) {
                    var ukDatea = a.split('/');
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function (a, b) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function (a, b) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            $(function () {
                //untuk id window.open
                var myWindow;

                //  options induk datatables
                var options = {
                    aaSorting: [[0, "asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-2'i>",
                    aoColumns: [null, {sType: "date-uk"}, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Tidak ada Daftar Pengiriman SO.",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    }
                };
                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5, 20, 50, -1], ["Per 5 data", "Per 20 data", "Per 50 data", "Semua data"]]
                });
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });

                //  eksekusi datatables dengan opsi1
                var oTable = $("#dataPengiriman").DataTable(opt1);

                //  submission function
                $("#formDataKirim").on("submit", function () {
                    //  destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                    oTable.destroy();
                    $("#dataPengiriman").DataTable(opt2);
                });

            });

            //exit dan reload dengan konfirmasi
            function exitAndReload() {
                var x = confirm('Anda yakin untuk membatalkan?');
                if (x) {
                    window.close();
                    window.opener.location.reload(true);
                }
            }
        </script>
    </body>
</html>