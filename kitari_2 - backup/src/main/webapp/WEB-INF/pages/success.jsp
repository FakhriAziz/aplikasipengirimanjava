<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <!--off by sonny-->
                <!--<div class="page-404 animated flipInX">-->
                <div class="page-404">
                    <img src="${contextPath}/resources/img/success.png" class="img-responsive"/><br/>
                    <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                    <div class="col-md-12" style="left: 300px">
                        <!--form untuk button cetak SJ-->
                        <form method="post" action="psj" target="win_psj" onsubmit="window.open('SURAT JALAN PENJUALAN', 'win_psj', 'resizeable=no');"
                              class="left spacer">
                            <button class="btn btn-3d btn-success" type="submit">
                                <span>Cetak Surat Jalan</span>
                            </button>
                        </form>
                        <!--form untuk button cetak invoice dan button close-->
                        <form method="post" action="pinvsj" target="win_pinvsj" onsubmit="window.open('INVOICE', 'win_pinvsj', 'resizeable=no');"
                              class="left spacer">
                            <button class="btn btn-3d btn-primary" type="submit">
                                <span>Cetak Invoice</span>
                            </button>
                            <span class="spacer"></span>
                            <button class="btn btn-3d btn-danger" type="button" OnClick="confirmAndExit()">
                                <span>Tutup - Close</span>
                            </button>
                        </form>
                    </div>
                </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script language="Javascript">
            $(function(){
                //disable back navigation
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });
                
                //disable F5/Ctrl+R, tp msh bisa reload jika klik kanan dan klik reload
                $(document).keydown(function(e){
                    var c = e.which || e.keyCode;
                    if (c===82 || c===116 || c===17) {
                        e.preventDefault();
                    }
                    
                }); 
            });
            
            function refreshParent(){ window.opener.location.reload(true); }
            window.onunload = refreshParent;
            
            //konfirmasi sebelum exit
            function confirmAndExit() {
                var x = confirm('Anda yakin untuk menutup form ini?');
                if (x) { window.close(); }
            }
        </script>
    </body>
</html>
