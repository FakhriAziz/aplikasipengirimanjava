<%-- 
    Document   : list-pembelian
    Created on : Jul 23, 2019, 7:35:52 AM
    Author     : PROGRAMER
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper2">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content">
                <div class="panel col-md-12 top-20 padding-0">
                    <div style="padding-left: 13px"><h3>Daftar Purchase Order (PO)</h3></div>
                    <c:if test="${not empty errtgl}">
                        <div class="alert alert-warning alert-raised alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">�</span>
                            </button>
                            <center><strong>Peringatan!</strong>${errtgl}</center>
                        </div>
                    </c:if>
                    <!--panel body atas, input pilih tanggal untuk menampilkan list PO dan cetak ke excel-->
                    <div class="panel-body">
                        <form method="post" action="listPO">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="form-group">
                                <div class="left">
                                    <label>Tanggal PO :</label>
                                    <input type="text" class="datepicker" name="tglAwal"/>
                                </div>
                                <div class="left">
                                    <label>&nbsp;s.d.&nbsp;</label>
                                    <input type="text" class="datepicker" name="tglAkhir"/>
                                </div>
                                <div class="left spacer">
                                    <button type="submit" name="btnPO" class="btn btn-3d btn-primary no_double_click">
                                        <span>Proses Tanggal</span>
                                    </button>   
                                </div>
                                <div class="left">
                                    <button id="btnExport" class="btn btn-3d btn-success" type="button">
                                        <span>Cetak</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--panel body bawah, list PO-->
                    <div class="panel-body">
                        <div class="responsive-table">
                            <form id="formListPO" method="post" action="terimaLPB" target="myLPB">
                                <table id="tables" class="table table-striped table-bordered" style="width: 1127px">
                                    <thead>
                                        <tr>
                                            <th align="center">No.</th>
                                            <th align="center">Tanggal PO</th>
                                            <th align="center">Nomor PO</th>
                                            <th align="center">Toko</th>
                                            <th align="center">Kode Barang</th>
                                            <th align="center">Nama Barang</th>
                                            <th align="center">Qty</th>
                                            <th align="center">Qty<br>(Maks.)</th>
                                            <!--informasi harga beli dan supplier-->
                                            <!--<th align="center">Harga Beli</th>-->
                                            <th align="center">Supplier</th>
                                            <th align="center">Keterangan PO</th>
                                            <!--hidden display data nomor-->
<!--                                            <th>Nomor TRMST</th>
                                            <th>Nomor TRDET</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--varStatus untuk membantu penomoran baris data-->
                                        <!--from: MainController->IF[daftarPO, listPO] backto: MainController goto: data-pembelian-->
                                        <c:forEach var="setdata" items="${dataPO}" varStatus="loopCounter">
                                            <tr>
                                                <!--loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                                <td>${loopCounter.count}.</td>
                                                
                                                <!--#1 to request, ganti mekanisme date dari semula java object menjadi format jstl-->
                                                <td>
                                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${setdata.TGL_PO}" var="tanggalPO" />
                                                    <fmt:formatDate value="${tanggalPO}" pattern="dd/MM/yyyy" var="displayTglPO" />
                                                    <c:out value="${displayTglPO}" />
                                                    <input type="hidden" name="getTglPO" value="${displayTglPO}"/>
                                                </td>
                                                
                                                <!--#2 to request-->
                                                <td>${setdata.NOMORBUKTI_PO}
                                                    <input type="hidden" class="cekNomorBuktiPO" name="getNomorBuktiPO" value="${setdata.NOMORBUKTI_PO}"/>
                                                </td>
                                                
                                                <!--#3 to request-->
                                                <td>${setdata.TOKO_PO}
                                                    <input type="hidden" class="cekGPO" name="getTokoPO" value="${setdata.TOKO_PO}"/>
                                                </td>
                                                
                                                <!--#4 to request-->
                                                <td>${setdata.KODESTOK_PO}
                                                    <input type="hidden" name="getKodeStokPO" value="${setdata.KODESTOK_PO}"/>
                                                </td>
                                                
                                                <!--#5 to request-->
                                                <td>${setdata.NAMASTOK_PO}
                                                    <input type="hidden" name="getNamaStokPO" value="${setdata.NAMASTOK_PO}"/>
                                                </td>
                                                
                                                <!--#6 to request-->
                                                <td align="right">
                                                    <input type="text" class="cekjml" name="getQtyLPB" value="0" min="0" max="${setdata.QTY_PO}"
                                                           style="width: 65px; font-size: 13px;" onblur="this.value = this.value * 1;" required
                                                           oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                                </td>
                                                
                                                <!--#7 to request-->
                                                <td align="right">${setdata.QTY_PO}
                                                    <input type="hidden" name="getQtyPO" value="${setdata.QTY_PO}"/>
                                                </td>
                                                
                                                <!--#8 to request harga beli-->
<!--                                                <td align="right">Rp. ${setdata.HARGABELI_PO}
                                                    <input type="hidden" name="getHargaBeliPO" value="${setdata.HARGABELI_PO}"/>
                                                </td>-->
                                                
                                                <!--#9 to request supplier-->
                                                <td>${setdata.NAMA_SUPPLIER_PO}
                                                    <input type="hidden" name="getNamaSupplierPO" value="${setdata.NAMA_SUPPLIER_PO}"/>
                                                </td>
                                                
                                                <!--#10 to request-->
                                                <input type="hidden" name="getAlamatSupplierPO" value="${setdata.ALAMAT_SUPPLIER_PO}"/>
                                                
                                                <!--#11 to request-->
                                                <input type="hidden" name="getTelpSupplierPO" value="${setdata.TELP_SUPPLIER_PO}"/>
                                                
                                                <!--#12 & #13 to request-->
                                                <td>${setdata.KET_PO}<br/>${setdata.SUBKET_PO}
                                                    <input type="hidden" name="getKetPO" value="${setdata.KET_PO}"/>
                                                    <input type="hidden" name="getSubKetPO" value="${setdata.SUBKET_PO}"/>
                                                </td>
                                                
                                                <!--#14 to request-->
                                                <input type="hidden" name="getNomorTRMSTPO" value="${setdata.NOMORTRMST_PO}"/>
                                                
                                                <!--#15 to request-->
                                                <input type="hidden" name="getNomorTRDETPO" value="${setdata.NOMORTRDET_PO}"/>
                                                
                                                <!--hidden display data nomor-->
<!--                                                <td align="center">${setdata.ALAMAT_SUPPLIER_PO}</td>
                                                <td align="center">${setdata.TELP_SUPPLIER_PO}</td>
                                                <td align="center">${setdata.NOMORTRMST_PO}</td>
                                                <td align="center">${setdata.NOMORTRDET_PO}</td>-->
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                                <c:set var="setDaftar" value="${dataPO}"/>
                                <c:choose>
                                    <c:when test="${not empty setDaftar}">
                                        <div class="col-md-12" style="text-align:center">
                                            <button class="btn btn-3d btn-primary" type="submit">
                                                <span>Proses LPB</span>
                                            </button>
                                        </div>
                                    </c:when>
                                </c:choose>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script src="${contextPath}/resources/js/appexcel.js"></script>
        <script type="text/javascript">
            //skrip untuk sort by date pada datatable
            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-uk-pre": function ( a ) {
                    var ukDatea = a.split("/");
                    return (ukDatea[2] + ukDatea[1] + ukDatea[0]);
                },
                "date-uk-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },
                "date-uk-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            });
            
            $(function(){
                //  unblock saat load/reload halaman
                setTimeout($.unblockUI, 2000);
                                  
                //  prevent multi click (no_double_click)
                var hitungKlik = 1;
                $(".no_double_click").click(function(event){
                    hitungKlik++;
                    if(hitungKlik > 2){
                        $(this).attr("disabled", "disabled");
                        event.preventDefault();
                    }
                });
                
                //  datepicker
                $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset;}});
                $(".datepicker").datepicker({
                    dateFormat: "dd/mm/yy",
                    monthNames: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
                    dayNamesMin: ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]
                });
                
                //  options induk datatables
                var options = {
                    aaSorting: [[0,"asc"]],
                    dom: "<'left'l><'col-sm-1'f>rt<'left'p><'col-sm-6'i>",
                    aoColumns: [null,{sType: "date-uk"},null,null,null,null,null,null,null,null],
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sLengthMenu: "Tampil: _MENU_",
                        sInfo: "[ Data: _START_ - _END_ dari total _TOTAL_ ]",
                        sInfoEmpty: "",
                        sInfoFiltered: "&nbsp;&nbsp;&nbsp;(Data telah di-filter)",
                        sEmptyTable: "Silahkan input \"Tanggal PO\" diatas untuk menampilkan \"Daftar PO\".",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan.",
                        oPaginate: {
                            sFirst: "Awal",
                            sLast: "Akhir",
                            sNext: "Berikutnya",
                            sPrevious: "Sebelumnya"
                        }
                    },
                    //set fixed width utk bbrp kolom
                    columnDefs: [
                        {'targets': [0,6], 'width': 25},
                        {'targets': [1,2], 'width': 75},
                        {'targets': 7, 'width': 35},
                        {'targets': 8, 'width': 155}
                    ]
                };
                //opsi 1
                var opt1 = $.extend({}, options, {
                    iDisplayLength: 5,
                    aLengthMenu: [[5,20,50,-1], ["Per 5 data", "Per 20 data", "Per 50 data", "Semua data"]]
                });
                //opsi 2
                var opt2 = $.extend({}, options, {
                    paging: false
                });
                
                //eksekusi datatables dengan opsi1
                var oTable = $("#tables").DataTable(opt1);
                
                /* solusi utk validasi input yg loop/iterasi dengan support pagination datatable */
                
                var allCekJml = oTable.$(":input.cekjml", {"page": "all"});
                //var allCekGPO = oTable.$(":input.cekGPO", {"page": "all"});     //off krn bukan validasi di toko
                //var allCekNomorBuktiPO = oTable.$(":input.cekNomorBuktiPO", {"page": "all"});   //OFF VALIDATION
                
                //formListPO submission function
                $("#formListPO").on("submit", function(evt){
                    //cegah submit dulu agar bisa run validasi dan blockUI
                    evt.preventDefault();
                    
                    /* OFF VALIDATION
                    //dummy variabel utk validasi dan sbg penanda proses lanjut/tidak.
                    var dummy;
                   
                   //block halaman saat akan submission dan setelah pengecekan isian qty LPB ada yg lebih dari 0, noBukti PO sama, dan dummy <> 0.
                    $.each(allCekJml, function(ind, el){
                        if($(this).val() > "0") {
                            if(typeof dummy === "undefined"){
                                dummy = $(allCekNomorBuktiPO[ind]).val();
                                console.log("if #1 => [ind]: " + ind + ", dummy: " + dummy + ", allCekNomorBuktiPO: " + $(allCekNomorBuktiPO[ind]).val());
                            }
                            else if(dummy !== $(allCekNomorBuktiPO[ind]).val()){
                                alert("Pilihan Beda PO: " + $(allCekNomorBuktiPO[ind]).val());
                                console.log("if #2 => [ind]: " + ind + ", dummy: " + dummy + ", allCekNomorBuktiPO: " + $(allCekNomorBuktiPO[ind]).val());
                                dummy = 0;
                                return false;
                            }
                        }
                    }); //end each
                    
                    if(dummy !== 0){
                        console.log("dummy setelah pengecekan: " + dummy);
                        //OFF if(dummy !== 0) krn OFF VALIDATION
                    */
                        $.blockUI({
                           css: {
                               border: "none", 
                               padding: "15px 0",
                               backgroundColor: "#000",
                               "-webkit-border-radius": "10px",
                               "-moz-border-radius": "10px",
                               opacity: .5,
                               color: "#fff"
                           },
                           message: "<h4>..harap tunggu, masih proses LPB..</h4>"
                        });
                       
                        //  destroy datatables dan panggil opsi 2 yg mem-false-kan paging agar bisa terbaca semua data ke controller
                        oTable.destroy();
                        $("#tables").DataTable(opt2);

                        //bind ulang untuk submit form
                        $(this).unbind("submit").submit();
                        
                        //buka jendela myLPB setelah submit form diatas
                        window.open("terimaLPB", "myLPB", "resizeable=no");
                    //} //OFF end if(dummy !== 0)
                    
                }); //END formListPO submission function
                
                //cek input jumlah min-max
                allCekJml.change(function(){
                    var max = parseFloat($(this).attr("max"));    //sonny, ubah parseInt ke parseFloat
                    var min = parseFloat($(this).attr("min"));
                    if ($(this).val() > max){ $(this).val(max); }
                });
                
                //cetak ke excel
                $("#btnExport").click(function(){
                    $("#tables").exportExcel({
                        name: "Purchase Order",
                        filename: "Daftar Purchase Order (PO)",
                        fileext: ".xls"
                    });
                });
            });
        </script>
    </body>
</html>