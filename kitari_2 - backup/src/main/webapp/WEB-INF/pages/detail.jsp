<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <div class="container-fluid mimin-wrapper">
            <div class="top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel">
                        <div style="padding-left: 13px">
                            <!--  filter jika transaksi penjualan maka judulnya penjualan, jika transaksi mutasi judulnya mutasi-->
                            <c:choose>
                                <c:when test="${detailBarang[0].KODETRANSAKSI < 6100}">
                                    <h3>Detail Kirim Mutasi</h3>
                                </c:when>
                                <c:otherwise>
                                    <h3>Detail Kirim Penjualan</h3>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <!--panel body atas, informasi yang tidak perlu dimuat dalam tabel krn tdk perlu berulang.-->
                        <!--FILTER TAMPIL PANEL HANYA UNTUK PENJUALAN-->
                        <c:choose>
                            <c:when test="${detailBarang[0].KODETRANSAKSI > 6100}">
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table class="table table-striped table-bordered" style="width:550px;" cellspacing="0">
                                            <tr>
                                                <th>No. Surat Jalan</th>
                                                <td>:</td>
                                                <td>${detailBarang[0].NOBUKTI_SJ_MT}</td>
                                            </tr>
                                            <tr>
                                                <th>Nama Customer</th>
                                                <td>:</td>
                                                <td>${detailBarang[0].NAMACUSTOMER}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat Customer</th>
                                                <td>:</td>
                                                <td>${detailBarang[0].ALAMATCUSTOMER}</td>
                                            </tr>
                                            <tr>
                                                <th>No. Telp/No. HP Customer</th>
                                                <td>:</td>
                                                <td>${detailBarang[0].TELPCUSTOMER}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </c:when>
                        </c:choose>
                        <!--panel body bawah, informasi detail-->
                        <div class="panel-body">
                            <div class="responsive-table">
                                <table id="listBarang" class="table table-striped table-bordered" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>No. Order</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Order</th>
                                            <th>Jumlah Kirim</th>
                                            <th>Toko</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--  varStatus untuk membantu penomoran baris data-->
                                        <c:forEach var="detail" items="${detailBarang}" varStatus="loopCounter">
                                            <tr>
                                                <!--  loopCounter untuk row numbers. index dimulai dari 0. count dimulai dari 1.-->
                                                <td>${loopCounter.count}.</td>
                                                <td>${detail.NOBUKTI_SO_OM}</td>
                                                <td>${detail.KODESTOK}</td>
                                                <td>${detail.NAMASTOK}</td>
                                                <td align="right">${detail.JMLORDER}</td>
                                                <td align="right">${detail.JMLKIRIM}</td>
                                                <td>${detail.TOKO}</td>
                                                <td>${detail.KETERANGAN}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            $(function(){
                $("#listBarang").DataTable({
                    ordering: false,
                    dom: "<'left'f>",
                    paging: false,
                    info: false,
                    oLanguage: {
                        sSearch: "Cari data: ",
                        sZeroRecords: "Data hasil pencarian tidak ditemukan."
                    }
                });
            });
        </script>
    </body>
</html>