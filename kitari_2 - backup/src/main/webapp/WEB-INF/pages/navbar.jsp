<%@page import="java.util.Base64"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%
    Base64.Encoder enk = Base64.getEncoder();     //untuk surat jalan
    Date dNow = new Date();
    SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy hh:mm");
%>
<%@page session="true"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:forEach var="cek" items="${cekUser}">
    <c:set var="Nama" value="${cek.NAMAUSER}" scope="request"/>
    <c:set var="NamaLokasi" value="${cek.NAMALOKASI}" scope="request"/>
    <c:set var="Lokasi" value="${cek.LOKASI}" scope="request"/>
    <c:set var="Toko" value="${cek.NAMATOKO}" scope="request"/>
    
    <%--eric pake DIVISIKODE yg dialiaskan DIVISI, sonny pake DIVISINOMOR yg dialiaskan NomorDivisi. cek method getCekUser() di vcekbayartrmstDAOImpl--%>
    <%--<c:set var="Divisi" value="${cek.DIVISI}" scope="request"/>--%>
    <c:set var="NomorDivisi" value="${cek.NOMORDIVISI}" scope="request"/>
</c:forEach>

<%--sonny, update 16/04/19 set off, cek total batas kirim untuk di-load di header--%>
<%--<c:set var="cekTotalBatasKirim" value="${totalBatasKirim}" scope="request"/>--%>