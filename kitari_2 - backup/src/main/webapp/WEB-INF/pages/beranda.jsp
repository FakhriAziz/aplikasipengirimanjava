<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin" class="dashboard">
        <%@ include file="/WEB-INF/pages/header.jsp"%>
        <div class="container-fluid mimin-wrapper">
            <%@ include file="/WEB-INF/pages/sidebar.jsp"%>
            <div id="content" style="width: 1127px">
                <div class="col-md-12" style="padding-top:27px;">
                    <h4 class="text-left margin-0">Silahkan klik langsung pada Menu Transaksi atau Laporan</h4>
                </div>
                <!--sonny, update 16/04/19 set off counter dashboard-->
<!--                <div class="col-md-12" style="padding-top:27px;">
                    dashboard pengiriman
                    <div class="col-md-4 left">
                        <div class="panel">
                            <div class="panel-heading border-none">
                                <h4 class="text-center margin-0">Penjualan</h4>
                            </div>
                            <div class="panel-body text-center padding-0">
                                <a href="list" class="no_double_click">
                                    <h1>${jumlahSO}</h1>
                                    <p><i>Item untuk Kirim Penjualan</i></p>
                                </a>
                                <hr/>
                                <h1>${totalSJPerHari}</h1>
                                <p><i>Qty Penjualan Terkirim Hari Ini</i></p>
                            </div>
                        </div>
                    </div>
                    dashboard mutasi
                    <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-heading border-none">
                                <h4 class="text-center margin-0">Mutasi</h4>
                            </div>
                            <div class="panel-body text-center padding-0">
                                <a href="listMutasi" class="no_double_click">
                                    <h1>${jumlahMutasi}</h1>
                                    <p><i>Item untuk Kirim Mutasi</i></p>
                                </a>
                                <hr/>
                                <h1>${totalMTPerHari}</h1>
                                <p><i>Qty Mutasi Terkirim Hari Ini</i></p>
                            </div>
                        </div>
                    </div>
                </div>       
                <div class="col-md-12">
                    dashboard retur jual
                    <div class="col-md-4 left">
                        <div class="panel">
                            <div class="panel-heading border-none">
                                <h4 class="text-center margin-0">Retur Penjualan</h4>
                            </div>
                            <div class="panel-body text-center padding-0">
                                <a href="listORJ" class="no_double_click">
                                    <h1>${jumlahORJ}</h1>
                                    <p><i>Item untuk Retur Jual</i></p>
                                </a>
                                <hr/>
                                <h1>${totalRJPerHari}</h1>
                                <p><i>Qty Retur Penjualan Hari Ini</i></p>
                            </div>
                        </div>
                    </div>
                    dashboard retur beli
                    <div class="col-md-4 left">
                        <div class="panel">
                            <div class="panel-heading border-none">
                                <h4 class="text-center margin-0">Retur Pembelian</h4>
                            </div>
                            <div class="panel-body text-center padding-0">
                                <a href="listORB" class="no_double_click">
                                    <h1>${jumlahORB}</h1>
                                    <p><i>Item untuk Retur Beli</i></p>
                                </a>
                                <hr/>
                                <h1>${totalRBPerHari}</h1>
                                <p><i>Qty Retur Pembelian Hari Ini</i></p>
                            </div>
                        </div>
                    </div>
                </div>
-->
            </div>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script type="text/javascript">
            $(function(){
                var hitungKlik = 1;
                $('.no_double_click').click(function(event) {
                    hitungKlik++;
                    if (hitungKlik > 2) {
                        $(this).attr('disabled', 'disabled');
                        event.preventDefault();
                    }
                });
            });
        </script>
    </body>
</html>