<%-- 
    Document   : success-retur-jual
    Created on : Oct 4, 2018, 6:09:06 PM
    Author     : PROGRAMER
--%>

<%@ include file="/WEB-INF/pages/navbar.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="/WEB-INF/pages/style.jsp"%>
    </head>
    <body id="mimin">
        <div class="col-md-12">
            <center>
                <div class="page-404">
                    <img src="${contextPath}/resources/img/success.png" class="img-responsive"/><br/>
                    <div class="col-md-12" style="left: 230px">
                        <!--form untuk button cetak TRJ-->
                        <form method="post" action="ptrj" target="win_ptrj" onsubmit="window.open('TERIMA RETUR JUAL', 'win_ptrj', 'resizeable=no');"
                              class="left spacer">
                            <button class="btn btn-3d btn-success" type="submit">
                                <span>Cetak Surat Terima Retur Jual</span>
                            </button>
                        </form>
                        <!--form untuk button cetak invoice dan button close-->
                        <form method="post" action="pinvtrj" target="win_pinvtrj" onsubmit="window.open('INVOICE_TRJ', 'win_pinvtrj', 'resizeable=no');"
                              class="left spacer">
                            <button class="btn btn-3d btn-primary" type="submit">
                                <span>Cetak Invoice Retur Jual</span>
                            </button>
                            <span class="spacer"></span>
                            <button class="btn btn-3d btn-danger" type="button" OnClick="confirmAndExit()">
                                <span>Tutup - Close</span>
                            </button>
                        </form>
                    </div>
                </div>
            </center>
        </div>
        <%@ include file="/WEB-INF/pages/js.jsp"%>
        <script language="Javascript">
            $(function(){
                //disable back navigation
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function () {
                    history.pushState(null, null, document.URL);
                });
                
                //disable F5/Ctrl+R, tp msh bisa reload jika klik kanan dan klik reload
                $(document).keydown(function(e){
                    var c = e.which || e.keyCode;
                    if (c===82 || c===116 || c===17) {
                        e.preventDefault();
                    }
                    
                }); 
            });
            
            function refreshParent(){ window.opener.location.reload(true); }
            window.onunload = refreshParent;
            
            //konfirmasi sebelum exit
            function confirmAndExit() {
                var x = confirm('Anda yakin untuk menutup form ini?');
                if (x) { window.close(); }
            }
        </script>
    </body>
</html>
